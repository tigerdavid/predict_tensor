//
//  SimpleRegressionStatisticsAggregator.h
//  FISH
//
//  Created by Yaozong Gao on 3/25/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __SimpleRegressionStatisticsAggregator_h__
#define __SimpleRegressionStatisticsAggregator_h__

#include "forest/interfaces.h"
#include "common/vect.h"
#include <fstream>

namespace BRIC { namespace IDEA { namespace FISH {

class SimpleRegressionStatisticsAggregator
{
public:
	typedef SimpleRegressionStatisticsAggregator Self;

	// constructors
	SimpleRegressionStatisticsAggregator();
	SimpleRegressionStatisticsAggregator(unsigned int dimension);
	SimpleRegressionStatisticsAggregator(const Self& stat);
	// destructor
	~SimpleRegressionStatisticsAggregator();

	// public interfaces
	inline void Clear();
	inline unsigned int GetSampleNumber() const;
	inline float Entropy() const;
	inline void Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx);
	inline void Aggregate(const Self& aggregator);
	inline Self DeepClone() const;

	// other interfaces
	inline bool operator==(const Self& stat) const;
	inline Self& operator=(const Self& stat);
	inline unsigned int GetTargetDimension() const;

	inline void GetMean(float* mean) const;
	inline void GetVariance(float* vars) const;

	// IO Operators
	void Serialize(std::ostream& o) const;
	void Deserialize(std::istream& in);

	static const char* BinaryHeader;

private:

	unsigned int m_num;
	unsigned int m_dimension;
	float* m_sum;
	float* m_sqsum;

};

//////////////////////////////////////////////////////////////////////////

const char* SimpleRegressionStatisticsAggregator::BinaryHeader = "BRIC.IDEA.RegressionForest.SimpleRegressionStatisticsAggregator";

SimpleRegressionStatisticsAggregator::SimpleRegressionStatisticsAggregator(): m_num(0), m_dimension(0), m_sum(NULL), m_sqsum(NULL) {}

SimpleRegressionStatisticsAggregator::SimpleRegressionStatisticsAggregator(unsigned int dimension): m_num(0), m_dimension(dimension) {

	unsigned int buffersize = dimension * 2;
	m_sum = (float *)malloc(sizeof(float) * buffersize);
	m_sqsum = m_sum + m_dimension;
	::memset(m_sum, 0, sizeof(float) * buffersize);
}

SimpleRegressionStatisticsAggregator::SimpleRegressionStatisticsAggregator(const SimpleRegressionStatisticsAggregator& stat) {

	m_num = stat.m_num;
	m_dimension = stat.m_dimension;

	if (stat.m_dimension == 0)
		m_sum = m_sqsum = NULL;
	else {
		unsigned int bufferSize = stat.m_dimension * 2;
		m_sum = (float *)malloc(sizeof(float) * bufferSize);
		m_sqsum = m_sum + m_dimension;
		::memcpy(m_sum , stat.m_sum, sizeof(float) * bufferSize);
	}
}

SimpleRegressionStatisticsAggregator::~SimpleRegressionStatisticsAggregator() {

	if (m_sum != NULL) {
		free(m_sum);
		m_sum = m_sqsum = NULL;
	}
	m_num = m_dimension = 0;
}

void SimpleRegressionStatisticsAggregator::Clear() {

	m_num = 0;
	::memset(m_sum, 0, sizeof(float) * m_dimension * 2);
}

unsigned int SimpleRegressionStatisticsAggregator::GetSampleNumber() const {

	return m_num;
}

float SimpleRegressionStatisticsAggregator::Entropy() const {

	float mean_var = 0;

	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		float mean = m_sum[i] / m_num;
		float var = m_sqsum[i] / m_num - mean * mean;
		if (var < 0) var = 0;
		mean_var += sqrt(var);
	}
	mean_var /= m_dimension;

	return mean_var;
}

void SimpleRegressionStatisticsAggregator::Aggregate(const IDataCollection* dataCollection, unsigned int dataIdx) {

	const float* t = dataCollection->GetTarget(dataIdx);

	// update sample sum
	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		m_sum[i] += t[i];
		m_sqsum[i] += t[i] * t[i];
	}

	++m_num;
}

void SimpleRegressionStatisticsAggregator::Aggregate(const SimpleRegressionStatisticsAggregator& stat) {

	// update m_sum
	for (unsigned int i = 0; i < m_dimension; ++i) {
		m_sum[i] += stat.m_sum[i];
		m_sqsum[i] += stat.m_sqsum[i];
	}

	m_num += stat.m_num;
}

SimpleRegressionStatisticsAggregator SimpleRegressionStatisticsAggregator::DeepClone() const {

	SimpleRegressionStatisticsAggregator copy;

	unsigned int buffersize = m_dimension * 2;
	copy.m_sum = (float*)malloc(sizeof(float) * buffersize);
	copy.m_sqsum = m_sum + m_dimension;
	copy.m_num = m_num;

	::memcpy(copy.m_sum, m_sum, sizeof(float) * buffersize);

	return copy;
}

bool SimpleRegressionStatisticsAggregator::operator==(const SimpleRegressionStatisticsAggregator& stat) const {

	if (stat.m_num != m_num || stat.m_dimension != m_dimension)
		return false;

	for (unsigned int i = 0; i < m_dimension; ++i) {
		if (std::abs(m_sum[i] - stat.m_sum[i]) >= VECT_EPSILON || std::abs(m_sqsum[i] - stat.m_sqsum[i]) >= VECT_EPSILON)
			return false;
	}
	return true;
}

SimpleRegressionStatisticsAggregator& SimpleRegressionStatisticsAggregator::operator=(const SimpleRegressionStatisticsAggregator& stat) {

	if (this == &stat)
		return *this;

	m_num = stat.m_num;

	if (stat.m_dimension == 0)
	{
		m_dimension = 0;
		if (m_sum != NULL)
			free(m_sum);
		m_sum = m_sqsum = NULL;
	}
	else {
		unsigned int bufferSize = stat.m_dimension * 2;
		if (m_dimension == stat.m_dimension) {
			::memcpy(m_sum, stat.m_sum, sizeof(float)* bufferSize);
		}
		else {
			m_dimension = stat.m_dimension;
			if (m_sum != NULL)
				free(m_sum);
			m_sum = (float*)malloc(sizeof(float) * bufferSize);
			m_sqsum = m_sum + m_dimension;
			::memcpy(m_sum, stat.m_sum, sizeof(float)* bufferSize);
		}
	}

	return *this;
}

unsigned int SimpleRegressionStatisticsAggregator::GetTargetDimension() const {
	return m_dimension;
}

void SimpleRegressionStatisticsAggregator::GetMean(float* mean) const {
	
	for (unsigned int i = 0; i < m_dimension; ++i)
		mean[i] = m_sum[i] / m_num;
}

void SimpleRegressionStatisticsAggregator::GetVariance(float* vars) const
{
	for (unsigned int i = 0; i < m_dimension; ++i)
	{
		float mean = m_sum[i] / m_num;
		vars[i] = m_sqsum[i] / m_num - mean * mean;
	}
}

void SimpleRegressionStatisticsAggregator::Serialize(std::ostream& o) const {

	o.write((const char*)(&m_num), sizeof(unsigned int));
	o.write((const char*)(&m_dimension), sizeof(unsigned int));
	o.write((const char*)(m_sum), sizeof(float)* m_dimension * 2);
}

void SimpleRegressionStatisticsAggregator::Deserialize(std::istream& in) {

	in.read((char*)(&m_num), sizeof(unsigned int));
	in.read((char*)(&m_dimension), sizeof(unsigned int));

	if (m_sum != NULL) {
		free(m_sum);
		m_sum = m_sqsum = NULL;
	}

	if (m_dimension != 0)
	{
		unsigned int buffersize = m_dimension * 2;
		m_sum = (float *)malloc(sizeof(float) * buffersize);
		m_sqsum = m_sum + m_dimension;
		in.read((char *)(m_sum), sizeof(float)* buffersize);
	}
}

} } }


#endif