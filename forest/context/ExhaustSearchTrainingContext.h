//
//  ExhaustSearchTrainingContext.h
//  FISH
//
//  Created by Yaozong Gao on 7/12/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ExhaustSearchTrainingContext_h__
#define __ExhaustSearchTrainingContext_h__

#include "forest/interfaces.h"
#include "forest/wl/MemoryAxisAlignedWeakLearner.h"
#include "forest/Forest.h"
#include "common/Random.h"
#include <vector>

namespace BRIC { namespace IDEA { namespace FISH {

/** @brief training context for exhaust search of weak learners
 *
 * features of this training context includes:
 * 1) exhaust search over all selected features (pull random feature selection out of training procedure)
 * NOTE: For this class to work properly, set treeParameters.numOfRandomWeakLearners = number of features
 */
template <typename S>
class ExhaustSearchTrainingContext: public ITrainingContext<MemoryAxisAlignedWeakLearner, S>
{
public:
    
    typedef S StatsType;
    
    /** @brief constructor */
    ExhaustSearchTrainingContext(unsigned int numFeatures) {
        m_numFeatures = numFeatures;
        m_featureIndex = 0;
    }
    
    /** @brief get statistics aggregator */
    S GetStatisticsAggregator() const {
        return S();
    }

    /** @brief exhaust search over all candidate features */
    MemoryAxisAlignedWeakLearner GetRandomWeakLearner(Random& random) {
        
        MemoryAxisAlignedWeakLearner weakLearner(m_featureIndex);
        m_featureIndex = (m_featureIndex + 1) % m_numFeatures;
        
        return weakLearner;
    }
    
private:
    
	unsigned int m_numFeatures;								// number of features
    unsigned int m_featureIndex;							// next weak learner to be drawn
};

} } }

#endif

