//
//  FixedDimensionRegressionTrainingContext.h
//  FISH
//
//  Created by Yaozong Gao on 6/25/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __FixedDimensionRegressionTrainingContext_h__
#define __FixedDimensionRegressionTrainingContext_h__

#include "forest/interfaces.h"
#include "common/Random.h"

namespace BRIC { namespace IDEA { namespace FISH {

/** @brief training context for fixed variable regression 
 *
 * training context for fixed dimension regression
 * e.g., single, double, triple variable regression
*/
template <class W, class S>
class FixedDimensionRegressionTrainingContext: public ITrainingContext<W, S>
{
public:
    
    FixedDimensionRegressionTrainingContext(unsigned int featureNumber) {
        m_featureNumber = featureNumber;
    }
    
    inline S GetStatisticsAggregator() const {
        return S();
    }

    inline W GetRandomWeakLearner(Random& random) {
        return W::CreateRandomFeature(m_featureNumber, random);
    }
    
private:
    unsigned int m_featureNumber;
    
};

} } }

#endif