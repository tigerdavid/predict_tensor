// modified from training.cpp, by adding two more changes: 3. generate new i/o strategy
// #define ARMA_DONT_USE_CXX11
#include "common/classes.h"
#include "common/BoostHeaders.h"

// Param Settings for the Trees
#define MAXDEPTH 30
#define MININFOGAIN 0
#define MINLEAFNUM 8
#define NUMTHRES 20

const int numCorrFeature = 1000;
const int numProbFeature = 1000;
// const int numMeanFeature = 600;


int main(int argc, char *argv[])
{
	//initialisation
	if(( argc != 13 )&&(argc!=14))
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " WorkingDIR TensorFileList fMRIFileList ProbFileList1 2 3 RandomSeed SeedSampling PatchNum numTree PatchSize detectorFolder (maskImage)" << endl;
		cerr << "For example: $HOME/ware $HOME/ware/tensor.txt $HOME/ware/fMRI.txt $HOME/probCollection1 2 3.txt 1 1 1000 20 11 detector ($HOME/mask.nii.gz)" << endl;
		
		return EXIT_FAILURE;
	}
	
	vector<string> fmriNames;
	vector<string> tensorNames;
	vector<string> probNames1;
	vector<string> probNames2;
	vector<string> probNames3;
	
	const string tensorFileList = argv[2];
	const string fMRIFileList = argv[3];
	const string probFileList1 = argv[4];
	const string probFileList2 = argv[5];
	const string probFileList3 = argv[6];
	
	funLoadVector<string>(fmriNames,fMRIFileList);
	funLoadVector<string>(tensorNames,tensorFileList);
	funLoadVector<string>(probNames1,probFileList1);
	funLoadVector<string>(probNames2,probFileList2);
	funLoadVector<string>(probNames3,probFileList3);
	
	const unsigned int count = fmriNames.size();
	if (tensorNames.size()!=count||(probNames1.size()!=count)||(probNames2.size()!=count)||(probNames3.size()!=count))
	{
		cerr << "Nums of subjects in those FileLists are different, please double check!" << endl;
		return EXIT_FAILURE;
	}
	
	const int seedNum = atoi(argv[7]);
	const int seedSampling = atoi(argv[8]);
	const unsigned int PatchNum = atoi (argv[9]);
	const unsigned int numTree = atoi(argv[10]); // same number as the numFeature;
	const unsigned int PatchSize = atoi( argv[11]); // Should corresponds to the neighboring window size in my FCT extraction method
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	const unsigned int sradius = 1; // Corresponds to the patch size in my FCT extraction method
// 	const unsigned int sPatchSize = sradius*2+1;
// 	const unsigned int sPatchSize3 = pow(sPatchSize,3);
	const unsigned int extradius = radius + sradius; // This extradius is used in the sampling process, since feature extraction for the patches requires larger spaces than radius
	
	srand(seedNum);
	
	string outputPath = argv[1];
	string detectorFolder = argv[12];
	string outputIterPath = outputPath +"/currentIter";
	
	
	//check if currentIter folder exists
	mkdir(outputIterPath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	
	// create Tree_*.mat files in the detectorFolder
	string outTreePath = outputIterPath + "/" + detectorFolder;
	mkdir(outTreePath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	vector<vector< vector<int> > > ranCorrParam(numTree);
// 	vector<vector< vector<int> > > ranMeanParam(numTree);
	vector<vector< vector<int> > > ranProbParam(numTree);
	for (unsigned int i=0;i<numTree;i++)
	{
		funInitParamCorr(seedNum+i, PatchSize3,numCorrFeature, ranCorrParam[i]);
		string outCorrFeatureFile = outTreePath + "/Tree_" + convertInt(seedNum+i) + ".mat";
		funSaveMatrix<int>(ranCorrParam[i],outCorrFeatureFile);
		      
// 		funInitParamHaar(seedNum+i, PatchSize, numMeanFeature, ranMeanParam[i]);
// 		string outMeanFeatureFile = outTreePath + "/Mean_" + convertInt(seedNum+i) + ".mat";
// 		funSaveMatrix<int>(ranMeanParam[i],outMeanFeatureFile);
		
		funInitParamHaar(seedNum+i, PatchSize,numProbFeature,ranProbParam[i]);
		string outProbFeatureFile = outTreePath + "/Prob_" + convertInt(seedNum+i) + ".mat";
		funSaveMatrix<int>(ranProbParam[i],outProbFeatureFile);
	}
	
	// intialize setting.txt file
	// following only to extract tensorVectorLength value. Will consider a better way in the future
	VectorImageType::Pointer tempTensorImage = ImageReader<VectorImageType>(tensorNames[0].data());
	const unsigned int tensorVectorLength = tempTensorImage->GetNumberOfComponentsPerPixel();
	const unsigned int numTarget = tensorVectorLength;
// 	cout << colred << numTarget << coldef << endl;

	
	// initialise maskImage if existed; create maskImage with whole region marked if not
	VectorImageType::Pointer maskImage = VectorImageType::New();
	if (argc == 14)
		maskImage = ImageReader<VectorImageType>(argv[13]);
	else
		maskImage = ImageInitializer<VectorImageType>(tempTensorImage,1,1);
	tempTensorImage = NULL;
	
	string outParamSettingFile = outTreePath + "/setting.txt";
	ofstream outParamSetting(outParamSettingFile.data());
// 	outParamSetting << numTree << endl; // more settings can be added here if needed
	outParamSetting << PatchSize << endl;
	outParamSetting << tensorVectorLength << endl;
	outParamSetting << numCorrFeature << endl;
// 	outParamSetting << numMeanFeature << endl;
	outParamSetting << numProbFeature << endl;
	outParamSetting << PatchNum << endl; // just for recording, the PatchNum won't be used in the testing stage
	outParamSetting.close();
	
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	
	
	const unsigned long AllPatchNum = PatchNum*count;
	BLULN("AllPatchNum is " << AllPatchNum);
	// Initialising Features from the Patches
	vector<vector< featureClass<VectorPixelType> > > FeatureCollection(numTree, vector<featureClass<VectorPixelType> >(AllPatchNum));
	
	for (unsigned int pp = 0;pp<count;pp++)
	{
		GRNLN("Working on No." << pp + 1 << " Image.");
		
		VectorImageType::Pointer fmriImage = ImageReader<VectorImageType>(fmriNames[pp].data());
		VectorImageType::Pointer tensorImage = ImageReader<VectorImageType>(tensorNames[pp].data());
		VectorImageType::Pointer probImage1 = ImageReader<VectorImageType>(probNames1[pp].data());
		VectorImageType::Pointer probImage2 = ImageReader<VectorImageType>(probNames2[pp].data());
		VectorImageType::Pointer probImage3 = ImageReader<VectorImageType>(probNames3[pp].data());
		
		const unsigned int fmriVectorLength = fmriImage->GetNumberOfComponentsPerPixel();
// 		cout << colred << fmriVectorLength << coldef << endl;
		
		VectorImageType::Pointer maskFromProb = VectorImageType::New();
		maskFromProb = funGenMaskFromProb(probImage1,probImage2,probImage3,maskImage);
		
		vector <location<VectorPixelType> > PatchCenter;
		funTrainingSampling(seedSampling,fmriImage,tensorImage,maskFromProb,PatchNum,extradius,PatchCenter);

		string outMapFile = outputIterPath + "/map_" + convertInt(pp+1) + ".txt";
		ofstream outMap(outMapFile.data());
		for (auto i : PatchCenter)
			i.Serialize(outMap);
		outMap.close();
		tensorImage = NULL; // cleaned to save memory space
		
		
		VectorImageType::SizeType regionSize;
		regionSize.Fill(PatchSize);
// 		VectorImageType::SizeType tinyRegionSize; // only 3x3x3 for patch correlation computation (instead of voxel correlation)
// 		tinyRegionSize.Fill(sPatchSize);
// 		VectorImageType::SizeType unitSize;
// 		unitSize.Fill(1);
		VectorImageType::SizeType radiusSize;
		radiusSize.Fill(radius);

		
		REDLN("Computing Features");
		
		#pragma omp parallel for ordered
		for (unsigned int jj = 0;jj < PatchNum; jj++)
		{
			unsigned long int currentPatchNum = jj+pp*PatchNum;
			
			// Initialising patch
			location<VectorPixelType> tempLocation = PatchCenter[jj];
			
			VectorImageType::RegionType smallRegion;
			smallRegion.SetSize(regionSize);
			VectorImageType::IndexType regionIndex = tempLocation.GetIndex() - radiusSize;
			smallRegion.SetIndex(regionIndex);
			
			ConstIteratorType patFMRIPtr(fmriImage, smallRegion);
			patFMRIPtr.GoToBegin();
			
			vector<Col<float> > PatchContent;
			PatchContent.reserve(PatchSize3);
// 			vector<Col<float> >::iterator ContentIterator;
			
			
// 			// initialize stuffs for patch correlation computation
// 			VectorImageType::RegionType tinyRegion;
// 			tinyRegion.SetSize(tinyRegionSize);
// 			VectorImageType::IndexType tinyRegionIndex;
			
// 			Cube<ImgPixelType> MeanContent(PatchSize,PatchSize,PatchSize);
			
			
			// initialize probImage information
			ConstIteratorType patProbPtr1(probImage1,smallRegion);
			ConstIteratorType patProbPtr2(probImage2,smallRegion);
			ConstIteratorType patProbPtr3(probImage3,smallRegion);
			patProbPtr1.GoToBegin();patProbPtr2.GoToBegin();patProbPtr3.GoToBegin();
			Cube<ImgPixelType> probContent1 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
			Cube<ImgPixelType> probContent2 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
			Cube<ImgPixelType> probContent3 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
			
			for (unsigned int kk = 0; kk < PatchSize3; kk++)
			{					
				VectorImageType::IndexType probIndex1 = patProbPtr1.GetIndex();
				VectorPixelType tempProbValue1 = patProbPtr1.Get();
				probContent1(probIndex1[0]-regionIndex[0],probIndex1[1]-regionIndex[1],probIndex1[2]-regionIndex[2]) = tempProbValue1[0];
				++patProbPtr1;
				
				VectorImageType::IndexType probIndex2 = patProbPtr2.GetIndex();
				VectorPixelType tempProbValue2 = patProbPtr2.Get();
				probContent2(probIndex2[0]-regionIndex[0],probIndex2[1]-regionIndex[1],probIndex2[2]-regionIndex[2]) = tempProbValue2[0];
				++patProbPtr2;
				
				VectorImageType::IndexType probIndex3 = patProbPtr3.GetIndex();
				VectorPixelType tempProbValue3 = patProbPtr3.Get();
				probContent3(probIndex3[0]-regionIndex[0],probIndex3[1]-regionIndex[1],probIndex3[2]-regionIndex[2]) = tempProbValue3[0];
				++patProbPtr3;
				
				vector<float> targetValue;
				targetValue.reserve(fmriVectorLength);
				VectorPixelType tempPatchContent = patFMRIPtr.Get();
				targetValue.insert(std::end(targetValue),tempPatchContent.GetDataPointer(),tempPatchContent.GetDataPointer()+fmriVectorLength);
				PatchContent.push_back(Col<float>(targetValue));
				++patFMRIPtr;
			}


// 			for (auto ContentIterator = PatchContent.begin();ContentIterator != PatchContent.end();++ContentIterator)
// 			{
// 				// revert back to the Ding's method in extraction
// 				vector<float> targetValue;
// 				targetValue.reserve(fmriVectorLength);
// 				VectorPixelType tempPatchContent = patFMRIPtr.Get();
// 				targetValue.insert(std::end(targetValue),tempPatchContent.GetDataPointer(),tempPatchContent.GetDataPointer()+fmriVectorLength);
// 				*ContentIterator = Col<float>(&targetValue[0],targetValue.size(),false,true);
// //
// // 				// for patch correlation computation
// // 				tinyRegionIndex = patFMRIPtr.GetIndex()-unitSize;
// // 				tinyRegion.SetIndex(tinyRegionIndex);
// // 				ConstIteratorType tinyPtr(fmriImage, tinyRegion);
// // 				vector<float> tempPatchContent;
// // 				tempPatchContent.reserve(fmriVectorLength*sPatchSize3); // assume that the tiny region size is 3x3x3
// // 				for (tinyPtr.GoToBegin();(!tinyPtr.IsAtEnd());++tinyPtr)
// // 				{
// // 					VectorPixelType tempTinyPatchContent = tinyPtr.Get();
// // 					tempPatchContent.insert(std::end(tempPatchContent),tempTinyPatchContent.GetDataPointer(),tempTinyPatchContent.GetDataPointer()+fmriVectorLength);
// // 				}
// // 				*ContentIterator = Col<float>(&tempPatchContent[0],tempPatchContent.size(),false,true);
// 				
// // 				// for collecting of mean fMRI
// // 				VectorImageType::IndexType tempIndex = patFMRIPtr.GetIndex();
// // 				MeanContent(tempIndex[0]-regionIndex[0],tempIndex[1]-regionIndex[1],tempIndex[2]-regionIndex[2]) = funMean(patFMRIPtr.Get());
// 
// 				++patFMRIPtr;
// 			}
			for (unsigned int i=0;i<numTree;i++)
			{
				FeatureCollection[i][currentPatchNum] = featureClass<VectorPixelType>(currentPatchNum,pp,tempLocation);
				FeatureCollection[i][currentPatchNum].funAssignCorr(PatchContent,ranCorrParam[i], numCorrFeature);
// 				FeatureCollection[i][currentPatchNum].funAssignMean(MeanContent,ranMeanParam[i],numMeanFeature);
				FeatureCollection[i][currentPatchNum].funAssignProbHaar(probContent1,probContent2,probContent3,ranProbParam[i],numProbFeature);
			}
			
		}
		
	}
	
	
	// warp the current data available to fit Yaozong's code
	unsigned int numPatchFeatures = FeatureCollection[0][0].featureWarpper().size();
	cout << "\nFeature Length is: " << numPatchFeatures << endl;
	
	BRIC::IDEA::FISH::TreeTrainingParameters treeParams;
	treeParams.maxTreeDepth = MAXDEPTH;
	treeParams.minElementsOfLeafNode = MINLEAFNUM;
	treeParams.numOfCandidateThresholdsPerWeakLearner = NUMTHRES;
	treeParams.numOfRandomWeakLearners = numPatchFeatures;
	treeParams.minInformationGain = MININFOGAIN;
	
	
	for (unsigned int i=0;i<numTree;i++)
	{   
		CYNLN("Working on No." << i+1 << " Tree.");
		boost::timer::auto_cpu_timer t; 
		unsigned int featureSeedNum = seedNum + i;
		BRIC::IDEA::FISH::Random random;
		random.Seed(featureSeedNum);
		
		BRIC::IDEA::FISH::MemoryDataCollection trainingData;
		trainingData.SetFeatureNumber(numPatchFeatures);
		trainingData.SetTargetDim(numTarget);
		
		vector<float>& data = trainingData.GetDataVector();
		vector<float>& targets = trainingData.GetTargetVector();
		
		data.reserve(numPatchFeatures*AllPatchNum);
		targets.reserve(numTarget*AllPatchNum);
		
		for (unsigned long int j=0;j<AllPatchNum;j++)
		{
			for (unsigned int ii=0;ii<tensorVectorLength;ii++)
				targets.push_back(FeatureCollection[i][j].center.label[ii]);
			FeatureCollection[i][j].featureWarpper(data); 
		}
		
// 		string outDataDebugFile = outputIterPath + "/data_debug_" + convertInt(i+1) + ".txt";
// 		string outLabelsDebugFile = outputIterPath + "/labels_debug_" + convertInt(i+1) + ".txt";
// 		funSaveVector(data,outDataDebugFile);
// 		funSaveVector(targets,outLabelsDebugFile);
		
		trainingData.SetSampleNumber(AllPatchNum);
		
		
		// using the Yaozong's TreeTraining code
		BRIC::IDEA::FISH::SimpleRegressionRandTrainingContext context(numPatchFeatures, numTarget);
		PURLN("Start formal training process");
		std::unique_ptr< TreeType > tree = BRIC::IDEA::FISH::TreeTrainer<W,S>::TrainTree(context, treeParams, &trainingData, random);
		
		// Write tree
		string outTreeFile = outTreePath + "/Tree_" + convertInt(seedNum+i) + ".bin";
		ofstream outTree(outTreeFile.data(),std::ios::binary);
		if(!outTree) cerr << "Fail to write the tree." << endl;
		tree->Serialize(outTree);
		outTree.close();
	}
	
	
	YELLN("Finished");
	return EXIT_SUCCESS;
}

