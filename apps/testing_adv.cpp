// NOTE: For the testing stage, modified from training.cpp


// #define ARMA_DONT_USE_CXX11
#include "common/classes.h"
#include "common/BoostHeaders.h"

// Needed by Yaozong's code (Forest's testing code)
#include "common/ArrayFunctor.h"
#include "forest/tester/ForestClassifier.h"

#define SPACING 1

// const int seedNum = 1;

int main(int argc, char *argv[])
{
	//initialisation
	if(( argc != 6 )&&(argc!=7))
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " WorkingDIR InputFMRI detectorFolder outTensorFile numTree (maskImage)" << endl;
		cerr << "For example: $HOME/ware $HOME/fMRIAllCollection/rfMRI_60.nii.gz detector outTensor_60 20 ($HOME/mask.nii.gz)" << endl;
		return EXIT_FAILURE;
	}
	
	string outputPath = argv[1];
	string inFMRIFile = argv[2];
	string detectorFolder = argv[3];
	string outTensorFile = argv[4];
	const unsigned int numTree = atoi(argv[5]);
	
	string outputIterPath = outputPath + "/currentIter";
	string inTreePath = outputIterPath + "/" + detectorFolder;
	string inParamSettingFile = inTreePath + "/setting.txt";
	
	boost::timer::auto_cpu_timer t; 
	
	vector<unsigned int> inParamVector;
	funLoadVector<unsigned int>(inParamVector,inParamSettingFile);
	const unsigned int PatchSize = inParamVector[0]; // Should corresponds to the neighboring window size in my FCT extraction method
	const unsigned int tensorVectorLength = inParamVector[1];
	const int numCorrFeature = inParamVector[2];
// 	const int numMeanFeature = inParamVector[3];
	const unsigned int numTarget = tensorVectorLength;
	
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	const unsigned int sradius = 1; // Corresponds to the patch size in my FCT extraction method
	const unsigned int sPatchSize = sradius*2+1;
	const unsigned int sPatchSize3 = pow(sPatchSize,3);
	const unsigned int extradius = radius + sradius; // This extradius is used in the sampling process, since feature extraction for the patches requires larger spaces than radius
	
	VectorImageType::Pointer fMRIImage = ImageReader<VectorImageType>(inFMRIFile.data());
	const unsigned int fmriVectorLength = fMRIImage->GetNumberOfComponentsPerPixel();

	
	// initialise maskImage if existed; create maskImage with whole region marked if not
	VectorImageType::Pointer maskImage = VectorImageType::New();
	if (argc == 7)
		maskImage = ImageReader<VectorImageType>(argv[6]);
	else
		maskImage = ImageInitializer<VectorImageType>(fMRIImage,1,1);

	
	VectorImageType::RegionType OutRegion = fMRIImage->GetLargestPossibleRegion();
	
	vector <location<VectorPixelType> > PatchCenter;
// 	patchClass<VectorPixelType> tempPatch;
	
	funTestingSampling(fMRIImage,maskImage,SPACING,extradius,PatchCenter);
	
	const int PatchNum = PatchCenter.size();
	BLULN("Selecting " << PatchNum << " patches from the image");
	
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	
	VectorImageType::SizeType regionSize;
	regionSize.Fill(PatchSize);
	
	GRNLN("Reading information from trained classifiers");
	
	// Tree Reading and Initilization Process
	vector<vector< featureClass<VectorPixelType> > > FeatureCollection(numTree, vector<featureClass<VectorPixelType> >(PatchNum));
	vector<vector< vector<int> > > ranCorrParam;ranCorrParam.reserve(numTree);
// 	vector<vector< vector<int> > > ranMeanParam;ranMeanParam.reserve(numTree);
	unique_ptr< ForestType> forest(new ForestType);
	
	for (unsigned int i=0;i<numTree;i++)
	{
		
		vector< vector<int> > tempCorrParam;
		string inHaarFeatureFile = inTreePath + "/Tree_" + convertInt(i+1) + ".mat";
		funLoadMatrix<int>(tempCorrParam,inHaarFeatureFile);
		ranCorrParam.push_back(tempCorrParam);
		
// 		vector< vector<int> > tempMeanParam;
// 		string inMeanFeatureFile = inTreePath + "/Mean_" + convertInt(i+1) + ".mat";
// 		funLoadMatrix<int>(tempMeanParam,inMeanFeatureFile);
// 		ranMeanParam.push_back(tempMeanParam);
		
		string inTreeFile = inTreePath + "/Tree_" + convertInt(i+1) + ".bin";
		ifstream inTree(inTreeFile.data(),ios::binary);
		if(!inTree) {
			cerr << "could not read tree from " << inTreeFile << endl; exit(-1);
		}
		unique_ptr<typename ForestType::TreeType> tree = ForestType::TreeType::Deserialize(inTree);
		inTree.close();
		tree->CheckValid();
		forest->AddTree(move(tree));
		tree.release();
	}
	
	REDLN("Computing Features");
	VectorImageType::SizeType tinyRegionSize; // only 3x3x3 for patch correlation computation (instead of voxel correlation)
	tinyRegionSize.Fill(3);
	VectorImageType::SizeType unitSize;
	unitSize.Fill(1);
	
	#pragma omp parallel for ordered 
	for (int j=0;j<PatchNum;j++)
	{
		
		unsigned long int currentPatchNum = j;
		
		// Initialising patch
		location<VectorPixelType> tempLocation = PatchCenter[currentPatchNum];
		
		VectorImageType::RegionType smallRegion;
		smallRegion.SetSize(regionSize);
		VectorImageType::IndexType regionIndex;
		regionIndex[0] = tempLocation.x-radius;
		regionIndex[1] = tempLocation.y-radius;
		regionIndex[2] = tempLocation.z-radius;
		smallRegion.SetIndex(regionIndex);
		
		ConstIteratorType patFMRIPtr(fMRIImage, smallRegion);
		patFMRIPtr.GoToBegin();
		
		vector<Col<float> > PatchContent;
		PatchContent.reserve(PatchSize3);
		
		// initialize stuffs for patch correlation computation
		VectorImageType::RegionType tinyRegion;
		tinyRegion.SetSize(tinyRegionSize);
		VectorImageType::IndexType tinyRegionIndex;
		
// 		Cube<ImgPixelType> MeanContent(PatchSize,PatchSize,PatchSize);
		
		for (unsigned int kk = 0; kk < PatchSize3; kk++)
		{
			// for patch correlation computation
			tinyRegionIndex = patFMRIPtr.GetIndex()-unitSize;
			tinyRegion.SetIndex(tinyRegionIndex);
			ConstIteratorType tinyPtr(fMRIImage, tinyRegion);
			vector<float> tempPatchContent;
			tempPatchContent.reserve(fmriVectorLength*sPatchSize3); // assume that the tiny region size is 3x3x3
			for (tinyPtr.GoToBegin();(!tinyPtr.IsAtEnd());++tinyPtr)
			{
				VectorPixelType tempTinyPatchContent = tinyPtr.Get();
				tempPatchContent.insert(std::end(tempPatchContent),tempTinyPatchContent.GetDataPointer(),tempTinyPatchContent.GetDataPointer()+fmriVectorLength);
			}
			PatchContent.push_back(Col<float>(tempPatchContent));
			
// 			// for collecting of mean fMRI
// 			VectorImageType::IndexType tempIndex = patFMRIPtr.GetIndex();
// 			MeanContent(tempIndex[0]-regionIndex[0],tempIndex[1]-regionIndex[1],tempIndex[2]-regionIndex[2]) = funMean(patFMRIPtr.Get());
			
			++patFMRIPtr;
		}
		for (unsigned int i=0;i<numTree;i++)
		{
			FeatureCollection[i][currentPatchNum] = featureClass<VectorPixelType>(currentPatchNum,1,tempLocation);
			FeatureCollection[i][currentPatchNum].funAssignCorr(PatchContent,ranCorrParam[i], numCorrFeature);
// 			FeatureCollection[i][currentPatchNum].funAssignMean(MeanContent,ranMeanParam[i],numMeanFeature);
		}
	}
	
	unsigned int numPatchFeatures = FeatureCollection[0][0].featureWarpper().size();
	cout << coldef << "\nFeature Length is: " << numPatchFeatures << endl;
	
	CYNLN("Start classifying the obtained patches.");
	vector<vector<float> > predictions(PatchNum,vector<float>(tensorVectorLength,0.0));
	for (int ii = 0; ii < PatchNum; ii++)
	{
		for (unsigned int i = 0; i < numTree; ++i) 
		{
			BRIC::IDEA::FISH::ArrayFunctor<float> functor;
			vector<float> features = FeatureCollection[i][ii].featureWarpper();
			functor.SetArray(&features[0]);
			const S& stat = forest->GetTree(i)->FastApply(functor);
			vector<float> tempProbs(numTarget);
			stat.GetMean(&tempProbs[0]);
			for (unsigned int jj = 0; jj < tensorVectorLength;jj++)
				predictions[ii][jj] += tempProbs[jj]/numTree;  
		}
	}
	
	
	// produce result
	VectorImageType::Pointer resTensorImage = ImageInitializer<VectorImageType>(fMRIImage,tensorVectorLength,0);
	
	
	for (int i=0;i<PatchNum;i++)
	{
		VectorPixelType tensorElement;
		VectorImageType::IndexType tensorIndex = PatchCenter[i].GetIndex();
		tensorElement.SetSize(tensorVectorLength);
		for (unsigned int j=0;j<tensorVectorLength;j++)
			tensorElement[j] = predictions[i][j];
		resTensorImage->SetPixel(tensorIndex, tensorElement);
	}
	
	
	string outTensorPath = outputIterPath + "/" + outTensorFile + ".nii.gz";
	ImageWriter<VectorImageType>(resTensorImage,outTensorPath);
	
	YELLN("Finished");
	
	return EXIT_SUCCESS;
}
