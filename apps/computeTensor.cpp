#include "common/common.h"

int main(int argc, char *argv[])
{
	if (argc < 3)
	{
		cerr << "Usage: " << argv[0] << " inImagesTemp outVecTensorFile" << endl;
		cerr << "For example: $HOME/645551/out_ $HOME/tensor_645551.nii.gz" << endl;
		
		return EXIT_FAILURE;
	}
	string filenameFormat = argv[1];
	string outFilename = argv[2];
	
	// Initialize all the images and readers
	
	string imageV1File = filenameFormat + "V1.nii.gz";
	string imageV2File = filenameFormat + "V2.nii.gz";
	string imageV3File = filenameFormat + "V3.nii.gz";
	string imageL1File = filenameFormat + "L1.nii.gz";
	string imageL2File = filenameFormat + "L2.nii.gz";
	string imageL3File = filenameFormat + "L3.nii.gz";
	
	VectorImageType::Pointer V1Image = VectorImageType::New();
	VectorImageType::Pointer V2Image = VectorImageType::New();
	VectorImageType::Pointer V3Image = VectorImageType::New();
	ImageType::Pointer L1Image = ImageType::New();
	ImageType::Pointer L2Image = ImageType::New();
	ImageType::Pointer L3Image = ImageType::New();
	
	VectorReaderType::Pointer V1Reader = VectorReaderType::New();
	VectorReaderType::Pointer V2Reader = VectorReaderType::New();
	VectorReaderType::Pointer V3Reader = VectorReaderType::New();
	ReaderType::Pointer L1Reader = ReaderType::New();
	ReaderType::Pointer L2Reader = ReaderType::New();
	ReaderType::Pointer L3Reader = ReaderType::New();
	
	V1Reader->SetFileName(imageV1File.data());V1Reader->Update();V1Image = V1Reader->GetOutput();
	V2Reader->SetFileName(imageV2File.data());V2Reader->Update();V2Image = V2Reader->GetOutput();
	V3Reader->SetFileName(imageV3File.data());V3Reader->Update();V3Image = V3Reader->GetOutput();
	L1Reader->SetFileName(imageL1File.data());L1Reader->Update();L1Image = L1Reader->GetOutput();
	L2Reader->SetFileName(imageL2File.data());L2Reader->Update();L2Image = L2Reader->GetOutput();
	L3Reader->SetFileName(imageL3File.data());L3Reader->Update();L3Image = L3Reader->GetOutput();
	
	
	VectorImageType::RegionType OutRegion = V1Image->GetLargestPossibleRegion();
	VectorImageType::SizeType InSize = OutRegion.GetSize();
	
	const int vectorNum = V1Image->GetNumberOfComponentsPerPixel();
	cout << vectorNum << endl;
	
	// Initialize output Image
	
	VectorImageType::Pointer outImage = VectorImageType::New();
	outImage->CopyInformation(V1Image);
	outImage->SetNumberOfComponentsPerPixel(6);
	outImage->SetRegions(OutRegion);
	outImage->Allocate();
	
	
	
	for ( int k = 0; k < ( int ) InSize[2]; k++ )
		for ( int j = 0; j < ( int ) InSize[1]; j++ )
			for ( int i = 0; i < ( int ) InSize[0]; i++ )
			{
				VectorImageType::IndexType PixelIndex;
				PixelIndex[0] = i;
				PixelIndex[1] = j;
				PixelIndex[2] = k;
				
				VectorImageType::PixelType V1 = V1Image->GetPixel(PixelIndex);
				VectorImageType::PixelType V2 = V2Image->GetPixel(PixelIndex);
				VectorImageType::PixelType V3 = V3Image->GetPixel(PixelIndex);
				ImageType::PixelType L1 = L1Image->GetPixel(PixelIndex);
				ImageType::PixelType L2 = L2Image->GetPixel(PixelIndex);
				ImageType::PixelType L3 = L3Image->GetPixel(PixelIndex);
				
				VectorImageType::PixelType Res = outImage->GetPixel(PixelIndex);
				
				Res[0] = V1[0]*L1*V1[0] + V2[0]*L2*V2[0] + V3[0]*L3*V3[0];
				Res[1] = V1[0]*L1*V1[1] + V2[0]*L2*V2[1] + V3[0]*L3*V3[1];
				Res[2] = V1[0]*L1*V1[2] + V2[0]*L2*V2[2] + V3[0]*L3*V3[2];
				Res[3] = V1[1]*L1*V1[1] + V2[1]*L2*V2[1] + V3[1]*L3*V3[1];
				Res[4] = V1[1]*L1*V1[2] + V2[1]*L2*V2[2] + V3[1]*L3*V3[2];
				Res[5] = V1[2]*L1*V1[2] + V2[2]*L2*V2[2] + V3[2]*L3*V3[2];
				
				outImage->SetPixel(PixelIndex,Res);
				
			}
	
	typedef itk::ImageFileWriter<VectorImageType> WriterType;
	WriterType::Pointer writer = WriterType::New();
	writer->SetFileName(outFilename.data());
	writer->SetInput(outImage);
	writer->Update();
	
	return EXIT_SUCCESS;
}
