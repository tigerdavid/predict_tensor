#include <armadillo>
#include <iostream>
#include <random>
#include <algorithm>

using namespace std;
using namespace arma;

// float funMean(vector<float> inputVector)
// {
// 	int size = inputVector.size();
// 	float result = 0;
// 	for (int i=0;i<size;i++)
// 		result +=inputVector[i];
// 	return result/size;
// }
// 
// float funSTD(vector<float> inputVector)
// {
// 	float result = 0;
// 	auto resMean = funMean(inputVector);
// 	for (unsigned int i=0;i<inputVector.size();i++)
// 		result+=pow((inputVector[i]-resMean),2);
// 	return sqrt(result/(inputVector.size()));
// }
// 
// int main(int argc, char *argv[])
// {
// // 	int seedNum = atoi(argv[1]);
// 	
// 	wall_clock timer;
// 	
// 	random_device rnd_device;
// 	mt19937 mersenne_engine(rnd_device());
// 	uniform_int_distribution<int> dist(1,52);
// 	auto gen = bind(dist,mersenne_engine);
// 	
// 	vector<float> vec1(1200);
// 	vector<float> vec2(1200);
// 	generate(vec1.begin(),vec1.end(),gen);
// 	generate(vec2.begin(),vec2.end(),gen);
// 	
// // 	arma_rng::set_seed(seedNum);
// // 	mat outRand = randu(1,10);
// // 	cout << outRand << endl;
// 	
// 	timer.tic();
// 	for (int i=0;i<200000;i++)
// 	{
// 		Col<float> test1(vec1);
// 		Col<float> test2(vec2);
// 		as_scalar(cor(test1,test2));
// 	}
// 	
// // 	cout << "Result from armadillo is " << cor(test1,test2) << endl;
// 	cout << "time taken = " << timer.toc() << endl;
// 	
// 	
// 	timer.tic();
// 	for (int ii=0;ii<200000;ii++)
// 	{
// 	const int length = vec1.size();
// 	auto meanVec1 = funMean(vec1);
// 	auto meanVec2 = funMean(vec2);
// 	float resCov = 0.0;
// 	for (int i=0;i<length;i++)
// 		resCov+=((vec1[i]-meanVec1)*(vec2[i]-meanVec2));
// 	resCov/=length;
// 	auto Denominator = (funSTD(vec1)*funSTD(vec2));
// 	resCov/Denominator;
// 	}
// 	
// // 	cout << "Result from vanilla is " << resCov/Denominator << endl;
// cout << "time taken = " << timer.toc() << endl;
// 	
// 	return EXIT_SUCCESS;
// }

int main(int argc, char *argv[])
{
	mat A = { {1,2},{3,4},{5,6},{7,8} };
// 	mat A = randu(10,4);
	A = A.t();
	
	
	
	mat coeff;
	mat score;
	vec latent;
	vec tsquared;
	
	princomp(coeff, score, latent, tsquared, A);
	
	cout << A << endl;
	cout << coeff << endl;
	cout << score << endl;
	cout << latent << endl;
	cout << tsquared << endl;
	
	cout << "Using SVD" << endl;
	
	mat U;
	vec s;
	mat V;
	
	svd_econ(U,s,V,A);
	
	cout << U << endl;
	cout << s << endl;
	cout << V << endl;
	cout << V*V.t() << endl;
	
	cout << U*diagmat(s)*V.t() << endl;
	cout << inv(U*diagmat(s))*A << endl;
	
	
	return EXIT_SUCCESS;
}