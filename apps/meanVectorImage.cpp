#include "common/common.h"

int main(int argc, char *argv[])
{
	if (argc < 3)
	{
		std::cerr << "Usage: " << argv[0] << " inImageFile outMeanImageFile" << std::endl;
		return EXIT_FAILURE;
	}
	string filename = argv[1];
	string outFilename = argv[2];
/*	
	typedef itk::VectorImage<int, dimension>  VectorImageType;
	typedef itk::Image<float, dimension> ImageType;*/
	
	VectorImageType::Pointer vectorImage = VectorImageType::New();
	ImageType::Pointer meanImage = ImageType::New();
	
// 	typedef itk::ImageFileReader<VectorImageType> VectorReaderType;
	VectorReaderType::Pointer reader = VectorReaderType::New();
	reader->SetFileName(filename);
	reader->Update();
	vectorImage = reader->GetOutput();
	
	VectorImageType::RegionType InRegion = vectorImage->GetLargestPossibleRegion();
	VectorImageType::SizeType InSize = InRegion.GetSize();
	VectorImageType::SpacingType InSpacing = vectorImage->GetSpacing();
	
	const int vectorNum = vectorImage->GetNumberOfComponentsPerPixel();
	cout << vectorNum << endl;
	
	meanImage->SetSpacing(InSpacing);
	meanImage->SetRegions(InRegion);
	meanImage->Allocate();
	
	for ( int k = 0; k < ( int ) InSize[2]; k++ )
		for ( int j = 0; j < ( int ) InSize[1]; j++ )
			for ( int i = 0; i < ( int ) InSize[0]; i++ )
			{
				VectorImageType::IndexType PixelIndex;
				PixelIndex[0] = i;
				PixelIndex[1] = j;
				PixelIndex[2] = k;
				
				VectorImageType::PixelType vectorImageValue = vectorImage->GetPixel(PixelIndex);
				ImageType::PixelType meanValue = 0;
				for (int ii = 0;ii<vectorNum;ii++)
					meanValue += vectorImageValue[ii];
				meanValue/=vectorNum;
				meanImage->SetPixel(PixelIndex,meanValue);
				
			}
			
			typedef itk::ImageFileWriter<ImageType> WriterType;
			WriterType::Pointer writer = WriterType::New();
			writer->SetFileName(outFilename.data());
			writer->SetInput(meanImage);
			writer->Update();
			
			return EXIT_SUCCESS;
}
