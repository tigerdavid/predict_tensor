#include "common/common.h"

int main(int argc, char *argv[])
{
	//initialisation
	if( argc != 4 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << "SampleFile SampleImage outImage" << endl;
		cerr << "For example: $HOME/map_1.txt $HOME/Image1.nii.gz $HOME/mask.nii.gz" << endl;
		
		return EXIT_FAILURE;
	}
	
	const string SampleFile = argv[1];
	const string EgImageFile = argv[2];
	const string outImageFile = argv[3];
	
	VectorReaderType::Pointer EgReader = VectorReaderType::New();
	VectorImageType::Pointer EgImage = VectorImageType::New();
	EgReader->SetFileName(EgImageFile.data());
	EgReader->Update();
	EgImage = EgReader->GetOutput();
	
	VectorImageType::RegionType OutRegion = EgImage->GetLargestPossibleRegion();
	// 	VectorImageType::SizeType OutSize = OutRegion.GetSize();
	
	VectorImageType::Pointer outImage = VectorImageType::New();
	outImage->CopyInformation(EgImage);
	outImage->SetNumberOfComponentsPerPixel(1);
	outImage->SetRegions(OutRegion);
	outImage->Allocate();
	VectorPixelType tempPixel;
	tempPixel.SetSize(1);
	tempPixel.Fill(0);
	outImage->FillBuffer(tempPixel);
	
// 	vector<location<int> > inputSample;
	ifstream inSampleFile(SampleFile.data());
	string inLine;
	tempPixel.Fill(1);
	while (getline(inSampleFile,inLine))
	{
		VectorImageType::IndexType tempPixelIndex;
		istringstream itt(inLine);
		if (!(itt >> tempPixelIndex[0] >> tempPixelIndex[1] >> tempPixelIndex[2]))
		{
			break;
		}
		outImage->SetPixel(tempPixelIndex,tempPixel);
	}
	inSampleFile.close();
	
	ImageWriter<VectorImageType>(outImage,outImageFile);
	
	return EXIT_SUCCESS;
}