#include "common/common.h"

int main(int argc, char *argv[])
{
	//initialisation
	if( argc != 9 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << "EgImage outMaskImage x1 y1 z1 x2 y2 z2" << endl;
		cerr << "For example: $HOME/Image1.nii.gz $HOME/mask.nii.gz 1 1 1 20 20 20" << endl;
		
		return EXIT_FAILURE;
	}
	
	const string EgImageFile = argv[1];
	const string outImageFile = argv[2];
	const unsigned int x1 = atoi(argv[3]);
	const unsigned int y1 = atoi(argv[4]);
	const unsigned int z1 = atoi(argv[5]);
	const unsigned int x2 = atoi(argv[6]);
	const unsigned int y2 = atoi(argv[7]);
	const unsigned int z2 = atoi(argv[8]);
	
// 	cout << x1 << " " << x2 << " " << y1 << " " << y2 << " " << z1 << " " << z2 << endl;
	
	VectorReaderType::Pointer EgReader = VectorReaderType::New();
	VectorImageType::Pointer EgImage = VectorImageType::New();
	EgReader->SetFileName(EgImageFile.data());
	EgReader->Update();
	EgImage = EgReader->GetOutput();
	
	VectorImageType::RegionType OutRegion = EgImage->GetLargestPossibleRegion();
// 	VectorImageType::SizeType OutSize = OutRegion.GetSize();
	
	VectorImageType::Pointer outImage = VectorImageType::New();
	outImage->CopyInformation(EgImage);
	outImage->SetNumberOfComponentsPerPixel(1);
	outImage->SetRegions(OutRegion);
	outImage->Allocate();
	VectorPixelType tempPixel;
	tempPixel.SetSize(1);
	tempPixel.Fill(0);
	outImage->FillBuffer(tempPixel);
	
	typedef itk::ImageRegionIterator<VectorImageType> IteratorType;
	IteratorType outTensorPtr(outImage,OutRegion);
	
	for (outTensorPtr.GoToBegin();(!outTensorPtr.IsAtEnd());++outTensorPtr)
	{
		VectorImageType::IndexType outPixelIndex = outTensorPtr.GetIndex();
		unsigned int x = outPixelIndex[0];
		unsigned int y = outPixelIndex[1];
		unsigned int z = outPixelIndex[2];
		if ((x<x1)||(x>x2)||(y<y1)||(y>y2)||(z<z1)||(z>z2))
		{
			tempPixel.Fill(0);
			outTensorPtr.Set(tempPixel);
		}
		else
		{
			tempPixel.Fill(1);
			outTensorPtr.Set(tempPixel);
		}
	}
	
	ImageWriter<VectorImageType>(outImage,outImageFile);
	
	return EXIT_SUCCESS;
}