// Obtain the maximum value of the given tensor

#include "common.h"

int main(int argc, char *argv[])
{
	
	//initialisation
	if( argc != 2 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " inputImageFile" << endl;
		return EXIT_FAILURE;
	}
	
	VectorImageType::Pointer image = ImageReader<VectorImageType>(argv[1]);
	const unsigned int VectorLength = image->GetNumberOfComponentsPerPixel();
	VectorImageType::RegionType OutRegion = image->GetLargestPossibleRegion();
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType ImgPtr(image, OutRegion);
	float outMaxVal = 0;
	for (ImgPtr.GoToBegin();!ImgPtr.IsAtEnd();++ImgPtr)
	{
		VectorPixelType currentImageValue = ImgPtr.Get();
		for (unsigned int i=0;i<VectorLength;i++)
			outMaxVal = (outMaxVal>currentImageValue[i])?outMaxVal:currentImageValue[i];
	}
	cout << outMaxVal << endl;
	return EXIT_FAILURE;
	
}