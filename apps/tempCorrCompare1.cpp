// modified from CorrelationTensor.cpp for the specific need when dealing with the ABIDE dataset

#include "common/common.h"

int main(int argc, char *argv[])
{
	//initialisation
	if( argc != 8 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " inputImageFile1  inputImageFile2 refImageFile1 refImageFile2 origImageFile1 origImageFile2 outImageFile" << endl;
		return EXIT_FAILURE;
	}
	
	ImageType::Pointer image1 = ImageReader<ImageType>(argv[1]);
	ImageType::Pointer image2 = ImageReader<ImageType>(argv[2]);
	ImageType::Pointer refImage1 = ImageReader<ImageType>(argv[3]);
	ImageType::Pointer refImage2 = ImageReader<ImageType>(argv[4]);
	ImageType::Pointer orig1 = ImageReader<ImageType>(argv[5]);
	ImageType::Pointer orig2 = ImageReader<ImageType>(argv[6]);
	const string outImgFile = argv[7];
	
	ImageType::RegionType inRegion1 = image1->GetLargestPossibleRegion();
	ImageType::RegionType inRegion2 = image2->GetLargestPossibleRegion();

	if ((inRegion1!=refImage1->GetLargestPossibleRegion()))
	{
		cerr << "inRegion for the inputImage1 and refImage1 are not identical" << endl;
		return EXIT_FAILURE;
	}
	
	if ((inRegion2!=refImage2->GetLargestPossibleRegion()))
	{
		cerr << "inRegion for the inputImage2 and refImage2 are not identical" << endl;
		return EXIT_FAILURE;
	}
	
	typedef itk::ImageRegionConstIterator<ImageType> ConstIteratorType;
	ConstIteratorType imgPtr1(image1,inRegion1);
	ConstIteratorType imgPtr2(image2,inRegion2);
	ConstIteratorType refPtr1(refImage1,inRegion1);
	ConstIteratorType refPtr2(refImage2,inRegion2);
	vector<double> collectVal1;
	vector<double> collectVal2;
	vector<double> collectRef1;
	vector<double> collectRef2;
	
	for (imgPtr1.GoToBegin(),imgPtr2.GoToBegin(),refPtr1.GoToBegin(),refPtr2.GoToBegin();!imgPtr1.IsAtEnd();++imgPtr1,++imgPtr2,++refPtr1,++refPtr2)
	{
		ImgPixelType vecValue1 = imgPtr1.Get();
		ImgPixelType vecValue2 = imgPtr2.Get();
		ImgPixelType refValue1 = refPtr1.Get();
        ImgPixelType refValue2 = refPtr2.Get();
		
		if ((vecValue1!=0)&(vecValue2!=0)&(refValue1!=0)&(refValue2!=0))
		{
			collectVal1.push_back(vecValue1);	
			collectVal2.push_back(vecValue2);
			collectRef1.push_back(refValue1);
			collectRef2.push_back(refValue2);
		}
	}
	
	double resVal1 = as_scalar(cor(vec(collectVal1),vec(collectRef1)));
	double resVal2 = as_scalar(cor(vec(collectVal2),vec(collectRef2)));
	
	if (resVal1 >= resVal2)
		ImageWriter<ImageType>(orig1, outImgFile);
	else
		ImageWriter<ImageType>(orig2, outImgFile);
	return EXIT_SUCCESS;
	
}
