// NOTE: Modidified from testing_adv_prob.cpp, aiming at reducing the memory space requirement
// Also cleans those commmented lines


// #define ARMA_DONT_USE_CXX11
#include "common/classes.h"
#include "common/BoostHeaders.h"

// Needed by Yaozong's code (Forest's testing code)
#include "common/ArrayFunctor.h"
#include "forest/tester/ForestClassifier.h"

#define SPACING 1

// const int seedNum = 1;

int main(int argc, char *argv[])
{
	//initialisation
	if(( argc != 9 )&&(argc!=10))
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " WorkingDIR InputFMRI InProb1 2 3 detectorFolder outTensorFile numTree (maskImage)" << endl;
		cerr << "For example: $HOME/ware $HOME/fMRIAllCollection/rfMRI_60.nii.gz $HOME/60/probImage1 2 3.nii.gz detector outTensor_60 20 ($HOME/mask.nii.gz)" << endl;
		return EXIT_FAILURE;
	}
	
	string outputPath = argv[1];
	string inFMRIFile = argv[2];
	const string inProbFile1 = argv[3];
	const string inProbFile2 = argv[4];
	const string inProbFile3 = argv[5];
	const string detectorFolder = argv[6];
	const string outTensorFile = argv[7];
	const unsigned int numTree = atoi(argv[8]);
	
	string outputIterPath = outputPath + "/currentIter";
	string inTreePath = outputIterPath + "/" + detectorFolder;
	string inParamSettingFile = inTreePath + "/setting.txt";
	
	boost::timer::auto_cpu_timer t; 
	
	vector<unsigned int> inParamVector;
	funLoadVector<unsigned int>(inParamVector,inParamSettingFile);
	const unsigned int PatchSize = inParamVector[0]; // Should corresponds to the neighboring window size in my FCT extraction method
	const unsigned int tensorVectorLength = inParamVector[1];
	const int numCorrFeature = inParamVector[2];
	const int numProbFeature = inParamVector[3];
	const unsigned int numTarget = tensorVectorLength;
	
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	const unsigned int sradius = 0; // Corresponds to the patch size in my FCT extraction method
	const unsigned int extradius = radius + sradius; // This extradius is used in the sampling process, since feature extraction for the patches requires larger spaces than radius
	
	VectorImageType::Pointer fMRIImage = ImageReaderPad<VectorImageType>(inFMRIFile.data(),extradius);
	const unsigned int fmriVectorLength = fMRIImage->GetNumberOfComponentsPerPixel();
	
	VectorImageType::Pointer probImage1 = ImageReaderPad<VectorImageType>(inProbFile1.data(),extradius);
	VectorImageType::Pointer probImage2 = ImageReaderPad<VectorImageType>(inProbFile2.data(),extradius);
	VectorImageType::Pointer probImage3 = ImageReaderPad<VectorImageType>(inProbFile3.data(),extradius);
	
	// initialise maskImage if existed; create maskImage with whole region marked if not
	VectorImageType::Pointer maskImage = VectorImageType::New();
	if (argc == 10)
		VectorImageType::Pointer maskImage = ImageReaderPad<VectorImageType>(argv[9],extradius);
	else
		maskImage = ImageInitializer<VectorImageType>(fMRIImage,1,1);
	

	VectorImageType::RegionType OutRegion = fMRIImage->GetLargestPossibleRegion();
	
	vector <location<VectorPixelType> > PatchCenter;
	
	VectorImageType::Pointer maskFromProb = VectorImageType::New();
	maskFromProb = funGenMaskFromProb(probImage1,probImage2,probImage3,maskImage);
	
	funTestingSampling(fMRIImage,maskFromProb,SPACING,extradius,PatchCenter);
	
	const int PatchNum = PatchCenter.size();
	BLULN("Selecting " << PatchNum << " patches from the image");
	
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	
	VectorImageType::SizeType regionSize;
	regionSize.Fill(PatchSize);
	
	GRNLN("Reading information from trained classifiers");
	
	// Tree Reading and Initilization Process
	vector<vector< vector<int> > > ranCorrParam;ranCorrParam.reserve(numTree);
	vector<vector< vector<int> > > ranProbParam;ranProbParam.reserve(numTree);
	unique_ptr< ForestType> forest(new ForestType);
	
	for (unsigned int i=0;i<numTree;i++)
	{	
		vector< vector<int> > tempCorrParam;
		string inHaarFeatureFile = inTreePath + "/Tree_" + convertInt(i+1) + ".mat";
		funLoadMatrix<int>(tempCorrParam,inHaarFeatureFile);
		ranCorrParam.push_back(tempCorrParam);
		
		vector< vector<int> > tempProbParam;
		string inProbFeatureFile = inTreePath + "/Prob_" + convertInt(i+1) + ".mat";
		funLoadMatrix<int>(tempProbParam,inProbFeatureFile);
		ranProbParam.push_back(tempProbParam);
		
		string inTreeFile = inTreePath + "/Tree_" + convertInt(i+1) + ".bin";
		ifstream inTree(inTreeFile.data(),ios::binary);
		if(!inTree) {
			cerr << "could not read tree from " << inTreeFile << endl; exit(-1);
		}
		unique_ptr<typename ForestType::TreeType> tree = ForestType::TreeType::Deserialize(inTree);
		inTree.close();
		tree->CheckValid();
		forest->AddTree(move(tree));
		tree.release();
	}
	
	REDLN("Computing Features....");
	CYNLN("Start classifying the obtained patches simulaneously.");
	vector<vector<float> > predictions(PatchNum,vector<float>(numTarget,0.0));
	
	#pragma omp parallel for ordered 
	for (int j=0;j<PatchNum;j++)
	{
		
		unsigned long int currentPatchNum = j;
		
		// Initialising patch
		location<VectorPixelType> tempLocation = PatchCenter[currentPatchNum];
		
		VectorImageType::RegionType smallRegion;
		smallRegion.SetSize(regionSize);
		VectorImageType::IndexType regionIndex;
		regionIndex[0] = tempLocation.x-radius;
		regionIndex[1] = tempLocation.y-radius;
		regionIndex[2] = tempLocation.z-radius;
		smallRegion.SetIndex(regionIndex);
		
		ConstIteratorType patFMRIPtr(fMRIImage, smallRegion);
		patFMRIPtr.GoToBegin();
		
		vector<Col<float> > PatchContent;
		PatchContent.reserve(PatchSize3);
		
		ConstIteratorType patProbPtr1(probImage1,smallRegion);
		ConstIteratorType patProbPtr2(probImage2,smallRegion);
		ConstIteratorType patProbPtr3(probImage3,smallRegion);
		patProbPtr1.GoToBegin();patProbPtr2.GoToBegin();patProbPtr3.GoToBegin();
		Cube<ImgPixelType> probContent1 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
		Cube<ImgPixelType> probContent2 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
		Cube<ImgPixelType> probContent3 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
		
		
		for (unsigned int kk = 0; kk < PatchSize3; kk++)
		{
			
			VectorImageType::IndexType probIndex1 = patProbPtr1.GetIndex();
			VectorPixelType tempProbValue1 = patProbPtr1.Get();
			probContent1(probIndex1[0]-regionIndex[0],probIndex1[1]-regionIndex[1],probIndex1[2]-regionIndex[2]) = tempProbValue1[0];
			++patProbPtr1;
			
			VectorImageType::IndexType probIndex2 = patProbPtr2.GetIndex();
			VectorPixelType tempProbValue2 = patProbPtr2.Get();
			probContent2(probIndex2[0]-regionIndex[0],probIndex2[1]-regionIndex[1],probIndex2[2]-regionIndex[2]) = tempProbValue2[0];
			++patProbPtr2;
			
			VectorImageType::IndexType probIndex3 = patProbPtr3.GetIndex();
			VectorPixelType tempProbValue3 = patProbPtr3.Get();
			probContent3(probIndex3[0]-regionIndex[0],probIndex3[1]-regionIndex[1],probIndex3[2]-regionIndex[2]) = tempProbValue3[0];
			++patProbPtr3;
			
			vector<float> targetValue;
			targetValue.reserve(fmriVectorLength);
			VectorPixelType tempPatchContent = patFMRIPtr.Get();
			targetValue.insert(std::end(targetValue),tempPatchContent.GetDataPointer(),tempPatchContent.GetDataPointer()+fmriVectorLength);
			PatchContent.push_back(Col<float>(targetValue));
			++patFMRIPtr;
			
		}
		
		for (unsigned int i=0;i<numTree;i++)
		{
			featureClass<VectorPixelType> FeatureIndv = featureClass<VectorPixelType>(currentPatchNum,1,tempLocation);
			FeatureIndv.funAssignCorr(PatchContent,ranCorrParam[i], numCorrFeature);
			FeatureIndv.funAssignProbHaar(probContent1,probContent2,probContent3,ranProbParam[i],numProbFeature);
			
			BRIC::IDEA::FISH::ArrayFunctor<float> functor;
			vector<float> features = FeatureIndv.featureWarpper();
			functor.SetArray(&features[0]);
			const S& stat = forest->GetTree(i)->FastApply(functor);
			vector<float> tempProbs(numTarget);
			stat.GetMean(&tempProbs[0]);
			for (unsigned int jj = 0; jj < numTarget;jj++)
				predictions[j][jj] += tempProbs[jj]/numTree;  
		}
	}
	
	
	// produce result
	VectorImageType::Pointer resTensorImage = ImageInitializer<VectorImageType>(fMRIImage,tensorVectorLength,0);
	
	
	for (int i=0;i<PatchNum;i++)
	{
		VectorPixelType tensorElement;
		VectorImageType::IndexType tensorIndex = PatchCenter[i].GetIndex();
		tensorElement.SetSize(tensorVectorLength);
		for (unsigned int j=0;j<tensorVectorLength;j++)
			tensorElement[j] = predictions[i][j];
		resTensorImage->SetPixel(tensorIndex, tensorElement);
	}
	
	
	string outTensorPath = outputIterPath + "/" + outTensorFile + ".nii.gz";
	ImageWriterPad<VectorImageType>(resTensorImage,outTensorPath,extradius);
	
	YELLN("Finished");
	
	return EXIT_SUCCESS;
}
