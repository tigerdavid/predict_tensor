// Modified from training program just for the damn auto-context training 
#include "common/classes.h"
#include "common/BoostHeaders.h"

// Param Settings for the Trees
#define MAXDEPTH 29
#define MININFOGAIN 0
#define MINLEAFNUM 8
#define NUMTHRES 20

const int numHaarFeature = 1000;
const int numContextFeature = 1000;
const int numProbFeature = 1000;
// const unsigned int confType = 1; // The very baseline of the configurations for this project, using only the very conventional way of RF



int main(int argc, char *argv[])
{
	//initialisation
	if(( argc != 14 )&&(argc != 15 ))
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " WorkingDIR dTensorFileList fTensorFileList ProbFileList1 2 3 ContextFileList RandomSeed SeedSampling PatchNum numTree PatchSize detectorFolder (maskImage)" << endl;
		cerr << "For example: $HOME/ware $HOME/DTICollection/CollectionFile.txt $HOME/fTensorCollection/Collection.txt $HOME/probCollection1 2 3.txt $HOME/ware/currentIter/ContextFiles.txt 1 1 1000 20 11 detector ($HOME/MaskImage.nii.gz)" << endl;
		
		return EXIT_FAILURE;
	}
	
	vector<string> fTensorNames;
	vector<string> dTensorNames;
	vector<string> contextNames;
	vector<string> probNames1;
	vector<string> probNames2;
	vector<string> probNames3;
	
	const string dTensorFileList = argv[2];
	const string fTensorFileList = argv[3];
	const string probFileList1 = argv[4];
	const string probFileList2 = argv[5];
	const string probFileList3 = argv[6];
	const string contextFileList = argv[7];
	
	funLoadVector<string>(fTensorNames,fTensorFileList);
	funLoadVector<string>(dTensorNames,dTensorFileList);
	funLoadVector<string>(contextNames,contextFileList);
	funLoadVector<string>(probNames1,probFileList1);
	funLoadVector<string>(probNames2,probFileList2);
	funLoadVector<string>(probNames3,probFileList3);
	
	const unsigned int count = fTensorNames.size();
	if ((dTensorNames.size()!=count)||(contextNames.size()!=count)||(probNames1.size()!=count)||(probNames2.size()!=count)||(probNames3.size()!=count))
	{
		cerr << "Nums of subjects in those FileLists are different, please double check!" << endl;
		return EXIT_FAILURE;
	}
	
	const int seedNum = atoi(argv[8]);
	const int seedSampling = atoi(argv[9]);
	const unsigned int PatchNum = atoi (argv[10]);
	const unsigned int numTree = atoi(argv[11]); // same number as the numFeature;
	const unsigned int PatchSize = atoi( argv[12]);
// 	const unsigned int PatchSize3 = pow(PatchSize,Dimension);
	const unsigned int radius = (PatchSize - 1)/2;
	
	string outputPath = argv[1];
	string detectorFolder = argv[13];
	string outputIterPath = outputPath +"/currentIter";
	
	
	//check if currentIter folder exists
	mkdir(outputIterPath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	
	// create Tree_*.mat files in the detectorFolder
	string outTreePath = outputIterPath + "/" + detectorFolder;
	mkdir(outTreePath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	vector<vector< vector<int> > > ranHaarParam(numTree);
	vector<vector< vector<int> > > ranContextParam(numTree);
	vector<vector< vector<int> > > ranProbParam(numTree);
	for (unsigned int i=0;i<numTree;i++)
	{
		funInitParamHaar(seedNum+i, PatchSize,numHaarFeature,ranHaarParam[i]);
		string outHaarFeatureFile = outTreePath + "/Tree_" + convertInt(seedNum+i) + ".mat";
		funSaveMatrix<int>(ranHaarParam[i],outHaarFeatureFile);
		
		funInitParamHaar(seedNum+i, PatchSize,numContextFeature,ranContextParam[i]);
		string outContextFeatureFile = outTreePath + "/Context_" + convertInt(seedNum+i) + ".mat";
		funSaveMatrix<int>(ranContextParam[i],outContextFeatureFile);
		
		funInitParamHaar(seedNum+i, PatchSize,numProbFeature,ranProbParam[i]);
		string outProbFeatureFile = outTreePath + "/Prob_" + convertInt(seedNum+i) + ".mat";
		funSaveMatrix<int>(ranProbParam[i],outProbFeatureFile);
	}
	
	vector<VectorImageType::Pointer> fTensorImage(count);
	vector<VectorImageType::Pointer> dTensorImage(count);
	vector<VectorImageType::Pointer> contextImage(count);
	vector<VectorImageType::Pointer> probImage1(count);
	vector<VectorImageType::Pointer> probImage2(count);
	vector<VectorImageType::Pointer> probImage3(count);
	for (unsigned int i=0;i<count;i++)
	{
		fTensorImage[i] = ImageReader<VectorImageType>(fTensorNames[i].data());
		dTensorImage[i] = ImageReader<VectorImageType>(dTensorNames[i].data());
		contextImage[i] = ImageReader<VectorImageType>(contextNames[i].data());
		probImage1[i] = ImageReader<VectorImageType>(probNames1[i].data());
		probImage2[i] = ImageReader<VectorImageType>(probNames2[i].data());
		probImage3[i] = ImageReader<VectorImageType>(probNames3[i].data());
	}
// 	const unsigned int fTensorVectorLength = fTensorImage[0]->GetNumberOfComponentsPerPixel();
	const unsigned int dTensorVectorLength = dTensorImage[0]->GetNumberOfComponentsPerPixel();
	const unsigned int numTarget = dTensorVectorLength;
	
	
	// initialise maskImage if existed; create maskImage with whole region marked if not
	VectorImageType::Pointer maskImage = VectorImageType::New();
	if (argc == 15)
		maskImage = ImageReader<VectorImageType>(argv[14]);
	else
		maskImage = ImageInitializer<VectorImageType>(dTensorImage[0],1,1);
	
	vector<vector<location<VectorPixelType> > >PatchCenter(count);
	for (unsigned int i=0;i<count;i++)
	{
		funTrainingSampling(seedSampling,fTensorImage[i],dTensorImage[i],maskImage,PatchNum,radius,PatchCenter[i]);
		
		string outMapFile = outTreePath + "/map_" + convertInt(i+1) + ".txt";
		ofstream outMap(outMapFile.data());
		for (auto j : PatchCenter[i])
			j.Serialize(outMap);
		outMap.close();
		
		dTensorImage[i] = NULL; // dTensor is eliminated at this stage now to save space
	}
	
	string outParamSettingFile = outTreePath + "/setting.txt";
	ofstream outParamSetting(outParamSettingFile.data());
// 	outParamSetting << numTree << endl; // more settings can be added here if needed
	outParamSetting << PatchSize << endl;
	outParamSetting << dTensorVectorLength << endl;
	outParamSetting << numHaarFeature << endl;
	outParamSetting << numProbFeature << endl;
	outParamSetting << numContextFeature << endl;
	outParamSetting << PatchNum << endl; // just for recording, the PatchNum won't be used in the testing stage
	outParamSetting.close();
	
	
// 	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	const unsigned long AllPatchNum = PatchNum*count;
	BLULN("AllPatchNum is " << AllPatchNum);

	cout << "Simulate feature extraction process to estimate its length." << endl;
	vector<float> tempData,tempTargets;
	featureExtractionProbContext(0,0,PatchCenter[0][0],fTensorImage[0],probImage1[0],probImage2[0],probImage3[0],contextImage[0],tempData,tempTargets,PatchSize,ranHaarParam[0],ranProbParam[0],ranContextParam[0]);
	
	
	// warp the current data available to fit Yaozong's code
	unsigned int numPatchFeatures = tempData.size();
	cout << coldef << "\nFeature Length is: " << numPatchFeatures << endl;
	tempData.clear();tempTargets.clear();
	// 	process_mem_usage();
	
	BRIC::IDEA::FISH::TreeTrainingParameters treeParams;
	treeParams.maxTreeDepth = MAXDEPTH;
	treeParams.minElementsOfLeafNode = MINLEAFNUM;
	treeParams.numOfCandidateThresholdsPerWeakLearner = NUMTHRES;
	treeParams.numOfRandomWeakLearners = numPatchFeatures;
	treeParams.minInformationGain = MININFOGAIN;
	
	for (unsigned int i=0;i<numTree;i++)
	{   
		CYNLN("Working on No." << i+1 << " Tree.");
		boost::timer::auto_cpu_timer t; 
		unsigned int featureSeedNum = seedNum + i;
		BRIC::IDEA::FISH::Random random;
		random.Seed(featureSeedNum);
		
		
		BRIC::IDEA::FISH::MemoryDataCollection trainingData;
		trainingData.SetFeatureNumber(numPatchFeatures);
		trainingData.SetTargetDim(numTarget);
		
		vector<float>& data = trainingData.GetDataVector();
		vector<float>& targets = trainingData.GetTargetVector();
		
		data.reserve(numPatchFeatures*AllPatchNum);
		targets.reserve(numTarget*AllPatchNum);
		
		REDLN("Computing Features");
		
		
		
		#pragma omp parallel for ordered
		for (unsigned int jj = 0;jj < PatchNum; jj++)
		{
			for (unsigned int pp=0;pp<count;pp++)
			{
				// Initialising patch
				unsigned long int currentPatchNum = jj*count+pp;
				vector<float> currentData,currentTargets;
				featureExtractionProbContext(currentPatchNum,pp,PatchCenter[pp][jj],fTensorImage[pp],probImage1[pp],probImage2[pp],probImage3[pp],contextImage[pp],currentData,currentTargets,PatchSize,ranHaarParam[i],ranProbParam[i],ranContextParam[i]);
				copy(begin(currentData),end(currentData),begin(data)+currentPatchNum*numPatchFeatures);
				copy(begin(currentTargets),end(currentTargets),begin(targets)+currentPatchNum*numTarget);
				
			}
		}
		
		
// 		string outDataDebugFile = outputIterPath + "/data_debug_" + convertInt(i+1) + ".txt";
// 		string outLabelsDebugFile = outputIterPath + "/labels_debug_" + convertInt(i+1) + ".txt";
// 		funSaveVector(data,outDataDebugFile);
// 		funSaveVector(targets,outLabelsDebugFile);
		
		trainingData.SetSampleNumber(AllPatchNum);
		
		
		// using the Yaozong's TreeTraining code
		BRIC::IDEA::FISH::SimpleRegressionRandTrainingContext context(numPatchFeatures, numTarget);
		PURLN("Start formal training process");
		std::unique_ptr< TreeType > tree = BRIC::IDEA::FISH::TreeTrainer<W,S>::TrainTree(context, treeParams, &trainingData, random);
		
		// Write tree
		string outTreeFile = outTreePath + "/Tree_" + convertInt(seedNum+i) + ".bin";
		ofstream outTree(outTreeFile.data(),std::ios::binary);
		if(!outTree) cerr << "Fail to write the tree." << endl;
		tree->Serialize(outTree);
		outTree.close();
	}
	
	YELLN("Finished");
	return EXIT_SUCCESS;
}

