// modified from CorrelationTensor.cpp for the specific need when dealing with the ABIDE dataset

#include "common/common.h"

int main(int argc, char *argv[])
{
	//initialisation
	if( argc != 7 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " inputImageFile1  inputImageFile2 refImageFile origImageFile1 origImageFile2 outImageFile" << endl;
		return EXIT_FAILURE;
	}
	
	ImageType::Pointer image1 = ImageReader<ImageType>(argv[1]);
	ImageType::Pointer image2 = ImageReader<ImageType>(argv[2]);
	ImageType::Pointer refImage = ImageReader<ImageType>(argv[3]);
	ImageType::Pointer orig1 = ImageReader<ImageType>(argv[4]);
	ImageType::Pointer orig2 = ImageReader<ImageType>(argv[5]);
	const string outImgFile = argv[6];
	
	ImageType::RegionType inRegion = image1->GetLargestPossibleRegion();
	if ((inRegion!=image2->GetLargestPossibleRegion())|(inRegion!=refImage->GetLargestPossibleRegion()))
	{
		cerr << "inRegion for two input images are not identical" << endl;
		return EXIT_FAILURE;
	}
//	ImageType::RegionType origRegion = orig1->GetLargestPossibleRegion();
//	if (origRegion!=orig2->GetLargestPossibleRegion())
//	{
//		cerr << "origRegion for two original images are not identical" << endl;
//		return EXIT_FAILURE;
//	}
	
	
	typedef itk::ImageRegionConstIterator<ImageType> ConstIteratorType;
	ConstIteratorType imgPtr1(image1,inRegion);
	ConstIteratorType imgPtr2(image2,inRegion);
	ConstIteratorType refPtr(refImage,inRegion);
	vector<double> collectVal1;
	vector<double> collectVal2;
	vector<double> collectRef;
	
	for (imgPtr1.GoToBegin(),imgPtr2.GoToBegin(),refPtr.GoToBegin();!imgPtr1.IsAtEnd();++imgPtr1,++imgPtr2,++refPtr)
	{
		ImgPixelType vecValue1 = imgPtr1.Get();
		ImgPixelType vecValue2 = imgPtr2.Get();
		ImgPixelType refValue = refPtr.Get();
		
		if ((vecValue1!=0)&(vecValue2!=0)&(refValue!=0))
		{
			collectVal1.push_back(vecValue1);	
			collectVal2.push_back(vecValue2);
			collectRef.push_back(refValue);
		}
	}
	
	double resVal1 = as_scalar(cor(vec(collectVal1),vec(collectRef)));
	double resVal2 = as_scalar(cor(vec(collectVal2),vec(collectRef)));
	
	if (resVal1 >= resVal2)
		ImageWriter<ImageType>(orig1, outImgFile);
	else
		ImageWriter<ImageType>(orig2, outImgFile);
	return EXIT_SUCCESS;
	
}
