// PSNR computation between two tensor images

#include "common/common.h"

int main(int argc, char *argv[])
{
	//initialisation
	if( argc != 5 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " inputImageFile1  inputImageFile2 maskFile maskInt" << endl;
		return EXIT_FAILURE;
	}
	
	VectorImageType::Pointer image1 = ImageReader<VectorImageType>(argv[1]);
	VectorImageType::Pointer image2 = ImageReader<VectorImageType>(argv[2]);
	ImageType::Pointer maskImage = ImageReader<ImageType>(argv[3]);
	const float notedMaskValue = atof(argv[4]);
	
	const unsigned int vectorLength = image1->GetNumberOfComponentsPerPixel();
	if (vectorLength!=image2->GetNumberOfComponentsPerPixel())
	{
		cerr << "VectorLength for two images are not identical" << endl;
		return EXIT_FAILURE;
	}
	VectorImageType::RegionType OutRegion = image1->GetLargestPossibleRegion();
	if ((OutRegion!=image2->GetLargestPossibleRegion())&(OutRegion!=maskImage->GetLargestPossibleRegion()))
	{
		cerr << "OutRegion for two images and the maskImage are not identical" << endl;
		return EXIT_FAILURE;
	}
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType imgPtr1(image1,OutRegion);
	ConstIteratorType imgPtr2(image2,OutRegion);
	itk::ImageRegionConstIterator<ImageType> maskPtr(maskImage,maskImage->GetLargestPossibleRegion());

	vector<double> resVal(vectorLength,0.0);
	vector<vector<double> > collectVal1(vectorLength,vector<double>(0));
	vector<vector<double> > collectVal2(vectorLength,vector<double>(0));
    
	for (imgPtr1.GoToBegin(),imgPtr2.GoToBegin(),maskPtr.GoToBegin();!imgPtr1.IsAtEnd();++imgPtr1,++imgPtr2,++maskPtr)
	{
		VectorPixelType vecValue1 = imgPtr1.Get();
		VectorPixelType vecValue2 = imgPtr2.Get();
		ImageType::PixelType maskValue = maskPtr.Get();
		if ((vecValue1[0]!=0)&(vecValue2[0]!=0)&(maskValue==notedMaskValue))
		{
			for (unsigned int i=0;i<vectorLength;i++)
			{
				collectVal1[i].push_back(vecValue1[i]);	
				collectVal2[i].push_back(vecValue2[i]);	
			}
		}
	}
	
	for (unsigned int i=0;i<vectorLength;i++)
	{
		resVal[i] = as_scalar(cor(vec(collectVal1[i]),vec(collectVal2[i])));
		cout << resVal[i] << "\t";
	}
	cout << endl;
	return EXIT_SUCCESS;
	
}
