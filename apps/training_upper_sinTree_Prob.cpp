// modified from training_sinTree_prob, for auto-context model
#include "common/classes.h"
#include "common/BoostHeaders.h"

// Param Settings for the Trees
#define MAXDEPTH 33
#define MININFOGAIN 0
#define MINLEAFNUM 8
#define NUMTHRES 20

const int numCorrFeature = 1000;
const int numProbFeature = 1000;
const int numContextFeature = 1000;
// const int numMeanFeature = 600;


int main(int argc, char *argv[])
{
	//initialisation
	if(( argc != 13 )&&(argc!=14))
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " WorkingDIR TensorFileList fMRIFileList ProbFileList1 2 3 ContextFileList RandomSeed(currTreeNum) SeedSampling PatchNum PatchSize detectorFolder (maskImage)" << endl;
		cerr << "For example: $HOME/ware $HOME/ware/tensor.txt $HOME/ware/fMRI.txt $HOME/probCollection1 2 3.txt $HOME/ware/currentIter/ContextFiles.txt 1 1 1000 11 detector ($HOME/mask.nii.gz)" << endl;
		
		return EXIT_FAILURE;
	}
	
	vector<string> fmriNames;
	vector<string> tensorNames;
	vector<string> contextNames;
	vector<string> probNames1;
	vector<string> probNames2;
	vector<string> probNames3;
	
	const string tensorFileList = argv[2];
	const string fMRIFileList = argv[3];
	const string probFileList1 = argv[4];
	const string probFileList2 = argv[5];
	const string probFileList3 = argv[6];
	const string contextFileList = argv[7];
	
	funLoadVector<string>(fmriNames,fMRIFileList);
	funLoadVector<string>(tensorNames,tensorFileList);
	funLoadVector<string>(contextNames,contextFileList);
	funLoadVector<string>(probNames1,probFileList1);
	funLoadVector<string>(probNames2,probFileList2);
	funLoadVector<string>(probNames3,probFileList3);
	
	const unsigned int count = fmriNames.size();
	if (tensorNames.size()!=count||(contextNames.size()!=count)||(probNames1.size()!=count)||(probNames2.size()!=count)||(probNames3.size()!=count))
	{
		cerr << "Nums of subjects in those FileLists are different, please double check!" << endl;
		return EXIT_FAILURE;
	}
	
	const int seedNum = atoi(argv[8]);
	const int seedSampling = atoi(argv[9]);
	const unsigned int PatchNum = atoi (argv[10]);
	const unsigned int PatchSize = atoi( argv[11]); // Should corresponds to the neighboring window size in my FCT extraction method
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	const unsigned int sradius = 1; // Corresponds to the patch size in my FCT extraction method
	// 	const unsigned int sPatchSize = sradius*2+1;
	// 	const unsigned int sPatchSize3 = pow(sPatchSize,3);
	const unsigned int extradius = radius + sradius; // This extradius is used in the sampling process, since feature extraction for the patches requires larger spaces than radius
	
	srand(seedNum);
	
	string outputPath = argv[1];
	string detectorFolder = argv[12];
	string outputIterPath = outputPath +"/currentIter";
	
	
	//check if currentIter folder exists
	mkdir(outputIterPath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	
	// create Tree_*.mat files in the detectorFolder
	string outTreePath = outputIterPath + "/" + detectorFolder;
	mkdir(outTreePath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	vector< vector<int> > ranCorrParam;
	vector< vector<int> > ranContextParam;
	vector< vector<int> > ranProbParam;
	
	funInitParamCorr(seedNum, PatchSize3,numCorrFeature, ranCorrParam);
	string outCorrFeatureFile = outTreePath + "/Tree_" + convertInt(seedNum) + ".mat";
	funSaveMatrix<int>(ranCorrParam,outCorrFeatureFile);
	
	funInitParamHaar(seedNum, PatchSize,numContextFeature,ranContextParam);
	string outContextFeatureFile = outTreePath + "/Context_" + convertInt(seedNum) + ".mat";
	funSaveMatrix<int>(ranContextParam,outContextFeatureFile);
	
	funInitParamHaar(seedNum, PatchSize,numProbFeature,ranProbParam);
	string outProbFeatureFile = outTreePath + "/Prob_" + convertInt(seedNum) + ".mat";
	funSaveMatrix<int>(ranProbParam,outProbFeatureFile);
	
	// intialize setting.txt file
	// following only to extract tensorVectorLength value. Will consider a better way in the future
	VectorImageType::Pointer tempTensorImage = ImageReader<VectorImageType>(tensorNames[0].data());
	const unsigned int tensorVectorLength = tempTensorImage->GetNumberOfComponentsPerPixel();
	const unsigned int numTarget = tensorVectorLength;
	// 	cout << colred << numTarget << coldef << endl;
	
	
	// initialise maskImage if existed; create maskImage with whole region marked if not
	VectorImageType::Pointer maskImage = VectorImageType::New();
	if (argc == 14)
		maskImage = ImageReader<VectorImageType>(argv[13]);
	else
		maskImage = ImageInitializer<VectorImageType>(tempTensorImage,1,1);
	tempTensorImage = NULL;
	
	string outParamSettingFile = outTreePath + "/setting.txt";
	ofstream outParamSetting(outParamSettingFile.data());
	outParamSetting << PatchSize << endl;
	outParamSetting << tensorVectorLength << endl;
	outParamSetting << numCorrFeature << endl;
	outParamSetting << numProbFeature << endl;
	outParamSetting << numContextFeature << endl;
	outParamSetting << PatchNum << endl; // just for recording, the PatchNum won't be used in the testing stage
	outParamSetting.close();
	
	
	const unsigned long AllPatchNum = PatchNum*count;
	BLULN("AllPatchNum is " << AllPatchNum);
	// Initialising Features from the Patches
	
	unsigned int numPatchFeatures = numCorrFeature + 3*numProbFeature + numContextFeature*6;
	cout << coldef << "\nFeature Length is: " << numPatchFeatures << endl;
	
	BRIC::IDEA::FISH::TreeTrainingParameters treeParams;
	treeParams.maxTreeDepth = MAXDEPTH;
	treeParams.minElementsOfLeafNode = MINLEAFNUM;
	treeParams.numOfCandidateThresholdsPerWeakLearner = NUMTHRES;
	treeParams.numOfRandomWeakLearners = numPatchFeatures;
	treeParams.minInformationGain = MININFOGAIN;
	
	CYNLN("Working on Tree.");
	// 		process_mem_usage();
	boost::timer::auto_cpu_timer t; 
	BRIC::IDEA::FISH::Random random;
	random.Seed(seedNum);
	
	
	BRIC::IDEA::FISH::MemoryDataCollection trainingData;
	trainingData.SetFeatureNumber(numPatchFeatures);
	trainingData.SetTargetDim(numTarget);
	trainingData.SetSampleNumber(AllPatchNum);
	
	vector<float>& data = trainingData.GetDataVector();
	vector<float>& targets = trainingData.GetTargetVector();
	
	data.reserve(numPatchFeatures*AllPatchNum);
	targets.reserve(numTarget*AllPatchNum);
	
	for (unsigned int pp = 0;pp<count;pp++)
	{
		GRNLN("Working on No." << pp + 1 << " Image.");
		
		VectorImageType::Pointer fmriImage = ImageReader<VectorImageType>(fmriNames[pp].data());
		VectorImageType::Pointer tensorImage = ImageReader<VectorImageType>(tensorNames[pp].data());
		VectorImageType::Pointer contextImage = ImageReader<VectorImageType>(contextNames[pp].data());
		VectorImageType::Pointer probImage1 = ImageReader<VectorImageType>(probNames1[pp].data());
		VectorImageType::Pointer probImage2 = ImageReader<VectorImageType>(probNames2[pp].data());
		VectorImageType::Pointer probImage3 = ImageReader<VectorImageType>(probNames3[pp].data());
		
		VectorImageType::Pointer maskFromProb = VectorImageType::New();
		maskFromProb = funGenMaskFromProb(probImage1,probImage2,probImage3,maskImage);
		
		vector <location<VectorPixelType> > PatchCenter;
		funTrainingSampling(seedSampling,fmriImage,tensorImage,maskFromProb,PatchNum,extradius,PatchCenter);
		
		string outMapFile = outputIterPath + "/map_" + convertInt(pp+1) + ".txt";
		ofstream outMap(outMapFile.data());
		for (auto i : PatchCenter)
			i.Serialize(outMap);
		outMap.close();
		tensorImage = NULL; // cleaned to save memory space
		
		
		REDLN("Computing Features");
		
		#pragma omp parallel for ordered
		for (unsigned int jj = 0;jj < PatchNum; jj++)
		{
			unsigned long int currentPatchNum = jj+pp*PatchNum;
			vector<float> currentData,currentTargets;
			featureExtractionFMRIContext(currentPatchNum,pp,PatchCenter[jj],fmriImage,probImage1,probImage2,probImage3, contextImage, currentData, currentTargets,PatchSize,ranCorrParam,ranProbParam,ranContextParam);
			copy(begin(currentData),end(currentData),begin(data)+currentPatchNum*numPatchFeatures);
			copy(begin(currentTargets),end(currentTargets),begin(targets)+currentPatchNum*numTarget);
			
		}
		
	}
	
	
	// using the Yaozong's TreeTraining code
	BRIC::IDEA::FISH::SimpleRegressionRandTrainingContext context(numPatchFeatures, numTarget);
	PURLN("Start formal training process");
	std::unique_ptr< TreeType > tree = BRIC::IDEA::FISH::TreeTrainer<W,S>::TrainTree(context, treeParams, &trainingData, random);
	
	// Write tree
	string outTreeFile = outTreePath + "/Tree_" + convertInt(seedNum) + ".bin";
	ofstream outTree(outTreeFile.data(),std::ios::binary);
	if(!outTree) cerr << "Fail to write the tree." << endl;
	tree->Serialize(outTree);
	outTree.close();
	
	
	YELLN("Finished");
	return EXIT_SUCCESS;
}

