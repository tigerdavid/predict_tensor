// for PSNR evaluation

#include "common.h"

int main(int argc, char *argv[])
{
	
	//initialisation
	if( argc != 3 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " inputImageFile1  inputImageFile2" << endl;
		return EXIT_FAILURE;
	}
	
	VectorImageType::Pointer image1 = ImageReader<VectorImageType>(argv[1]);
	VectorImageType::Pointer image2 = ImageReader<VectorImageType>(argv[2]);
	
	cout << image1 << endl;
	cout << image2 << endl;
	
	const unsigned int vectorLength = image1->GetNumberOfComponentsPerPixel();
	if (vectorLength!=(image2->GetNumberOfComponentsPerPixel()))
	{
		cerr << " VectorLength of two input images are different, please double check!" << endl;
		return EXIT_FAILURE;
	}
	
	VectorImageType::PixelType res = intcompare(image1,image2);
	cout << res << endl;

	
// 	float meandiff = vecintcompare(image1,image2);
// 	cout << meandiff << endl;
	
	return EXIT_SUCCESS;
	
	
}
