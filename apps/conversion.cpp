//First self-made ITK-based program, used to compute the mean of the two hdr images
//Now for simply read in and write out the same image, mainly for format conversion
//finished, all are working fine

#include <iostream>
#include "itkImage.h"
//#include "itkSimpleFilterWatcher.h"
//#include "itkImageRegionIterator.h"
#include "itkImageFileWriter.h"
#include "itkImageFileReader.h"
//#include "itkAddImageFilter.h"
// #include "itkMeanSampleFilter.h"

// #include "vtkSmartPointer.h"
// #include "vtkStructuredPointsReader.h"
// #include "vtkMarchingCubes.h"
// #include "vtkPolyDataConnectivityFilter.h"
// #include "vtkPolyDataMapper.h"
// #include "vtkActor.h"
// #include "vtkProperty.h"
// #include "vtkRenderer.h"
// #include "vtkRenderWindow.h"
// #include "vtkRenderWindowInteractor.h"

// #include "itkRescaleIntensityImageFilter.h"
// 
// #include "QuickView.h"

int main(int argc, char *argv[])
{

    //initialisation
    if( argc < 2 )
    {
        std::cerr << "Usage: " << std::endl;
        std::cerr << argv[0] << " inputImageFile1  inputImageFile2" << std::endl;
        return EXIT_FAILURE;
    }

    const unsigned int Dimension = 3;
//     int masking = 0;

    typedef itk::Image< unsigned char, Dimension > ImageType;
    typedef itk::ImageFileReader<ImageType> ReaderType;
    typedef itk::ImageFileWriter<ImageType> WriterType;

    ReaderType::Pointer reader = ReaderType::New();
    WriterType::Pointer writer = WriterType::New();

    ImageType::Pointer OutImage = ImageType::New();//declared from line "typedef itk::Image< float, 2 >         ImageType;"





    //read the first image
    std::cout << "Reading first image in " << argv[1] << " ....... ";
    reader->SetFileName(argv[1]);
    reader->Update();
    ImageType::Pointer image1=reader->GetOutput();
//     std::cout << "Finished." << std::endl;
    
    
    
//     typedef itk::RescaleIntensityImageFilter< ImageType, ImageType > RescaleFilterType;
//     RescaleFilterType::Pointer rescaleFilter = RescaleFilterType::New();
//     rescaleFilter->SetInput(image1);
//     rescaleFilter->SetOutputMinimum(0);
//     rescaleFilter->SetOutputMaximum(255);
//     rescaleFilter->Update();
//     
//     QuickView viewer;
//     viewer.AddImage(image1.GetPointer());
//     viewer.AddImage(rescaleFilter->GetOutput());
//     viewer.Visualize();

//     //read the second image
//     std::cout << "Reading second image in " << argv[2] << " ....... ";
//     reader->SetFileName(argv[2]);
//     reader->Update();
//     ImageType::Pointer image2=reader->GetOutput();
//     std::cout << "Finished." << std::endl;

    
    

//     //Further initialisation
//     ImageType::RegionType InRegion = image1->GetLargestPossibleRegion();
//     ImageType::SizeType InSize = InRegion.GetSize();
//     ImageType::PixelType InPixelValue1;
//     ImageType::PixelType InPixelValue2;
//     ImageType::IndexType InPixelIndex;
//     ImageType::SpacingType InSpacing = image1->GetSpacing();
//     ImageType::PointType InOrigin;

//     ImageType::RegionType OutRegion = InRegion;
//     ImageType::SizeType OutSize = InSize;
//     ImageType::PixelType OutPixelValue;
//     ImageType::IndexType OutPixelIndex;
//     ImageType::SpacingType OutSpacing;
//     ImageType::PointType OutOrigin;
// 
// 
//     OutPixelIndex[0] = 0;
//     OutPixelIndex[1] = 0;
//     OutPixelIndex[2] = 0;
// 
//     OutRegion.SetSize(OutSize);//must
//     OutImage->CopyInformation(image1);//must
//     OutImage->SetRegions(OutRegion);//must
// //   OutImage->SetDirection(OutDirection);
//     OutImage->Allocate();//must






//     //for mean computation, using iteration, compute the results one by one
//     std::cout << "Start mean computation......." << std::endl;
//     for (int k = 0; k < (int) OutSize[2]; k++)
//     {
//         std::cout << k << std::endl;
//         for (int j = 0; j < (int) OutSize[1]; j++)
//         {
// //             std::cout << j << std::endl;
//             for (int i = 0; i < (int) OutSize[0]; i++)
//             {
// //                 std::cout << i << std::endl;
//                 InPixelIndex[0] = i;
//                 InPixelIndex[1] = j;
//                 InPixelIndex[2] = k;
//                 InPixelValue1 = image1->GetPixel(InPixelIndex);
//                 InPixelValue2 = image2->GetPixel(InPixelIndex);
// 
// 
// 
//                 OutPixelIndex[0] = i;
//                 OutPixelIndex[1] = j;
//                 OutPixelIndex[2] = k;
// 
// //                 if (masking
// //                         && !((MaskPixelValue = MaskImage->GetPixel(InPixelIndex)) > 0))
// //                 {
// //
// //                     OutPixelValue = 0;
// //                 }
// //                 else
// //                 {
//                 OutPixelValue = (InPixelValue1 + InPixelValue2)/2;
// //                 }
// 
//                 OutImage->SetPixel(OutPixelIndex, OutPixelValue);
//             }
//         }
//     }
//     std::cout << "Finished." << std::endl;

//     //show the result
//     QuickView viewer;
//     viewer.AddImage<ImageType>(OutImage);
//     viewer.Visualize();

    //write to the output file
    std::cout << "Saving to " << argv[2] << std::endl;
    writer->SetFileName(argv[2]);
    writer->SetInput(image1);
    writer->Update();

    return EXIT_SUCCESS;
}
