#include "common/common.h"

int main(int argc, char *argv[])
{
	//initialisation
	if( argc != 3 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " inputImageFile1 inputImageIndex" << endl;
		return EXIT_FAILURE;
	}
	
	VectorImageType::Pointer image = ImageReader<VectorImageType>(argv[1]);
	const string index = argv[2];
	
	const unsigned int vectorLength = image->GetNumberOfComponentsPerPixel();
//    cout << vectorLength << endl;
	if (vectorLength > 150)
		cout << index << endl;
	
	return EXIT_SUCCESS;
	
	
}
