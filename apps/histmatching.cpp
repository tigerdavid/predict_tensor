// for histogram matching based on the intensity distribution of the labels under study

#include "common.h"


int main(int argc, char *argv[])
{

	
	//initialisation
	if( argc != 4 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " InputFolder firstInputData lastInputData" << endl;
		cerr << "For example: $HOME/ware 1 90" << endl;
		return EXIT_FAILURE;
	}
	
	vector<string> labelnames;
	vector<string> imagenames;
	
	const int first = atoi( argv[2] );
	const int last  = atoi( argv[3] );
	const int count  = last - first + 1;
	
	string outputPath = argv[1];
	string labelnameformat = outputPath + "/Hippo_%d.nii.gz";
	string imagenameformat = outputPath + "/Image_%d.nii.gz";
	
	
	// Initialising input and output filenames
	typedef itk::NumericSeriesFileNames NameGeneratorType;
	NameGeneratorType::Pointer labelnameGenerator = NameGeneratorType::New();
	labelnameGenerator->SetSeriesFormat( labelnameformat.data() );
	labelnameGenerator->SetStartIndex( first );
	labelnameGenerator->SetEndIndex( last );
	labelnameGenerator->SetIncrementIndex( 1 );
	labelnames = labelnameGenerator->GetFileNames();
	
	NameGeneratorType::Pointer imagenameGenerator = NameGeneratorType::New();
	imagenameGenerator->SetSeriesFormat( imagenameformat.data() );
	imagenameGenerator->SetStartIndex( first );
	imagenameGenerator->SetEndIndex( last );
	imagenameGenerator->SetIncrementIndex( 1 );
	imagenames = imagenameGenerator->GetFileNames();
	
	
	// read the selectlabelFile
	vector<float> selectLabel;
	selectLabel.push_back(1);
	const int labelCount = selectLabel.size();
	
	
	// read all images;
	vector<ReaderType::Pointer> labelReader(count);
	vector<ImageType::Pointer> labelImage(count);
	vector<ReaderType::Pointer> intReader(count);
	vector<ImageType::Pointer> intImage(count);
	
	
	#pragma omp parallel for ordered
	for ( int ii=0;ii<count;ii++)
	{
		labelReader[ii] = ReaderType::New();
		labelImage[ii] = ImageType::New();
		intReader[ii] = ReaderType::New();
		intImage[ii] = ImageType::New();
			
		cout << "Reading " << imagenames[ii] << endl;
		intReader[ii]->SetFileName(imagenames[ii].data());
		intReader[ii]->Update();
		intImage[ii] = intReader[ii]->GetOutput();
		
		cout << "Reading " << labelnames[ii] << endl;
		labelReader[ii]->SetFileName(labelnames[ii].data());
		labelReader[ii]->Update();
		labelImage[ii] = labelReader[ii]->GetOutput();
	}
	
	
	
	
	vector<vector<ImageType::PixelType> > meanLabels(labelCount,vector<ImageType::PixelType>(count,0.0));
	vector<ImageType::PixelType> fmeanLabels(count,0.0);
	for (int ii=0;ii<labelCount;ii++)
	{
		typedef itk::ImageRegionConstIterator<ImageType> ConstIteratorType;
		vector<ConstIteratorType> labelImgPtr;
		vector<ConstIteratorType> intImgPtr;
		for (int i=0;i<count;i++)
		{
			ConstIteratorType tmpIterator1(labelImage[i],labelImage[i]->GetRequestedRegion());
			tmpIterator1.GoToBegin();
			labelImgPtr.push_back(tmpIterator1);
			
			ConstIteratorType tmpIterator2(intImage[i],intImage[i]->GetRequestedRegion());
			tmpIterator2.GoToBegin();
			intImgPtr.push_back(tmpIterator2);
			
		}
		
		int currentLabel = selectLabel[ii];
		ImageType::PixelType ProbPixelValue;
		ImageType::PixelType weight;
		
		for (int i=0;i<count;i++)
		{
			ProbPixelValue=0;
			weight = 0;
			while (!labelImgPtr[i].IsAtEnd())
			{
				if ((labelImgPtr[i].Get())==currentLabel)
				{
					ProbPixelValue+=intImgPtr[i].Get();
					weight++;
				}
				++labelImgPtr[i];
				++intImgPtr[i];
			}
			meanLabels[ii][i] = ProbPixelValue/weight;
		}
	}
	
	// show the intensity difference
	for (int j=0;j<count;j++)
	{
// 		cout << j+1 << "\t";
		float tempValue=0;
		for (int i=0;i<labelCount;i++)
		{
// 			cout << meanLabels[i][j] << "\t";
			tempValue+=meanLabels[i][j];
		}
		fmeanLabels[j] = tempValue/labelCount;
		cout << fmeanLabels[j] << endl;
	}
	
	float allmeanLabel = accumulate(fmeanLabels.begin(),fmeanLabels.end(),0.0)/count;
	cout << allmeanLabel << endl;


	return EXIT_SUCCESS;
}