// modified from conversion.cpp, as the test for padImageFilter

#include "ITKHeaders.h"

int main(int argc, char *argv[])
{
	
	//initialisation
	if( argc < 3 )
	{
		std::cerr << "Usage: " << std::endl;
		std::cerr << argv[0] << " inputImageFile1  inputImageFile2 padSize" << std::endl;
		return EXIT_FAILURE;
	}
	
	VectorReaderType::Pointer reader = VectorReaderType::New();
	VectorWriterType::Pointer writer = VectorWriterType::New();
	


	
	//read the first image
	std::cout << "Reading first image in " << argv[1] << " ....... ";
	reader->SetFileName(argv[1]);
	reader->Update();
	VectorImageType::Pointer refImage=reader->GetOutput();
	//     std::cout << "Finished." << std::endl;
	
	VectorImageType::Pointer resImage = VectorImageType::New();
	resImage = ZeroPad<VectorImageType>(refImage, atoi(argv[3]));
	refImage = NULL;
	
	VectorImageType::Pointer resImage2 = VectorImageType::New();
	resImage2 = rmZeroPad<VectorImageType>(resImage,atoi(argv[3]));
	
	
	
	//write to the output file
	std::cout << "Saving to " << argv[2] << std::endl;
	writer->SetFileName(argv[2]);
	writer->SetInput(resImage2);
	writer->Update();
	
	return EXIT_SUCCESS;
}
