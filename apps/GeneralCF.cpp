// Modified from GeneralRF for the "Classification Forest"


//  #include "stdafx.h"
#include <iostream>
#include "forest/RandomForest.h"
#include "common/BoostHelper.h"
#include "common/ArrayFunctor.h"
#include "common/BoostHeaders.h"

using namespace BRIC::IDEA::FISH;

typedef MemoryAxisAlignedWeakLearner W;
typedef BRIC::IDEA::FISH::HistogramStatisticsAggregator S;

void print_usage()
{
	std::cerr << "[ General Regression Forest Training ]" << std::endl;
	std::cerr << "GeneralRF --train --data file_path --class file_path --tree num --feature num --threshold num --depth num --leaf num --out forest_path --seed id\n" << std::endl;
	
	std::cerr << "[ General Regression Forest Testing ]" << std::endl;
	std::cerr << "GeneralRF --test --data file_path --forest path --out file_path\n" << std::endl;
}

int main(int argc, char** argv)
{
	try {
		using namespace boost::program_options;
		
		options_description generic("Generic options");
		generic.add_options()("help", "display helper information");
		
		options_description functional("Functional options (must pick one)");
		functional.add_options()("train", "training");
		functional.add_options()("test", "testing");
		
		options_description params("Parameter options");
		params.add_options()("data", value<std::string>(), "data path");
		params.add_options()("class", value<std::string>(), "class path");
		params.add_options()("tree", value<unsigned int>(), "number of trees");
		params.add_options()("feature", value<unsigned int>(), "number of random features");
		params.add_options()("threshold", value<unsigned int>(), "number of random thresholds");
		params.add_options()("depth", value<unsigned int>(), "tree depth");
		params.add_options()("leaf", value<unsigned int>(), "leaf num");
		params.add_options()("out", value<std::string>(), "output tree path");
		params.add_options()("seed", value<int>(), "random seed");
		params.add_options()("forest", value<std::string>(), "forest folder");
		
		options_description cmd_options;
		cmd_options.add(generic).add(functional).add(params);
		
		variables_map vm;
		store(command_line_parser(argc, argv).options(cmd_options).style(command_line_style::unix_style ^ command_line_style::allow_short).run(), vm);
		notify(vm);
		
		if (vm.count("help") || argc == 1)
		{
			std::cerr << cmd_options << std::endl;
			print_usage();
			return -1;
		}
		
		if (vm.count("train"))
		{
			std::string data_path = BoostCmdHelper::Get<std::string>(vm, "data");
			std::string class_path = BoostCmdHelper::Get<std::string>(vm, "class");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");
			
			ForestTrainingParameters params;
			params.numTrees = BoostCmdHelper::Get<unsigned int>(vm, "tree");
			params.treeParameters.maxTreeDepth = BoostCmdHelper::Get<unsigned int>(vm, "depth");
			params.treeParameters.numOfRandomWeakLearners = BoostCmdHelper::Get<unsigned int>(vm, "feature");
			params.treeParameters.numOfCandidateThresholdsPerWeakLearner = BoostCmdHelper::Get<unsigned int>(vm, "threshold");
			params.treeParameters.minElementsOfLeafNode = BoostCmdHelper::Get<unsigned int>(vm, "leaf");
			params.treeParameters.minInformationGain = 0;
			
			Random random;
			if (vm.count("seed")) {
				int seed = BoostCmdHelper::Get<int>(vm, "seed");
				random.Seed(seed);
			}
			
			MemoryDataCollection data;
			if (!data.LoadDataFromFile(data_path.c_str())) {
				std::cerr << "fails to load features from " << data_path << std::endl; return -1;
			}
			
			if (!data.LoadLabelsFromFile(class_path.c_str())) {
				std::cerr << "fails to load classs from " << class_path << std::endl; return -1;
			}
			
			// print out feature and class information
			std::cout << "Input feature number: " << data.GetFeatureNumber() << std::endl;
			std::cout << "Input sample number: " << data.GetSampleNumber() << std::endl;
			std::cout << "Input class number: " << data.GetClassNumber() << std::endl;
			
			ClassificationExhaustSearchTrainingContext context(data.GetFeatureNumber(), data.GetClassNumber());
			std::unique_ptr< Forest<W, S> > forest = ForestTrainer<W, S>::TrainForest(context, params, &data, random);
			
			std::ofstream out(out_path.c_str(), std::ios::binary);
			if (!out) {
				std::cerr << "fails to write tree to " << out_path << std::endl; return -1;
			}
			forest->Serialize(out);
			out.close();
		}
		else if (vm.count("test")) {
			
			std::string data_path = BoostCmdHelper::Get<std::string>(vm, "data");
			std::string forest_path = BoostCmdHelper::Get<std::string>(vm, "forest");
			std::string out_path = BoostCmdHelper::Get<std::string>(vm, "out");
			
			unsigned int feature_dimension = 0, sample_number = 0;
			if (!DataCollectionIO::LoadMatrixInfo(data_path.c_str(), feature_dimension, sample_number)) {
				std::cerr << "fail to load matrix info from " << data_path << std::endl; return -1;
			}
			std::cout << "input feature number: " << feature_dimension << std::endl;
			std::cout << "input sample number: " << sample_number << std::endl;
			
			std::vector<float> features;
			try {
				features.resize(static_cast<size_t>(feature_dimension)* sample_number);
			}
			catch ( std::exception& exp ) {
				std::cerr << exp.what() << std::endl; return -1;
			}
			
			if (!DataCollectionIO::LoadMatrix(data_path.c_str(), &features[0], features.size())) {
				std::cerr << "fail to load matrix from " << data_path << std::endl; return -1;
			}
			
			std::ifstream infile(forest_path.c_str(), std::ios::binary);
			if (!infile) {
				std::cerr << "fails to open " << forest_path << std::endl; return -1;
			}
			std::unique_ptr< Forest<W, S> > forest = Forest<W, S>::Deserialize(infile);
			if (forest->GetTreeNumber() <= 0) {
				std::cerr << "empty forest loaded " << std::endl; return -1;
			}
			std::cout << "input tree number: " << forest->GetTreeNumber() << std::endl;
			
			std::vector<float> probabilities;
			const S& stat = forest->GetTree(0)->GetAnyLeafStatisticsAggregator();
			unsigned int class_number = stat.GetClassNumber();
			probabilities.resize(sample_number);
			
			#pragma omp parallel for
			for (int i = 0; i < static_cast<int>(sample_number); ++i)
			{
				std::vector<float> temp_probs(class_number,0.0);
				ArrayFunctor<float> functor;
				functor.SetArray(&features[i*feature_dimension]);
				ForestClassifier::Classify(forest.get(), functor, temp_probs);
				probabilities[i] = std::distance(temp_probs.begin(),std::max_element(temp_probs.begin(), temp_probs.end()));
			}
			
			if (!DataCollectionIO::WriteMatrix(probabilities, class_number, sample_number, out_path.c_str())) {
				std::cerr << "fail to write matrix to " << out_path << std::endl; return -1;
			}
			
		}
		else {
			std::cerr << "no train or test option" << std::endl;
			return -1;
		}
		
	}
	catch (const std::runtime_error& exp) {
		std::cerr << exp.what() << std::endl;
		return -1;
	}
	
	return 0;
}
