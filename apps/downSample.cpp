// To downSample the tensor vectorImage from 1.25X1.25X1.25 to 2X2X2
#include "common/common.h"

int main(int argc, char *argv[])
{
	
	//initialisation
	if( argc != 4 )
	{
		cerr << "Usage: " << argv[0] << " inputTensorFile samplefMRIFile outTensorFile " << endl;
		cerr << "For example: $HOME/ware/tensor.nii.gz $HOME/ware/fMRI.nii.gz $HOME/ware/small_tensor.nii.gz" << endl;
		return EXIT_FAILURE;
	}
	
	string inFile = argv[1];
	string egFile = argv[2];
	string outFile = argv[3];
	
	
	VectorReaderType::Pointer inReader = VectorReaderType::New();
	VectorReaderType::Pointer egReader = VectorReaderType::New();
	VectorImageType::Pointer inImage = VectorImageType::New();
	VectorImageType::Pointer egImage = VectorImageType::New();
	
	inReader->SetFileName(inFile.data());
	inReader->Update();
	inImage = inReader->GetOutput();
	egReader->SetFileName(egFile.data());
	egReader->Update();
	egImage = egReader->GetOutput();
	
	VectorImageType::RegionType InRegion = inImage->GetLargestPossibleRegion();
	VectorImageType::SizeType InSize = InRegion.GetSize();
	VectorImageType::RegionType EgRegion = egImage->GetLargestPossibleRegion();
	VectorImageType::SizeType EgSize = EgRegion.GetSize();
	VectorImageType::SpacingType inSpacing = inImage->GetSpacing();
	VectorImageType::SpacingType egSpacing = egImage->GetSpacing();
	
	const float inResValue = inSpacing[0]; 
	const float egResValue = egSpacing[0];
	cout << "Input Resolution: " << inResValue << endl;
	cout << "Example Resolution: " << egResValue << endl;
	cout << "Input Size: " << InSize << endl;
	cout << "Example Size: " << EgSize << endl;
	
	
	const int tensorLength = inImage->GetNumberOfComponentsPerPixel();
	cout << "Input VoxelLength: " << tensorLength << endl;
	cout << "Example VoxelLenth: " << egImage->GetNumberOfComponentsPerPixel() << endl;
	
	// Initialize output Image
	
	VectorImageType::Pointer outImage = VectorImageType::New();
	outImage->CopyInformation(egImage);
	outImage->SetNumberOfComponentsPerPixel(tensorLength);
	outImage->SetRegions(EgRegion);
	outImage->Allocate();
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType outTensorPtr(outImage,EgRegion);
	outTensorPtr.GoToBegin();
	
	for (outTensorPtr.IsAtBegin();outTensorPtr.IsAtEnd();++outTensorPtr)
	{
		VectorImageType::IndexType outPixelIndex = outTensorPtr.GetIndex();
		
		
	}
	
	
	
	
	
	
	
	
	
	//read the first image
	std::cout << "Reading first image in " << argv[1] << " ....... ";
	reader->SetFileName(argv[1]);
	reader->Update();
	ImageType::Pointer image1=reader->GetOutput();
	//     std::cout << "Finished." << std::endl;
	
	

	
	//write to the output file
	std::cout << "Saving to " << argv[2] << std::endl;
	writer->SetFileName(argv[2]);
	writer->SetInput(image1);
	writer->Update();
	
	return EXIT_SUCCESS;
}
