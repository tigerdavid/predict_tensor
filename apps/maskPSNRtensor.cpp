// PSNR computation between two tensor images

#include "common/common.h"

int main(int argc, char *argv[])
{
	//initialisation
	if( argc != 6 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " inputImageFile1  inputImageFile2 maxVal maskFile maskInt" << endl;
		return EXIT_FAILURE;
	}
	
	VectorImageType::Pointer image1 = ImageReader<VectorImageType>(argv[1]);
	VectorImageType::Pointer image2 = ImageReader<VectorImageType>(argv[2]);
	ImageType::Pointer maskImage = ImageReader<ImageType>(argv[4]);
	const float maxVal = atof(argv[3]);
	const float notedMaskValue = atof(argv[5]);
	
	const unsigned int vectorLength = image1->GetNumberOfComponentsPerPixel();
	if (vectorLength!=image2->GetNumberOfComponentsPerPixel())
	{
		cerr << "VectorLength for two images are not identical" << endl;
		return EXIT_FAILURE;
	}
	VectorImageType::RegionType OutRegion = image1->GetLargestPossibleRegion();
	if (OutRegion!=image2->GetLargestPossibleRegion())
	{
		cerr << "OutRegion for two images are not identical" << endl;
		return EXIT_FAILURE;
	}
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType imgPtr1(image1,OutRegion);
	ConstIteratorType imgPtr2(image2,OutRegion);
    itk::ImageRegionConstIterator<ImageType> maskPtr(maskImage,maskImage->GetLargestPossibleRegion());

	long long longCount = 0;
	double sumVal = 0.0;
	for (maskPtr.GoToBegin(),imgPtr1.GoToBegin(),imgPtr2.GoToBegin();!imgPtr1.IsAtEnd();++imgPtr1,++imgPtr2,++maskPtr)
	{
		VectorPixelType vecValue1 = imgPtr1.Get();
		VectorPixelType vecValue2 = imgPtr2.Get();
		ImageType::PixelType maskValue = maskPtr.Get();
		if ((vecValue1[0]!=0)&(vecValue2[0]!=0)&(maskValue==notedMaskValue))
		{
			longCount++;
			double tempSumVal = 0.0;
			for (unsigned int i=0;i<vectorLength;i++)
				tempSumVal+=pow(vecValue1[i]-vecValue2[i],2);
			sumVal+=tempSumVal/vectorLength;
		}

	}
	double resVal = 20*log10(maxVal/sqrt(sumVal/longCount));
	cout << resVal << endl;
	return EXIT_SUCCESS;
	
}
