// NOTE: For the testing stage, modified from training.cpp


// #define ARMA_DONT_USE_CXX11
#include "common/classes.h"
#include "common/BoostHeaders.h"

// Needed by Yaozong's code (Forest's testing code)
#include "common/ArrayFunctor.h"
#include "forest/tester/ForestClassifier.h"

#define SPACING 1

int main(int argc, char *argv[])
{
	//initialisation
	if(( argc != 9 )&&(argc!=10))
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " WorkingDIR InputFTensor InProb1 2 3 detectorFolder outTensorFile numTree (maskImage)" << endl;
		cerr << "For example: $HOME/ware $HOME/fTensor/fTensor_60.nii.gz $HOME/60/probImage1 2 3.nii.gz detector outTensor_60 20 ($HOME/maskImage.nii.gz)" << endl;
		return EXIT_FAILURE;
	}
	
	const string outputPath = argv[1];
	const string inFTensorFile = argv[2];
	const string inProbFile1 = argv[3];
	const string inProbFile2 = argv[4];
	const string inProbFile3 = argv[5];
	const string detectorFolder = argv[6];
	const string outDTensorFile = argv[7];
	const unsigned int numTree = atoi(argv[8]);
	
	const string outputIterPath = outputPath + "/currentIter";
	const string inTreePath = outputIterPath + "/" + detectorFolder;
	const string inParamSettingFile = inTreePath + "/setting.txt";
	
	boost::timer::auto_cpu_timer t; 
	
	vector<unsigned int> inParamVector;
	funLoadVector<unsigned int>(inParamVector,inParamSettingFile);
	// 	const unsigned int numTree = inParamVector[0];
	const unsigned int PatchSize = inParamVector[0];
	const unsigned int dTensorVectorLength = inParamVector[1];
	const int numHaarFeature = inParamVector[2];
	const int numProbFeature = inParamVector[3];
	const unsigned int numTarget = dTensorVectorLength;
	
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	
	VectorImageType::Pointer fTensorImage = ImageReader<VectorImageType>(inFTensorFile.data());
	const unsigned int fTensorVectorLength = fTensorImage->GetNumberOfComponentsPerPixel();
	
	VectorImageType::Pointer probImage1 = ImageReader<VectorImageType>(inProbFile1.data());
	VectorImageType::Pointer probImage2 = ImageReader<VectorImageType>(inProbFile2.data());
	VectorImageType::Pointer probImage3 = ImageReader<VectorImageType>(inProbFile3.data());
	
	
	// initialise maskImage if existed; create maskImage with whole region marked if not
	VectorImageType::Pointer maskImage = VectorImageType::New();
	if (argc == 10)
		maskImage = ImageReader<VectorImageType>(argv[9]);
	else
		maskImage = ImageInitializer<VectorImageType>(fTensorImage,1,1);
	
	VectorImageType::RegionType OutRegion = fTensorImage->GetLargestPossibleRegion();
	
	vector <location<VectorPixelType> > PatchCenter;   
	// 	patchClass<VectorPixelType> tempPatch;
	
	funTestingSampling(fTensorImage,maskImage,SPACING,radius,PatchCenter);
	
	const int PatchNum = PatchCenter.size();
	BLULN("Selecting " << PatchNum << " patches from the image");
	
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	
	VectorImageType::SizeType regionSize;
	regionSize.Fill(PatchSize);
	
	GRNLN("Reading information from trained classifiers");
	
	// Tree Testing Process
// 	vector<vector< featureClass<VectorPixelType> > > FeatureCollection(numTree, vector<featureClass<VectorPixelType> >(PatchNum));
	vector<vector< vector<int> > > ranHaarParam;ranHaarParam.reserve(numTree);
	vector<vector< vector<int> > > ranProbParam;ranProbParam.reserve(numTree);
	unique_ptr< ForestType> forest(new ForestType);
	
	for (unsigned int i=0;i<numTree;i++)
	{
		
		vector< vector<int> > tempHaarParam;
		string inHaarFeatureFile = inTreePath + "/Tree_" + convertInt(i+1) + ".mat";
		funLoadMatrix<int>(tempHaarParam,inHaarFeatureFile);
		ranHaarParam.push_back(tempHaarParam);
		
		vector< vector<int> > tempProbParam;
		string inProbFeatureFile = inTreePath + "/Prob_" + convertInt(i+1) + ".mat";
		funLoadMatrix<int>(tempProbParam,inProbFeatureFile);
		ranProbParam.push_back(tempProbParam);
		
		string inTreeFile = inTreePath + "/Tree_" + convertInt(i+1) + ".bin";
		ifstream inTree(inTreeFile.data(),ios::binary);
		if(!inTree) {
			cerr << "could not read tree from " << inTreeFile << endl; exit(-1);
		}
		unique_ptr<typename ForestType::TreeType> tree = ForestType::TreeType::Deserialize(inTree);
		inTree.close();
		tree->CheckValid();
		forest->AddTree(move(tree));
		tree.release();
	}
	
	REDLN("Computing Features....");
	CYNLN("Start classifying the obtained patches simulaneously.");
	vector<vector<float> > predictions(PatchNum,vector<float>(numTarget,0.0));
	// 	vector<int> progress(PatchNum,0);
	
	
	
	#pragma omp parallel for ordered 
	for (int j=0;j<PatchNum;j++)
	{
		
		unsigned long int currentPatchNum = j;
		
		// Initialising patch
		location<VectorPixelType> tempLocation = PatchCenter[currentPatchNum];
		
		VectorImageType::RegionType smallRegion;
		smallRegion.SetSize(regionSize);
		VectorImageType::IndexType regionIndex;
		regionIndex[0] = tempLocation.x-radius;
		regionIndex[1] = tempLocation.y-radius;
		regionIndex[2] = tempLocation.z-radius;
		smallRegion.SetIndex(regionIndex);
		
		ConstIteratorType patFTPtr(fTensorImage, smallRegion);
		patFTPtr.GoToBegin();
		ConstIteratorType patProbPtr1(probImage1,smallRegion);
		ConstIteratorType patProbPtr2(probImage2,smallRegion);
		ConstIteratorType patProbPtr3(probImage3,smallRegion);
		patProbPtr1.GoToBegin();patProbPtr2.GoToBegin();patProbPtr3.GoToBegin();
		
		vector<Cube<ImgPixelType> > fTensorContent(fTensorVectorLength);
		for (unsigned int kk=0;kk < fTensorVectorLength;kk++)
			fTensorContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
		Cube<ImgPixelType> probContent1 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
		Cube<ImgPixelType> probContent2 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
		Cube<ImgPixelType> probContent3 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
		
		for (unsigned int kk = 0; kk < PatchSize3; kk++)
		{				
			VectorImageType::IndexType tempIndex = patFTPtr.GetIndex();
			VectorPixelType tempFTensorValue = patFTPtr.Get();
			for (unsigned int jj = 0; jj < fTensorVectorLength;jj++)
				fTensorContent[jj](tempIndex[0]-regionIndex[0],tempIndex[1]-regionIndex[1],tempIndex[2]-regionIndex[2]) = tempFTensorValue[jj];
			++patFTPtr;
			VectorImageType::IndexType probIndex1 = patProbPtr1.GetIndex();
			VectorPixelType tempProbValue1 = patProbPtr1.Get();
			probContent1(probIndex1[0]-regionIndex[0],probIndex1[1]-regionIndex[1],probIndex1[2]-regionIndex[2]) = tempProbValue1[0];
			++patProbPtr1;
			
			VectorImageType::IndexType probIndex2 = patProbPtr2.GetIndex();
			VectorPixelType tempProbValue2 = patProbPtr2.Get();
			probContent2(probIndex2[0]-regionIndex[0],probIndex2[1]-regionIndex[1],probIndex2[2]-regionIndex[2]) = tempProbValue2[0];
			++patProbPtr2;
			
			VectorImageType::IndexType probIndex3 = patProbPtr3.GetIndex();
			VectorPixelType tempProbValue3 = patProbPtr3.Get();
			probContent3(probIndex3[0]-regionIndex[0],probIndex3[1]-regionIndex[1],probIndex3[2]-regionIndex[2]) = tempProbValue3[0];
			++patProbPtr3;
		}
		
		for (unsigned int i=0;i<numTree;i++)
		{
			featureClass<VectorPixelType> FeatureIndv = featureClass<VectorPixelType>(currentPatchNum,1,tempLocation);
			FeatureIndv.funAssignHaar(fTensorContent,ranHaarParam[i], numHaarFeature);
			FeatureIndv.funAssignProbHaar(probContent1,probContent2,probContent3,ranProbParam[i],numProbFeature);
			
			
			BRIC::IDEA::FISH::ArrayFunctor<float> functor;
			vector<float> features = FeatureIndv.featureWarpper();
			functor.SetArray(&features[0]);
			const S& stat = forest->GetTree(i)->FastApply(functor);
			vector<float> tempProbs(numTarget);
			stat.GetMean(&tempProbs[0]);
			for (unsigned int jj = 0; jj < numTarget;jj++)
				predictions[j][jj] += tempProbs[jj]/numTree;  
		}
		
	}           
	
	
	// 	unsigned int numPatchFeatures = FeatureCollection[0][0].featureWarpper().size();
	// 	cout << coldef << "\nFeature Length is: " << numPatchFeatures << endl;
	/*	
	 *	cout << colcyn << "Start classifying the obtained patches." << endl;
	 *	vector<vector<float> > predictions(PatchNum,vector<float>(numTarget,0.0));
	 *	for (int ii = 0; ii < PatchNum; ii++)
	 *	{
	 *		for (unsigned int i = 0; i < numTree; ++i) 
	 *		{
	 *			BRIC::IDEA::FISH::ArrayFunctor<float> functor;
	 *			vector<float> features = FeatureCollection[i][ii].featureWarpper();
	 *			functor.SetArray(&features[0]);
	 *			const S& stat = forest->GetTree(i)->FastApply(functor);
	 *			vector<float> tempProbs(numTarget);
	 *			stat.GetMean(&tempProbs[0]);
	 *			for (unsigned int jj = 0; jj < numTarget;jj++)
	 *				predictions[ii][jj] += tempProbs[jj]/numTree;  
}
}*/
	
	
	// produce result
	VectorImageType::Pointer resTensorImage = ImageInitializer<VectorImageType>(fTensorImage,numTarget,0);
	
	for (int i=0;i<PatchNum;i++)
	{
		VectorPixelType tensorElement;
		VectorImageType::IndexType tensorIndex = PatchCenter[i].GetIndex();
		tensorElement.SetSize(numTarget);
		for (unsigned int j=0;j<numTarget;j++)
			tensorElement[j] = predictions[i][j];
		resTensorImage->SetPixel(tensorIndex, tensorElement);
	}
	
	
	string outTensorPath = outputIterPath + "/" + outDTensorFile + ".nii.gz";
	ImageWriter<VectorImageType>(resTensorImage,outTensorPath);
	
	YELLN("Finished");
	
	return EXIT_SUCCESS;
}
