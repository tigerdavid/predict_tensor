// modified from SelectLabels.cpp, to convert the probability map into binary results

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <numeric>

#include "itkImage.h"
#include "itkSimpleFilterWatcher.h"
#include "itkImageRegionIterator.h"
#include "itkImageFileWriter.h"
#include "itkImageFileReader.h"
#include "itkAddImageFilter.h"

using namespace std;

int main(int argc, char *argv[])
{

    //initialisation
    if( argc != 6 )
    {
        cerr << "Usage: " << endl;
        cerr << argv[0] << " inputImageFile outImageFile lowerNum upperNum labelNum" << endl;
        return EXIT_FAILURE;
    }

    const unsigned int Dimension = 3;
//     int masking = 0;

    typedef itk::Image< unsigned char, Dimension > ImageType;
    typedef itk::ImageFileReader<ImageType> ReaderType;
    typedef itk::ImageFileWriter<ImageType> WriterType;

    ReaderType::Pointer reader = ReaderType::New();
    WriterType::Pointer writer = WriterType::New();

    const ImageType::PixelType lowerNum = atoi(argv[3]);
    const ImageType::PixelType upperNum = atoi(argv[4]);
    const int labelNum = atoi(argv[5]);

    //read the first image
    cout << "Reading first image in " << argv[1] << " ....... " << endl;
    reader->SetFileName(argv[1]);
    reader->Update();
    ImageType::Pointer image1=reader->GetOutput();

     //Further initialisation
     ImageType::RegionType InRegion = image1->GetLargestPossibleRegion();
     ImageType::SizeType InSize = InRegion.GetSize();
     ImageType::PixelType InPixelValue1;
     ImageType::IndexType InPixelIndex;


     for (int k = 0; k < (int) InSize[2]; k++)
     {
         for (int j = 0; j < (int) InSize[1]; j++)
         {
             for (int i = 0; i < (int) InSize[0]; i++)
             {
                 InPixelIndex[0] = i;
                 InPixelIndex[1] = j;
                 InPixelIndex[2] = k;
                 InPixelValue1 = image1->GetPixel(InPixelIndex);
                 if ((InPixelValue1 >=lowerNum) && (InPixelValue1 <= upperNum))
                    InPixelValue1 = labelNum;
		 else
		   InPixelValue1 = 0;
                 image1->SetPixel(InPixelIndex, InPixelValue1);
             }
         }
     }

    //write to the output file
    writer->SetFileName(argv[2]);
    writer->SetInput(image1);
    writer->Update();
    cout << "Finished." << endl;

    return EXIT_SUCCESS;
}

