// simulate the registration

#include "common/classes.h"
#include "common/BoostHeaders.h"


int main(int argc, char *argv[])
{
	
	//initialisation
	if( argc != 4 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " inputImageFile  out2inXFMFile outImageFile" << endl;
		return EXIT_FAILURE;
	}
	
	const string inImageFile = argv[1];
	const string out2inXFMFile = argv[2];
	VectorImageType::Pointer inImage = ImageReader<VectorImageType>(inImageFile);
	const string outImageFile = argv[3];
	
	Mat<float> xfm(4,4);
	xfm.load(out2inXFMFile);
// 	cout << xfm << endl;
	
	VectorImageType::IndexType inIndex,outIndex;
	VectorImageType::PointType inPoint,outPoint;
// 	VectorImageType::SpacingType inSpacing = inImage->GetSpacing();
	
	VectorImageType::Pointer outImage = ImageInitializer<VectorImageType>(inImage,6,0);
	
	typedef itk::ImageRegionIterator<VectorImageType> IteratorType;
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	
	VectorImageType::RegionType inRegion = inImage->GetLargestPossibleRegion();
	VectorImageType::SizeType inSize = inRegion.GetSize();
	ConstIteratorType inPtr(inImage,inRegion);
	IteratorType outPtr(outImage,inRegion);
	
	
// 	#pragma omp parallel for ordered
	for (int k=0;k< (int)inSize[2];k++)
		for (int j=0;j<(int)inSize[1];j++)
			for (int i=0;i<(int)inSize[0];i++)
			{
				VectorImageType::IndexType outIndex;
				outIndex[0] = i;
				outIndex[1] = j;
				outIndex[2] = k;
				
// 				inImage->TransformIndexToPhysicalPoint( outIndex,outPoint);
// 				Col<float> arma_outPoint(4);
// 				arma_outPoint << outPoint[0] << outPoint[1] << outPoint[2] << 1;
// 				Col<float> arma_inPoint = xfm * arma_outPoint;
// 				inPoint[0] = arma_inPoint[0];
// 				inPoint[1] = arma_inPoint[1];
// 				inPoint[2] = arma_inPoint[2];
// 				inImage->TransformPhysicalPointToIndex(inPoint,inIndex);
				
				Col<float> arma_outIndex(4);
// 				arma_outIndex << outIndex[0]*inSpacing[0] << outIndex[1]*inSpacing[1] << outIndex[2]*inSpacing[2] << 1;
				arma_outIndex << outIndex[0] << outIndex[1] << outIndex[2] << 1;
				Col<float> arma_inIndex = xfm * arma_outIndex;
// 				inIndex[0] = round(arma_inIndex[0]/inSpacing[0]);
// 				inIndex[1] = round(arma_inIndex[1]/inSpacing[1]);
// 				inIndex[2] = round(arma_inIndex[1]/inSpacing[2]);
				inIndex[0] = round(arma_inIndex[0]);
				inIndex[1] = round(arma_inIndex[1]);
				inIndex[2] = round(arma_inIndex[2]);

				if (inRegion.IsInside(inIndex))
				{
					VectorImageType::PixelType inValue = inImage->GetPixel(inIndex);
					outImage->SetPixel(outIndex,inValue);
				}
			}
	
	ImageWriter<VectorImageType>(outImage,outImageFile);
	
	return EXIT_SUCCESS;
	
	
}
