// to fuse the results from the tripleTrain

#include "common.h"

int main(int argc, char *argv[])
{
	if (argc != 5)
	{
		std::cerr << "Usage: " << argv[0] << " inImageFileFormat firstNum lastNum outMeanImageFile" << std::endl;
		return EXIT_FAILURE;
	}
	string inFileFormat = argv[1];
	const int first = atoi( argv[2] );
	const int last  = atoi( argv[3] );
	const string outFilename = argv[4];
	vector<string> inNames;
	
	// Initialising input and output filenames
	typedef itk::NumericSeriesFileNames NameGeneratorType;
	NameGeneratorType::Pointer inNameGenerator = NameGeneratorType::New();
	inNameGenerator->SetSeriesFormat(inFileFormat.data() );
	inNameGenerator->SetStartIndex( first );
	inNameGenerator->SetEndIndex( last );
	inNameGenerator->SetIncrementIndex( 1 );
	inNames = inNameGenerator->GetFileNames();
	const int count = inNames.size();
	vector<VectorImageType::Pointer> inImage(count);
	
	
	for (int i=0;i<count;i++)
		inImage[i] = ImageReader<VectorImageType>(inNames[i]);

	VectorImageType::Pointer medianImage = ImageInitializer<VectorImageType>(inImage[0],0,0);
	const int vectorLength = medianImage->GetNumberOfComponentsPerPixel();
	
	typedef itk::ImageRegionIterator<VectorImageType> IteratorType;
	
	   IteratorType medianPtr(medianImage,medianImage->GetLargestPossibleRegion());
	
	for (medianPtr.GoToBegin();!medianPtr.IsAtEnd();++medianPtr)
	{
		VectorImageType::PixelType medianValue;
        medianValue.SetSize(vectorLength);
		VectorImageType::IndexType currentIndex = medianPtr.GetIndex();
		for (int i=0;i<vectorLength;i++)
		{
			vector<float> valCollection(count);
			for (int j=0;j<count;j++)
				valCollection[j] = inImage[j]->GetPixel(currentIndex)[i];
			nth_element(valCollection.begin(),valCollection.begin() + count/2,valCollection.end());
			if (count %2 == 0)
				medianValue[i] = (valCollection[count/2-1]/2 + valCollection[count/2]/2);
			else
				medianValue[i] = valCollection[(count-1)/2];
		}
		medianPtr.Set(medianValue);
		
	}
	
	ImageWriter<VectorImageType>(medianImage,outFilename);
	
	return EXIT_SUCCESS;
}


