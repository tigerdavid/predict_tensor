// obtain the FC matrix using the given fMRI & label map

#include "common/common.h"

int main(int argc, char *argv[])
{
	//initialisation
	if( argc != 5 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " inputfMRI inputLabels outMatrix labelFile" << endl;
		return EXIT_FAILURE;
	}
	
	VectorImageType::Pointer inFMRIFile = ImageReader<VectorImageType>(argv[1]);
	intImageType::Pointer inLabelMap = ImageReader<intImageType>(argv[2]);
	string outMatFile = argv[3];
	string labelListFile = argv[4];
	
	vector<int> labelList;
	funLoadVector<int>(labelList,labelListFile);
	unsigned int labelCount = labelList.size();
	
	cout << "The selected labels are: " << endl;
	for (unsigned int i = 0;i<labelCount;i++)
		cout << labelList[i] << " ";
	cout << endl;
	
	VectorImageType::RegionType OutRegion = inFMRIFile->GetLargestPossibleRegion();
	if (OutRegion!=inLabelMap->GetLargestPossibleRegion())
	{
		cerr << "OutRegion for inputfMRI and inLabels are not identical" << endl;
		return EXIT_FAILURE;
	}
	
	const unsigned int vectorLength = inFMRIFile->GetNumberOfComponentsPerPixel();
	
	vector<long> labelDistri(labelCount+1,0);
	vector<Col<double> > collectVal;
	collectVal.reserve(labelCount+1);
	for (unsigned int i=0;i<=labelCount;i++)
	{
		Col<double> tempVal; tempVal.zeros(vectorLength);
		collectVal.push_back(tempVal);
	}
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType FMRIPtr(inFMRIFile,OutRegion);
	itk::ImageRegionConstIterator<intImageType> LabPtr(inLabelMap,inLabelMap->GetLargestPossibleRegion());
	
	for (FMRIPtr.GoToBegin(),LabPtr.GoToBegin();!FMRIPtr.IsAtEnd();++FMRIPtr,++LabPtr)
	{
		VectorPixelType FMRIVal = FMRIPtr.Get();
		intImageType::PixelType LabVal = LabPtr.Get();
		unsigned int indLab = distance(labelList.begin(),find(labelList.begin(), labelList.end(), LabVal));
		
		VectorImageType::IndexType FMRIInd = FMRIPtr.GetIndex();
		
		if (indLab!=0)
		{
			labelDistri[indLab]++;
			for (unsigned int i=0;i<vectorLength;i++)
				collectVal[indLab][i]+=FMRIVal[i];
		}	
	}
	
	for (unsigned int i=1;i<=labelCount;i++)
		for (unsigned int j=0;j<vectorLength;j++)
			collectVal[i][j]/=labelDistri[i];
		
	vector<vector<double> > FCmap(labelCount,vector<double>(labelCount,0.0));
	for (unsigned int i=1;i<=labelCount;i++)
		for (unsigned int j=1;j<=labelCount;j++)
			FCmap[i-1][j-1]=as_scalar(cor(collectVal[i], collectVal[j]));
		
	funSaveMatrix<double>(FCmap, outMatFile);
	return EXIT_SUCCESS;
	
}
