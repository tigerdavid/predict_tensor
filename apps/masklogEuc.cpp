// log-Euclidean computation between two tensor images, as requested by Yap

#include "common/common.h"
double precision=0.0000000000001;

int main(int argc, char *argv[])
{
	//initialisation
	if( argc != 6 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " inputImageFile1 inputImageFile2 maskFile maskInt outImageFile" << endl;
		return EXIT_FAILURE;
	}
	
	VectorImageType::Pointer image1 = ImageReader<VectorImageType>(argv[1]);
	VectorImageType::Pointer image2 = ImageReader<VectorImageType>(argv[2]);
	ImageType::Pointer maskImage = ImageReader<ImageType>(argv[3]);
    ImageType::Pointer outImage = ImageType::New();
    outImage->CopyInformation(maskImage);
    outImage->SetRegions(maskImage->GetLargestPossibleRegion());
    outImage->Allocate();
    outImage->FillBuffer(0);



	const float notedMaskValue = atof(argv[4]);
    const string outImageFile = argv[5];
	
	const unsigned int vectorLength = image1->GetNumberOfComponentsPerPixel();
	if (vectorLength!=image2->GetNumberOfComponentsPerPixel())
	{
		cerr << "VectorLength for two images are not identical" << endl;
		return EXIT_FAILURE;
	}
	VectorImageType::RegionType OutRegion = image1->GetLargestPossibleRegion();
	if ((OutRegion!=image2->GetLargestPossibleRegion())&(OutRegion!=maskImage->GetLargestPossibleRegion()))
	{
		cerr << "OutRegion for two images and the maskImage are not identical" << endl;
		return EXIT_FAILURE;
	}
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType imgPtr1(image1,OutRegion);
	ConstIteratorType imgPtr2(image2,OutRegion);
	itk::ImageRegionConstIterator<ImageType> maskPtr(maskImage,maskImage->GetLargestPossibleRegion());
	
	long long longCount = 0;
	double sumVal = 0.0;
	for (imgPtr1.GoToBegin(),imgPtr2.GoToBegin(),maskPtr.GoToBegin();!imgPtr1.IsAtEnd();++imgPtr1,++imgPtr2,++maskPtr)
	{
		VectorPixelType vec1 = imgPtr1.Get();
		VectorPixelType vec2 = imgPtr2.Get();
		ImageType::PixelType maskValue = maskPtr.Get();
		if ((vec1[0]>precision)&(vec2[0]>precision)&(maskValue==notedMaskValue))
		{
			longCount++;
			arma::mat mat1 = { {vec1[0],vec1[1],vec1[2]},{vec1[1],vec1[3],vec1[4]},{vec1[2],vec1[4],vec1[5]} };
			arma::mat mat2 = { {vec2[0],vec2[1],vec2[2]},{vec2[1],vec2[3],vec2[4]},{vec2[2],vec2[4],vec2[5]} };
//            cout << mat1 << "\t" << mat2 << endl;
			double tempVal = (arma::norm((arma::logmat(mat1)-arma::logmat(mat2)),"fro"));
			double tempSumVal = pow(tempVal,2);
			sumVal+=tempSumVal;	
            outImage->SetPixel(maskPtr.GetIndex(),tempSumVal);
		}
	}
	
	double resVal = sumVal/longCount;
	cout << resVal << endl;
    ImageWriter<ImageType>(outImage,outImageFile);

	return EXIT_SUCCESS;
	
}
