#!/bin/bash - 
#===============================================================================
#
#          FILE: TripleTrain.sh
# 
#         USAGE: ./TripleTrain.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 05/29/2016 02:20
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -e

waiting(){
    while [ ! -z "$(bjobs)"  ]
    do
        sleep 10
    done

}

START=$(date +%s.%N)
echo Start time: $(date)

rm $HOME/ware_triple[1-3]/*.txt
for ((k=1;k<=3;k++))
do
    for j in $(cat ~/group$k ); do ls $HOME/mountdata2/FunImgF3/$j/tensor.nii >> $HOME/ware_triple$k/fTensor.txt; done
    for j in $(cat ~/group$k ); do ls $HOME/mountdata2/smaller-tensors3/$j/out_tensor.nii >> $HOME/ware_triple$k/dTensor.txt; done
done

for ((i=0;i<=3;i++))
do
    for j in $(cat ~/group3 ); do echo $HOME/ware_triple1/iter$i/outTensor_$j.nii.gz >> $HOME/ware_triple3/iter$i.txt; done
    for j in $(cat ~/group1 ); do echo $HOME/ware_triple2/iter$i/outTensor_$j.nii.gz >> $HOME/ware_triple1/iter$i.txt; done
    for j in $(cat ~/group2 ); do echo $HOME/ware_triple3/iter$i/outTensor_$j.nii.gz >> $HOME/ware_triple2/iter$i.txt; done
done

export OMP_NUM_THREADS=8
for ((k=1;k<=3;k++))
do
    bsub -o $HOME/mountdata2/outlog/$k.out -k $HOME/mountdata2/outlog -n 8 -M 35 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Training $HOME/ware_triple$k $HOME/ware_triple$k/dTensor.txt $HOME/ware_triple$k/fTensor.txt 1 15000 20 11 detector
done
waiting

for ((k=1;k<=3;k++))
do
    for ((j=1;j<=81;j++)); do bsub -o $HOME/mountdata2/outlog/$k\_$j.out -k $HOME/mountdata2/outlog -n 8 -M 45 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Testing $HOME/ware_triple$k $HOME/mountdata2/formal_ADNI_fMRI/FunImgF/$j/tensor.nii detector outTensor_$j;done
done

for j in $(cat ~/group3 ); do bsub -o $HOME/mountdata2/outlog/$j.out -k $HOME/mountdata2/outlog -n 8 -M 45 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Testing $HOME/ware_triple1 $HOME/mountdata2/FunImgF3/$j/tensor.nii detector outTensor_$j;done
for j in $(cat ~/group1 ); do bsub -o $HOME/mountdata2/outlog/$j.out -k $HOME/mountdata2/outlog -n 8 -M 45 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Testing $HOME/ware_triple2 $HOME/mountdata2/FunImgF3/$j/tensor.nii detector outTensor_$j;done
for j in $(cat ~/group2 ); do bsub -o $HOME/mountdata2/outlog/$j.out -k $HOME/mountdata2/outlog -n 8 -M 45 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Testing $HOME/ware_triple3 $HOME/mountdata2/FunImgF3/$j/tensor.nii detector outTensor_$j;done
waiting

export OMP_NUM_THREADS=8
for ((i=1;i<=2;i++))
do
    for ((k=1;k<=3;k++))
    do
        mv $HOME/ware_triple$k/currentIter $HOME/ware_triple$k/iter$i
        bsub -o $HOME/mountdata2/outlog/$i\_$k.out -k $HOME/mountdata2/outlog -n 8 -M 60 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Training $HOME/ware_triple$k $HOME/ware_triple$k/dTensor.txt $HOME/ware_triple$k/fTensor.txt $HOME/ware_triple$k/iter$i.txt 1 15000 20 11 detector
    done
    waiting

    for ((k=1;k<=3;k++))
    do
        for ((j=1;j<=81;j++)); do bsub -o $HOME/mountdata2/outlog/$i\_$k\_$j.out -k $HOME/mountdata2/outlog -n 8 -M 75 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware_triple$k $HOME/mountdata2/formal_ADNI_fMRI/FunImgF/$j/tensor.nii $HOME/ware_triple$k/iter$i/outTensor_$j.nii.gz detector outTensor_$j;done
    done
    for j in $(cat ~/group3 ); do bsub -q hour -o $HOME/mountdata2/outlog/$i\_$k\_$j.out -k $HOME/mountdata2/outlog -n 8 -M 75 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware_triple1 $HOME/mountdata2/FunImgF3/$j/tensor.nii $HOME/ware_triple1/iter$i/outTensor_$j.nii.gz detector outTensor_$j;done
    for j in $(cat ~/group1 ); do bsub -q hour -o $HOME/mountdata2/outlog/$i\_$k\_$j.out -k $HOME/mountdata2/outlog -n 8 -M 75 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware_triple2 $HOME/mountdata2/FunImgF3/$j/tensor.nii $HOME/ware_triple2/iter$i/outTensor_$j.nii.gz detector outTensor_$j;done
    for j in $(cat ~/group2 ); do bsub -q hour -o $HOME/mountdata2/outlog/$i\_$k\_$j.out -k $HOME/mountdata2/outlog -n 8 -M 75 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware_triple3 $HOME/mountdata2/FunImgF3/$j/tensor.nii $HOME/ware_triple3/iter$i/outTensor_$j.nii.gz detector outTensor_$j;done
    waiting
done

END=$(date +%s.%N)
echo End time: $(date)
DIFF=$(echo "$END - $START" | bc)
echo Total execution time: $DIFF
