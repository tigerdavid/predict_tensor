#!/bin/bash - 
#===============================================================================
#
#          FILE: TripleTrain.sh
# 
#         USAGE: ./TripleTrain.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 05/29/2016 02:20
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -e

waiting(){
    while [ ! -z "$(bjobs )"  ]
    do
        sleep 10
    done

}

export OMP_NUM_THREADS=10
START=$(date +%s.%N)
echo Start time: $(date)

case "$1" in
    1)
        fileCollection=$(cat ~/group2 ~/group3 ~/group4)
        ;;
    2)
        fileCollection=$(cat ~/group1 ~/group3 ~/group4)
        ;;
    3)
        fileCollection=$(cat ~/group1 ~/group2 ~/group4)
        ;;
    4)
        fileCollection=$(cat ~/group1 ~/group2 ~/group3)
        ;;
    *)
        echo "Unknown command. Exit"
        exit 1
esac

# # rm $HOME/ware_HCP$1/*.txt
# for j in $fileCollection; do ls $HOME/mountdata2/FunImgF/$j/tensor.nii.gz >> $HOME/ware_HCP$1/fTensor.txt; done
# for j in $fileCollection; do ls $HOME/mountdata2/smaller-tensors/$j/out_tensor.nii.gz >> $HOME/ware_HCP$1/dTensor.txt; done
# for ((i=0;i<=2;i++))
# do
#     for j in $fileCollection; do echo $HOME/ware_HCP$1/iter$i/outTensor_$j.nii.gz >> $HOME/ware_HCP$1/iter$i.txt; done
# done
# 
# for k in {1..20}
# do
#     bsub -o $HOME/mountdata2/outlog/training.out -k $HOME/mountdata2/outlog -n 8 -M 55 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Training $HOME/ware_HCP$1 $HOME/ware_HCP$1/dTensor.txt $HOME/ware_HCP$1/fTensor.txt $k 1 30000 1 11 detector
# done
# waiting
# for j in $(cat ~/all); do bsub -o $HOME/mountdata2/outlog/$j.out -k $HOME/mountdata2/outlog -n 8 -M 10 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Testing $HOME/ware_HCP$1 $HOME/mountdata2/FunImgF/$j/tensor.nii detector outTensor_$j 20;done
# waiting

for ((i=1;i<=1;i++))
do
    mv $HOME/ware_HCP$1/currentIter $HOME/ware_HCP$1/iter$i

    for k in {1..20}
    do
        bsub -o $HOME/mountdata2/outlog/training_$i.out -k $HOME/mountdata2/outlog -n 8 -M 115 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Training $HOME/ware_HCP$1 $HOME/ware_HCP$1/dTensor.txt $HOME/ware_HCP$1/fTensor.txt $HOME/ware_HCP$1/iter$i.txt $k 1 30000 1 11 detector
    done
    waiting
    for j in $(cat ~/all ); do bsub -o $HOME/mountdata2/outlog/$i\_$j.out -k $HOME/mountdata2/outlog -n 8 -M 15 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware_HCP$1 $HOME/mountdata2/FunImgF/$j/tensor.nii $HOME/ware_HCP$1/iter$i/outTensor_$j.nii.gz detector outTensor_$j 20;done
    waiting
done

END=$(date +%s.%N)
echo End time: $(date)
DIFF=$(echo "$END - $START" | bc)
echo Total execution time: $DIFF
