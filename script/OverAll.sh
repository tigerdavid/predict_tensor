#!/bin/bash - 
#===============================================================================
#
#          FILE: OverAll.sh
# 
#         USAGE: ./OverAll.sh StartRndNum EndRndNum
# 
#   DESCRIPTION: The overall process of my work
# 
#       OPTIONS: EG: ./OverAll.sh 1 2 -- Start from Round 1 to Round 2
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang, 
#  ORGANIZATION: Med-X, SJTU
#       CREATED: 02/13/14 01:54
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -e
WORKFOLDER=$3

for ((i=$1;i<=$2;i++))
do
    mv $WORKFOLDER $WORKFOLDER\_$i
    mkdir $WORKFOLDER
    cp -v $WORKFOLDER\_$i/*.nii.gz $WORKFOLDER
    cp -v $WORKFOLDER\_$i/index*.* $WORKFOLDER
    cp -v $WORKFOLDER\_$i/tumor.dat $WORKFOLDER

    ./xval.sh
done
