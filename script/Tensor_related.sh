#!/bin/bash - 
#===============================================================================
#
#          FILE: Tensor_related.sh
# 
#         USAGE: ./Tensor_related.sh 
# 
#   DESCRIPTION: Compute all the necesary files using dtifit 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 04/13/2016 21:33
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
for i in *
do
    cd $i
    dtifit -k data.nii -m nodif_brain_mask.nii.gz -o out -r bvecs -b bvals --save_tensor
    cd ..
done

