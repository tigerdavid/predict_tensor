#!/bin/bash - 
#===============================================================================
#
#          FILE: computeTensor.sh
# 
#         USAGE: ./computeTensor.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 05/23/2016 12:43
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
job_limit () {
    # Test for single positive integer input
    if (( $# == 1 )) && [[ $1 =~ ^[1-9][0-9]*$ ]]
    then

        # Check number of running jobs
        joblist=($(jobs -rp))
        while (( ${#joblist[*]} >= $1 ))
        do

            # Wait for any job to finish
            command='wait '${joblist[0]}
            for job in ${joblist[@]:1}
            do
                command+=' || wait '$job
            done
            eval $command
            joblist=($(jobs -rp))
        done
    fi
}

for i in $(cat ~/$1)
# for ((i=$1;i<=$2;i++))
do
   matlab -nosplash -nodesktop -r "misc_function($i)" &
   job_limit 8
done
