#!/bin/bash - 
#===============================================================================
#
#          FILE: transpose.sh
# 
#         USAGE: ./transpose.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 04/13/2016 16:40
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

awk '
{ 
    for (i=1; i<=NF; i++)  {
        a[NR,i] = $i
    }
}
NF>p { p = NF }
END {    
for(j=1; j<=p; j++) {
    str=a[1,j]
    for(i=2; i<=NR; i++){
        str=str" "a[i,j];
    }
    print str
}
    }' $1 
