#!/bin/bash - 
#===============================================================================
#
#          FILE: TripleTrain.sh
# 
#         USAGE: ./TripleTrain.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 05/29/2016 02:20
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -e

rm $HOME/ware_triple[1-3]/*.txt
for ((k=1;k<=3;k++))
do
    for j in $(cat ~/group$k ); do ls $HOME/mountdata/FunImgF/$j/tensor.nii >> $HOME/ware_triple$k/fTensor.txt; done
    for j in $(cat ~/group$k ); do ls $HOME/mountdata/smaller-tensors/$j/out_tensor.nii.gz >> $HOME/ware_triple$k/dTensor.txt; done
done

for ((i=0;i<=3;i++))
do
    for j in $(cat ~/group3 ); do echo $HOME/ware_triple1/iter$i/outTensor_$j.nii.gz >> $HOME/ware_triple3/iter$i.txt; done
    for j in $(cat ~/group1 ); do echo $HOME/ware_triple2/iter$i/outTensor_$j.nii.gz >> $HOME/ware_triple1/iter$i.txt; done
    for j in $(cat ~/group2 ); do echo $HOME/ware_triple3/iter$i/outTensor_$j.nii.gz >> $HOME/ware_triple2/iter$i.txt; done
done

for ((k=1;k<=3;k++))
do
    $HOME/projects/predict_dev/Map_Training $HOME/ware_triple$k $HOME/ware_triple$k/dTensor.txt $HOME/ware_triple$k/fTensor.txt 1 30000 20 11 detector

    for j in $(cat ~/group4 ); do $HOME/projects/predict_dev/Map_Testing $HOME/ware_triple$k $HOME/mountdata/FunImgF/$j/tensor.nii detector outTensor_$j;done
    #     for ((j=1;j<=81;j++)); do $HOME/projects/predict_dev/Map_Testing $HOME/ware_triple$k $HOME/mountdata/formal_ADNI_fMRI/FunImgF/$j/tensor.nii.gz detector outTensor_$j;done
done

for j in $(cat ~/group3 ); do $HOME/projects/predict_dev/Map_Testing $HOME/ware_triple1 $HOME/mountdata/FunImgF/$j/tensor.nii detector outTensor_$j;done
for j in $(cat ~/group1 ); do $HOME/projects/predict_dev/Map_Testing $HOME/ware_triple2 $HOME/mountdata/FunImgF/$j/tensor.nii detector outTensor_$j;done
for j in $(cat ~/group2 ); do $HOME/projects/predict_dev/Map_Testing $HOME/ware_triple3 $HOME/mountdata/FunImgF/$j/tensor.nii detector outTensor_$j;done

for ((i=0;i<=2;i++))
do
    for ((k=1;k<=3;k++))
    do
        mv $HOME/ware_triple$k/currentIter $HOME/ware_triple$k/iter$i
    done
    for ((k=1;k<=3;k++))
    do
        $HOME/projects/predict_dev/Map_Upper_Training $HOME/ware_triple$k $HOME/ware_triple$k/dTensor.txt $HOME/ware_triple$k/fTensor.txt $HOME/ware_triple$k/iter$i.txt 1 30000 20 11 detector
    for j in $(cat ~/group4 ); do $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware_triple$k $HOME/mountdata/FunImgF/$j/tensor.nii $HOME/ware_triple1/iter$i/outTensor_$j.nii.gz detector outTensor_$j;done
#     for ((j=1;j<=81;j++)); do $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware_triple$k $HOME/mountdata/formal_ADNI_fMRI/FunImgF/$j/tensor.nii.gz $HOME/ware_triple$k/iter$i/outTensor_$j.nii.gz detector outTensor_$j;done
    done

    for j in $(cat ~/group3 ); do $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware_triple1 $HOME/mountdata/FunImgF/$j/tensor.nii $HOME/ware_triple1/iter$i/outTensor_$j.nii.gz detector outTensor_$j;done
    for j in $(cat ~/group1 ); do $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware_triple2 $HOME/mountdata/FunImgF/$j/tensor.nii $HOME/ware_triple2/iter$i/outTensor_$j.nii.gz detector outTensor_$j;done
    for j in $(cat ~/group2 ); do $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware_triple3 $HOME/mountdata/FunImgF/$j/tensor.nii $HOME/ware_triple3/iter$i/outTensor_$j.nii.gz detector outTensor_$j;done
done

