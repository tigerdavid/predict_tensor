# For cross-validation
for ((i=1;i<=12;i++))
do
	let j=$i+96;
	mv -v ~/tumor/Hippo_$i.nii.gz ~/tumor/Hippo_$j.nii.gz
	mv -v ~/tumor/Image_$i.nii.gz ~/tumor/Image_$j.nii.gz
	mv -v ~/tumor/index_$i.txt ~/tumor/index_$j.txt
done

for ((i=13;i<=108;i++))
do
	let j=$i-12;
	mv -v ~/tumor/Hippo_$i.nii.gz ~/tumor/Hippo_$j.nii.gz
	mv -v ~/tumor/Image_$i.nii.gz ~/tumor/Image_$j.nii.gz
	mv -v ~/tumor/index_$i.txt ~/tumor/index_$j.txt
done
