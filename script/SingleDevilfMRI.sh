#!/bin/bash - 
#===============================================================================
#
#          FILE: SingleDevilfMRI.sh
# 
#         USAGE: ./SingleDevilfMRI.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: Modified from SingleDevilHCP.sh for using Training_sinTree_Prob-
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 01/23/2017 16:18
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -e

waiting(){
    while [ ! -z "$(bjobs )"  ]
    do
        sleep 10
    done

}

export OMP_NUM_THREADS=8
START=$(date +%s.%N)
echo Start time: $(date)

case "$1" in
    1)
        fileCollection=$(cat ~/group2 ~/group3 ~/group4)
        ;;
    2)
        fileCollection=$(cat ~/group1 ~/group3 ~/group4)
        ;;
    3)
        fileCollection=$(cat ~/group1 ~/group2 ~/group4)
        ;;
    4)
        fileCollection=$(cat ~/group1 ~/group2 ~/group3)
        ;;
    *)
        echo "Unknown command. Exit"
        exit 1
esac

# # rm $HOME/ware_fMRI$1/*.txt
# for j in $fileCollection; do ls $HOME/mountdata2/FunImgF/$j/Filtered_4DVolume.nii >> $HOME/ware_fMRI$1/fMRI.txt; done
# for j in $fileCollection; do ls $HOME/mountdata2/orig_smaller-tensors/$j/out_tensor.nii >> $HOME/ware_fMRI$1/dTensor.txt; done
# for j in $fileCollection; do ls $HOME/mountdata2/orig_T1w_Seg/$j/label_pve_0.nii.gz >> $HOME/ware_fMRI$1/prob0.txt; done
# for j in $fileCollection; do ls $HOME/mountdata2/orig_T1w_Seg/$j/label_pve_1.nii.gz >> $HOME/ware_fMRI$1/prob1.txt; done
# for j in $fileCollection; do ls $HOME/mountdata2/orig_T1w_Seg/$j/label_pve_2.nii.gz >> $HOME/ware_fMRI$1/prob2.txt; done
# 
# for ((i=0;i<=2;i++))
# do
#     for j in $fileCollection; do echo $HOME/ware_fMRI$1/iter$i/outTensor_$j.nii.gz >> $HOME/ware_fMRI$1/iter$i.txt; done
# done
# 
# for k in {1..20}
# do
#     bsub -o $HOME/mountdata2/outlog/training.out -k $HOME/mountdata2/outlog -n 8 -M 30 -R "span[hosts=1]" $HOME/projects/predict_dev/Training_sinTree_Prob $HOME/ware_fMRI$1 $HOME/ware_fMRI$1/dTensor.txt $HOME/ware_fMRI$1/fMRI.txt $HOME/ware_fMRI$1/prob0.txt $HOME/ware_fMRI$1/prob1.txt $HOME/ware_fMRI$1/prob2.txt $k 1 20000 11 detector
# done
# waiting
# for j in $(cat ~/all); do bsub -o $HOME/mountdata2/outlog/$j.out -k $HOME/mountdata2/outlog -n 8 -M 10 -R "span[hosts=1]" $HOME/projects/predict_dev/Testing_Adv_Prob $HOME/ware_fMRI$1 $HOME/mountdata2/FunImgF/$j/Filtered_4DVolume.nii $HOME/mountdata2/orig_T1w_Seg/$j/label_pve_0.nii.gz $HOME/mountdata2/orig_T1w_Seg/$j/label_pve_1.nii.gz $HOME/mountdata2/orig_T1w_Seg/$j/label_pve_2.nii.gz detector outTensor_$j 20;done
# waiting

for ((i=1;i<=1;i++))
do
    mv $HOME/ware_fMRI$1/currentIter $HOME/ware_fMRI$1/iter$i

    for k in {1..20}
    do
        bsub -o $HOME/mountdata2/outlog/training.out -k $HOME/mountdata2/outlog -n 8 -M 70 -R "span[hosts=1]" $HOME/projects/predict_dev/Training_Upper_sinTree_Prob $HOME/ware_fMRI$1 $HOME/ware_fMRI$1/dTensor.txt $HOME/ware_fMRI$1/fMRI.txt $HOME/ware_fMRI$1/prob0.txt $HOME/ware_fMRI$1/prob1.txt $HOME/ware_fMRI$1/prob2.txt $HOME/ware_fMRI$1/iter$i.txt $k 1 20000 11 detector
    done
    waiting
    for j in $(cat ~/all); do bsub -o $HOME/mountdata2/outlog/$j.out -k $HOME/mountdata2/outlog -n 8 -M 15 -R "span[hosts=1]" $HOME/projects/predict_dev/Testing_Upper_Adv_Prob $HOME/ware_fMRI$1 $HOME/mountdata2/FunImgF/$j/Filtered_4DVolume.nii $HOME/mountdata2/orig_T1w_Seg/$j/label_pve_0.nii.gz $HOME/mountdata2/orig_T1w_Seg/$j/label_pve_1.nii.gz $HOME/mountdata2/orig_T1w_Seg/$j/label_pve_2.nii.gz $HOME/ware_fMRI$1/iter$i/outTensor_$j.nii.gz detector outTensor_$j 20;done
    waiting
done

END=$(date +%s.%N)
echo End time: $(date)
DIFF=$(echo "$END - $START" | bc)
echo Total execution time: $DIFF

