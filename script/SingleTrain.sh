#!/bin/bash - 
#===============================================================================
#
#          FILE: TripleTrain.sh
# 
#         USAGE: ./TripleTrain.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 05/29/2016 02:20
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -e

job_limit () {
    # Test for single positive integer input
    if (( $# == 1 )) && [[ $1 =~ ^[1-9][0-9]*$ ]]
    then

        # Check number of running jobs
        joblist=($(jobs -rp))
        while (( ${#joblist[*]} >= $1 ))
        do

            # Wait for any job to finish
            command='wait '${joblist[0]}
            for job in ${joblist[@]:1}
            do
                command+=' || wait '$job
            done
            eval $command
            joblist=($(jobs -rp))
        done
    fi
}


START=$(date +%s.%N)
echo Start time: $(date)


# rm $HOME/ware/*.txt
# for j in $(cat ~/group123 ); do ls $HOME/mountdata/FunImgF/$j/tensor.nii >> $HOME/ware/fTensor.txt; done
# for j in $(cat ~/group123 ); do ls $HOME/mountdata/smaller-tensors/$j/out_tensor.nii.gz >> $HOME/ware/dTensor.txt; done
# for ((i=0;i<=3;i++))
# do
#     for j in $(cat ~/group123 ); do echo $HOME/ware/iter$i/outTensor_$j.nii.gz >> $HOME/ware/iter$i.txt; done
# done
# 
# $HOME/projects/predict_dev/Map_Training $HOME/ware $HOME/ware/dTensor.txt $HOME/ware/fTensor.txt 1 30000 20 11 detector;
# for j in $(cat ~/group4 ~/group123 )
# do
#     $HOME/projects/predict_dev/Map_Testing $HOME/ware $HOME/mountdata/FunImgF/$j/tensor.nii detector outTensor_$j &
#     job_limit 10
# done

for ((i=0;i<=0;i++))
do
    mv $HOME/ware/currentIter $HOME/ware/iter$i
    $HOME/projects/predict_dev/Map_Upper_Training $HOME/ware $HOME/ware/dTensor.txt $HOME/ware/fTensor.txt $HOME/ware/iter$i.txt 1 30000 20 11 detector;
    for j in $(cat ~/group4 ~/group123 ); do $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware $HOME/mountdata/FunImgF/$j/tensor.nii $HOME/ware/iter$i/outTensor_$j.nii.gz detector outTensor_$j & job_limit 2;done
done

# END=$(date +%s.%N)
# echo End time: $(date)
# DIFF=$(echo "$END - $START" | bc)
# echo Total execution time: $DIFF
