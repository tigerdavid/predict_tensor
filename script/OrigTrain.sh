#!/bin/bash - 
#===============================================================================
#
#          FILE: FastTrain.sh
# 
#         USAGE: ./FastTrain.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 03/08/2016 11:20
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -e

export wareFolder=$HOME/$1

START=$(date +%s.%N)
echo Start time: $(date)

cd $HOME/projects/predict_dev

k=0;
echo Start iteration $k

mkdir $wareFolder/currentIter
./Training $wareFolder 1 84 1 400 20 15 detector
for ((i=85;i<=96;i++)); do ./Testing $wareFolder $i detector PatchLocation_; done
