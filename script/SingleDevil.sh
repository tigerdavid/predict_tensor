#!/bin/bash - 
#===============================================================================
#
#          FILE: TripleTrain.sh
# 
#         USAGE: ./TripleTrain.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 05/29/2016 02:20
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
set -e

waiting(){
    while [ ! -z "$(bjobs)"  ]
    do
        sleep 10
    done

}

START=$(date +%s.%N)
echo Start time: $(date)


# rm $HOME/ware/*.txt
# for j in $(cat ~/all ); do ls $HOME/mountdata2/FunImgF3/$j/tensor.nii >> $HOME/ware/fTensor.txt; done
# for j in $(cat ~/all ); do ls $HOME/mountdata2/smaller-tensors3/$j/out_tensor.nii.gz >> $HOME/ware/dTensor.txt; done
# for ((i=0;i<=3;i++))
# do
#     for j in $(cat ~/all ); do echo $HOME/ware/iter$i/outTensor_$j.nii.gz >> $HOME/ware/iter$i.txt; done
# done
# 
export OMP_NUM_THREADS=10
for k in {1..20}
do
    bsub -o $HOME/mountdata2/outlog/training.out -k $HOME/mountdata2/outlog -n 8 -M 40 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Training $HOME/ware $HOME/ware/dTensor.txt $HOME/ware/fTensor.txt $k 1 15000 1 11 detector
done
waiting
for ((j=1;j<=81;j++)); do bsub -o $HOME/mountdata2/outlog/$j.out -k $HOME/mountdata2/outlog -n 8 -M 8 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Testing $HOME/ware $HOME/mountdata2/formal_ADNI_fMRI/FunImgF/$j/tensor.nii detector outTensor_$j 20;done
for j in $(cat ~/all); do bsub -o $HOME/mountdata2/outlog/$j.out -k $HOME/mountdata2/outlog -n 8 -M 8 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Testing $HOME/ware $HOME/mountdata2/FunImgF3/$j/tensor.nii detector outTensor_$j 20;done
waiting

for ((i=0;i<=2;i++))
do
    mv $HOME/ware/currentIter $HOME/ware/iter$i

    for k in {1..20}
    do
        bsub -o $HOME/mountdata2/outlog/training_$i.out -k $HOME/mountdata2/outlog -n 8 -M 70 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Training $HOME/ware $HOME/ware/dTensor.txt $HOME/ware/fTensor.txt $HOME/ware/iter$i.txt $k 1 15000 1 11 detector
    done
    waiting
    for ((j=1;j<=81;j++)); do bsub -o $HOME/mountdata2/outlog/$i\_$j.out -k $HOME/mountdata2/outlog -n 8 -M 15 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware $HOME/mountdata2/formal_ADNI_fMRI/FunImgF/$j/tensor.nii $HOME/ware/iter$i/outTensor_$j.nii.gz detector outTensor_$j 20;done
    for j in $(cat ~/all ); do bsub -o $HOME/mountdata2/outlog/$i\_$j.out -k $HOME/mountdata2/outlog -n 8 -M 15 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware $HOME/mountdata2/FunImgF3/$j/tensor.nii $HOME/ware/iter$i/outTensor_$j.nii.gz detector outTensor_$j 20;done
    waiting
done

END=$(date +%s.%N)
echo End time: $(date)
DIFF=$(echo "$END - $START" | bc)
echo Total execution time: $DIFF
