// simulate the registration
// backup, by calling the img2imgcoord file
// abandoned for its low efficiency

#include "common/classes.h"
#include "common/BoostHeaders.h"

int main(int argc, char *argv[])
{
	
	//initialisation
	if( argc != 4 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " inputImageFile  out2inXFMFile outImageFile" << endl;
		return EXIT_FAILURE;
	}
	
	const string inImageFile = argv[1];
	const string out2inXFMFile = argv[2];
	VectorImageType::Pointer inImage = ImageReader<VectorImageType>(inImageFile);
	const string outImageFile = argv[3];
	
	VectorImageType::Pointer outImage = ImageInitializer<VectorImageType>(inImage,1,0);
	
	typedef itk::ImageRegionIterator<VectorImageType> IteratorType;
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	
	VectorImageType::RegionType inRegion = inImage->GetLargestPossibleRegion();
    VectorImageType::SizeType inSize = inRegion.GetSize();
	ConstIteratorType inPtr(inImage,inRegion);
	IteratorType outPtr(outImage,inRegion);
	
	cout << "Command template is:" << endl;
	string temp_command = "echo 0 0 0 | img2imgcoord -src " + inImageFile + " -dest " + inImageFile + " -xfm " + out2inXFMFile + " | sed -n 2p";
	cout << temp_command << endl;

#pragma omp parallel for ordered
	for (int k=0;k< (int)inSize[2];k++)
        for (int j=0;j<(int)inSize[1];j++)
            for (int i=0;i<(int)inSize[0];i++)
//	for (outPtr.GoToBegin();!outPtr.IsAtEnd();++outPtr)
	{
		VectorImageType::IndexType outIndex;
        outIndex[0] = i;
        outIndex[1] = j;
        outIndex[2] = k;
		string out_loc = convertInt(outIndex[0]) + " " + convertInt(outIndex[1]) + " " + convertInt(outIndex[2]);
		string str_command = "echo " + out_loc + " | img2imgcoord -src " + inImageFile + " -dest " + inImageFile + " -xfm " + out2inXFMFile + " | sed -n 2p";
		string str_result = exec(str_command.data());
		stringstream ss(str_result);
		VectorImageType::IndexType inIndex;
		ss >> inIndex[0] >> inIndex[1] >> inIndex[2];
		if (inRegion.IsInside(inIndex))
		{
			VectorImageType::PixelType inValue = inImage->GetPixel(inIndex);
			outImage->SetPixel(outIndex,inValue);
		}	
	}
	
	ImageWriter<VectorImageType>(outImage,outImageFile);
	
	return EXIT_SUCCESS;
	
	
}
