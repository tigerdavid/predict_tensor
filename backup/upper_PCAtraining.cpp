// Modified from training program just for the damn training 
// #define ARMA_DONT_USE_CXX11
#include "common/classes.h"
#include "common/BoostHeaders.h"

// Param Settings for the Trees
#define MAXDEPTH 25
#define MININFOGAIN 0
#define MINLEAFNUM 8
#define NUMTHRES 20

const int numHaarFeature = 1000;
// const unsigned int confType = 1; // The very baseline of the configurations for this project, using only the very conventional way of RF



int main(int argc, char *argv[])
{
	//initialisation
	if( argc != 10 )
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " WorkingDIR TensorFileList fTensorFileList ContextFileList RandomSeed PatchNum numTree PatchSize detectorFolder" << endl;
		cerr << "For example: $HOME/ware $HOME/DTICollection/CollectionFile.txt $HOME/ware/currentIter/ContextFiles.txt $HOME/fTensorCollection/Collection.txt 1 1000 20 11 detector" << endl;
		
		return EXIT_FAILURE;
	}
	
	vector<string> fTensorNames;
	vector<string> dTensorNames;
	vector<string> contextNames;
	
	const string dTensorFileList = argv[2];
	const string fTensorFileList = argv[3];
	const string contextFileList = argv[4];
	
	funLoadVector<string>(fTensorNames,fTensorFileList);
	funLoadVector<string>(dTensorNames,dTensorFileList);
	funLoadVector<string>(contextNames,contextFileList);
	
	const unsigned int count = fTensorNames.size();
	if ((dTensorNames.size()!=count)||(contextNames.size()!=count))
	{
		cerr << "Num of subjects in fTensorFileList is different with that in dTensorFileList and contextFileList, please double check!" << endl;
		return EXIT_FAILURE;
	}
	
	const int seedNum = atoi(argv[5]);
	const unsigned int PatchNum = atoi (argv[6]);
	const unsigned int numTree = atoi(argv[7]); // same number as the numFeature;
	const unsigned int PatchSize = atoi( argv[8]);
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	
	srand(seedNum);
	
	string outputPath = argv[1];
	string detectorFolder = argv[9];
	string outputIterPath = outputPath +"/currentIter";
	
	
	//check if currentIter folder exists
	mkdir(outputIterPath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	
	// create Tree_*.mat files in the detectorFolder
	string outTreePath = outputIterPath + "/" + detectorFolder;
	mkdir(outTreePath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	const int numParamPerHaar=9;// one or two block;sizes of two blocks;locations of two blocks
	const vector<int> blockHaarSize{1,3,5};
	vector<vector< vector<int> > > ranHaarParam(numTree,vector<vector<int> >(numHaarFeature,vector<int>(numParamPerHaar)));
	for (unsigned int i=0;i<numTree;i++)
	{
		vector<vector<int> > tempHaarParam(numHaarFeature,vector<int>(numParamPerHaar));
		for (int j = 0;j<numHaarFeature;j++)
		{
			tempHaarParam[j][0] = rand()% 2; // To decide if it is one or two blocks
			tempHaarParam[j][1] = blockHaarSize[rand()%3];
			tempHaarParam[j][2] = blockHaarSize[rand()%3]; // To decide the size of two blocks
			for (int k = 3;k < 6;k++)
				tempHaarParam[j][k] = rand()%(PatchSize-2*tempHaarParam[j][1]) + tempHaarParam[j][1];
			for (int k = 7;k < 9;k++)
				tempHaarParam[j][k] = rand()%(PatchSize-2*tempHaarParam[j][2]) + tempHaarParam[j][2];
		}
		ranHaarParam[i] = tempHaarParam;
		string outHaarFeatureFile = outTreePath + "/Tree_" + convertInt(i+1) + ".mat";
		funSaveMatrix<int>(ranHaarParam[i],outHaarFeatureFile);
	}
	
	// intialize setting.txt file
	// following only to extract tensorVectorLength value. Will consider a better way in the future
	VectorReaderType::Pointer tempDTensorReader = VectorReaderType::New();
	VectorImageType::Pointer tempDTensorImage = VectorImageType::New();
	tempDTensorReader->SetFileName(dTensorNames[0].data());
	tempDTensorReader->Update();
	tempDTensorImage = tempDTensorReader->GetOutput();
	const unsigned int dTensorVectorLength = tempDTensorImage->GetNumberOfComponentsPerPixel();
	tempDTensorImage = NULL;
	tempDTensorReader = NULL;
	const unsigned int numTarget = dTensorVectorLength;
	
	string outParamSettingFile = outTreePath + "/setting.txt";
	ofstream outParamSetting(outParamSettingFile.data());
	outParamSetting << numTree << endl; // more settings can be added here if needed
	outParamSetting << PatchSize << endl;
	outParamSetting << dTensorVectorLength << endl;
	outParamSetting << numHaarFeature << endl;
	outParamSetting.close();
	
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	
	
	const unsigned long AllPatchNum = PatchNum*count;
	cout << "AllPatchNum is " << AllPatchNum << endl;
	// Initialising Features from the Patches
	vector<vector< featureClass<VectorPixelType> > > FeatureCollection(numTree, vector<featureClass<VectorPixelType> >(AllPatchNum));
	
	for (unsigned int pp = 0;pp<count;pp++)
	{
		cout << "Working on No." << pp + 1 << " Image." << endl;
		
		VectorReaderType::Pointer fTensorReader = VectorReaderType::New();
		VectorImageType::Pointer fTensorImage = VectorImageType::New();
		VectorReaderType::Pointer dTensorReader = VectorReaderType::New();
		VectorImageType::Pointer dTensorImage = VectorImageType::New();
		VectorReaderType::Pointer contextReader = VectorReaderType::New();
		VectorImageType::Pointer contextImage = VectorImageType::New();
		fTensorReader->SetFileName(fTensorNames[pp].data());
		fTensorReader->Update();
		fTensorImage = fTensorReader->GetOutput();
		dTensorReader->SetFileName(dTensorNames[pp].data());
		dTensorReader->Update();
		dTensorImage = dTensorReader->GetOutput();
		contextReader->SetFileName(contextNames[pp].data());
		contextReader->Update();
		contextImage = contextReader->GetOutput();
		
		
		const unsigned int fTensorVectorLength = fTensorImage->GetNumberOfComponentsPerPixel();
		// 		const unsigned int dTensorVectorLength = dTensorImage->GetNumberOfComponentsPerPixel();
		
		
		// select patches' locations
		VectorImageType::RegionType OutRegion = dTensorImage->GetLargestPossibleRegion();
		VectorImageType::SizeType OutSize = OutRegion.GetSize();
		
		vector <location<VectorPixelType> > PatchCenter;
		string outMapFile = outputIterPath + "/map_" + convertInt(pp+1) + ".txt";
		ofstream outMap(outMapFile.data());
		
		while (PatchCenter.size() < PatchNum)
		{
			VectorImageType::IndexType TempPixelIndex;
			TempPixelIndex[0] = (int)(rand() % (OutSize[0]-2*radius)) + radius;
			TempPixelIndex[1] = (int)(rand() % (OutSize[1]-2*radius)) + radius;
			TempPixelIndex[2] = (int)(rand() % (OutSize[2]-2*radius)) + radius;
			VectorPixelType fTensorPixelValue = fTensorImage->GetPixel(TempPixelIndex);
			VectorPixelType dTensorPixelValue = dTensorImage->GetPixel(TempPixelIndex);
			if ((funMean(funAbs(fTensorPixelValue)) != 0)&&(funMean(funAbs(dTensorPixelValue)) !=0))
			{
				location<VectorPixelType> tempLoc(TempPixelIndex[0],TempPixelIndex[1],TempPixelIndex[2],dTensorPixelValue);
				PatchCenter.push_back(tempLoc);
				tempLoc.Serialize(outMap);
				
			}
		}
		outMap.close();
		
		VectorImageType::SizeType regionSize;
		regionSize.Fill(PatchSize);
		cout << "Computing Features" << endl;
		
		#pragma omp parallel for ordered
		for (unsigned int jj = 0;jj < PatchNum; jj++)
		{
			unsigned long int currentPatchNum = jj+pp*PatchNum;
			
			// Initialising patch
			location<VectorPixelType> tempLocation = PatchCenter[jj];
			
			VectorImageType::RegionType smallRegion;
			smallRegion.SetSize(regionSize);
			VectorImageType::IndexType regionIndex;
			regionIndex[0] = tempLocation.x-radius;
			regionIndex[1] = tempLocation.y-radius;
			regionIndex[2] = tempLocation.z-radius;
			smallRegion.SetIndex(regionIndex);
			
			ConstIteratorType patFTPtr(fTensorImage, smallRegion);
			patFTPtr.GoToBegin();
			ConstIteratorType patConPtr(contextImage, smallRegion);
			patConPtr.GoToBegin();
			
			// 			vector<Col<float> > PatchContent(PatchSize3);
			vector<Cube<ImgPixelType> > fTensorContent(fTensorVectorLength);
			for (unsigned int kk=0;kk < fTensorVectorLength;kk++)
				fTensorContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
			vector<Cube<ImgPixelType> > ContextContent(dTensorVectorLength);
			for (unsigned int kk=0;kk < dTensorVectorLength;kk++)
				ContextContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
			
			for (unsigned int kk = 0; kk < PatchSize3; kk++)
			{				
				VectorImageType::IndexType FTIndex = patFTPtr.GetIndex();
				VectorPixelType tempFTensorValue = patFTPtr.Get();
				for (unsigned int jj = 0; jj < fTensorVectorLength;jj++)
					fTensorContent[jj](FTIndex[0]-regionIndex[0],FTIndex[1]-regionIndex[1],FTIndex[2]-regionIndex[2]) = tempFTensorValue[jj];
				
				VectorImageType::IndexType ConIndex = patConPtr.GetIndex();
				VectorPixelType tempContextValue = patConPtr.Get();
				for (unsigned int jj = 0; jj < dTensorVectorLength;jj++)
					ContextContent[jj](ConIndex[0]-regionIndex[0],ConIndex[1]-regionIndex[1],ConIndex[2]-regionIndex[2]) = tempContextValue[jj];
				++patFTPtr;++patConPtr;
			}
			for (unsigned int i=0;i<numTree;i++)
				FeatureCollection[i][currentPatchNum] = featureClass<VectorPixelType>(currentPatchNum,pp,tempLocation,fTensorContent,ContextContent,ranHaarParam[i], numHaarFeature);
			
		}
		
	}
	
	
	// warp the current data available to fit Yaozong's code
	vector<float> tempFeatureVector = FeatureCollection[0][0].featureWarpper();
	unsigned int numPatchFeatures = tempFeatureVector.size();
	tempFeatureVector.empty();
	cout << "\nFeature Length is: " << numPatchFeatures << endl;
	
	BRIC::IDEA::FISH::TreeTrainingParameters treeParams;
	treeParams.maxTreeDepth = MAXDEPTH;
	treeParams.minElementsOfLeafNode = MINLEAFNUM;
	treeParams.numOfCandidateThresholdsPerWeakLearner = NUMTHRES;
	treeParams.numOfRandomWeakLearners = numPatchFeatures;
	treeParams.minInformationGain = MININFOGAIN;
	
	
	for (unsigned int i=0;i<numTree;i++)
	{   
		cout << "Working on No." << i+1 << " Tree." << endl;
		boost::timer::auto_cpu_timer t; 
		unsigned int featureSeedNum = seedNum + i;
		BRIC::IDEA::FISH::Random random;
		random.Seed(featureSeedNum);
		
		BRIC::IDEA::FISH::MemoryDataCollection trainingData;
		trainingData.SetFeatureNumber(numPatchFeatures);
		trainingData.SetTargetDim(numTarget);
		
		vector<float>& data = trainingData.GetDataVector();
		vector<float>& targets = trainingData.GetTargetVector();
		
		
		for (unsigned long int j=0;j<AllPatchNum;j++)
		{
			for (unsigned int ii=0;ii<dTensorVectorLength;ii++)
				targets.push_back(FeatureCollection[i][j].center.label[ii]);
			vector<float> tempFeature = FeatureCollection[i][j].featureWarpper(); 
			data.insert(data.end(),tempFeature.begin(),tempFeature.end()); 
		}
		
		string outDataDebugFile = outputIterPath + "/data_debug_" + convertInt(i+1) + ".txt";
		string outLabelsDebugFile = outputIterPath + "/labels_debug_" + convertInt(i+1) + ".txt";
		funSaveVector(data,outDataDebugFile);
		funSaveVector(targets,outLabelsDebugFile);
		
		trainingData.SetSampleNumber(AllPatchNum);
		
		
		// using the Yaozong's TreeTraining code
		BRIC::IDEA::FISH::SimpleRegressionRandTrainingContext context(numPatchFeatures, numTarget);
		cout << "Start formal training process" << endl;
		std::unique_ptr< TreeType > tree = BRIC::IDEA::FISH::TreeTrainer<W,S>::TrainTree(context, treeParams, &trainingData, random);
		
		// Write tree
		string outTreeFile = outTreePath + "/Tree_" + convertInt(i+1) + ".bin";
		ofstream outTree(outTreeFile.data(),std::ios::binary);
		if(!outTree) cerr << "Fail to write the tree." << endl;
		tree->Serialize(outTree);
		outTree.close();
	}
	
	return EXIT_SUCCESS;
}

