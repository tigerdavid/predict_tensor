// Modified from map_training.cpp for the structured RF
// #define ARMA_DONT_USE_CXX11
#include "common/classes.h"
#include "common/BoostHeaders.h"

// Param Settings for the Trees
#define MAXDEPTH 26
#define MININFOGAIN 0
#define MINLEAFNUM 8
#define NUMTHRES 20

const int numHaarFeature = 1000;
const int reducedLength = 15;
// const unsigned int confType = 1; // The very baseline of the configurations for this project, using only the very conventional way of RF



int main(int argc, char *argv[])
{
	//initialisation
	if(( argc != 9 )&&(argc != 10 ))
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " WorkingDIR TensorFileList fTensorFileList RandomSeed PatchNum numTree PatchSize detectorFolder (maskImage)" << endl;
		cerr << "For example: $HOME/ware $HOME/DTICollection/CollectionFile.txt $HOME/fTensorCollection/Collection.txt 1 1000 20 11 detector ($HOME/MaskImage.nii.gz)" << endl;
		
		return EXIT_FAILURE;
	}
	
	vector<string> fTensorNames;
	vector<string> dTensorNames;
	
	const string dTensorFileList = argv[2];
	const string fTensorFileList = argv[3];
	
	funLoadVector<string>(fTensorNames,fTensorFileList);
	funLoadVector<string>(dTensorNames,dTensorFileList);
	
	const unsigned int count = fTensorNames.size();
	if (dTensorNames.size()!=count)
	{
		cerr << "Num of subjects in fTensorFileList is different with that in dTensorFileList, please double check!" << endl;
		return EXIT_FAILURE;
	}
	
	const int seedNum = atoi(argv[4]);
	const unsigned int PatchNum = atoi (argv[5]);
	const unsigned int numTree = atoi(argv[6]); // same number as the numFeature;
	const unsigned int PatchSize = atoi( argv[7]);
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	
	srand(seedNum);
	
	string outputPath = argv[1];
	string detectorFolder = argv[8];
	string outputIterPath = outputPath +"/currentIter";
	
	
	//check if currentIter folder exists
	mkdir(outputIterPath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	
	// create Tree_*.mat files in the detectorFolder
	string outTreePath = outputIterPath + "/" + detectorFolder;
	mkdir(outTreePath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	vector<vector< vector<int> > > ranHaarParam(numTree);
	for (unsigned int i=0;i<numTree;i++)
	{
		funInitParamHaar(seedNum+i, PatchSize,numHaarFeature,ranHaarParam[i]);
		string outHaarFeatureFile = outTreePath + "/Tree_" + convertInt(i+1) + ".mat";
		funSaveMatrix<int>(ranHaarParam[i],outHaarFeatureFile);
	}
	
	
	vector<VectorImageType::Pointer> fTensorImage(count);
	vector<VectorImageType::Pointer> dTensorImage(count);
	for (unsigned int i=0;i<count;i++)
	{
		fTensorImage[i] = ImageReader<VectorImageType>(fTensorNames[i].data());
		dTensorImage[i] = ImageReader<VectorImageType>(dTensorNames[i].data());
	}
	const unsigned int fTensorVectorLength = fTensorImage[0]->GetNumberOfComponentsPerPixel();
	const unsigned int dTensorVectorLength = dTensorImage[0]->GetNumberOfComponentsPerPixel();
	const unsigned int numTarget = dTensorVectorLength;
	
	
	
	// initialise maskImage if existed; create maskImage with whole region marked if not
	VectorImageType::Pointer maskImage = VectorImageType::New();
	if (argc == 10)
		maskImage = ImageReader<VectorImageType>(argv[9]);
	else
		maskImage = ImageInitializer<VectorImageType>(dTensorImage[0],1,1);
	
	vector<location<Row<float> > > AllPatchCenter;
	vector<location<VectorPixelType> > PatchCenter;
	for (unsigned int i=0;i<count;i++)
	{
		funPCATrainingSampling(fTensorImage[i],dTensorImage[i],maskImage,PatchNum,radius,AllPatchCenter);
		
		string outMapFile = outputIterPath + "/map_" + convertInt(i+1) + ".txt";
		ofstream outMap(outMapFile.data());
		for (unsigned int j = i*PatchNum;j<(i+1)*PatchNum;j++)
			AllPatchCenter[j].Serialize(outMap);
		outMap.close();
		
		dTensorImage[i] = NULL; // dTensor is eliminated at this stage now to save space
	}
	
	// PCA part
	Col<float> s;
	Mat<float> V;
	PatchCenter = funDimensionReduction(AllPatchCenter,s,V,reducedLength);
	string outSFile = outTreePath + "/SFile.mat";
	string outVFile = outTreePath + "/VFile.mat";
	s.save(outSFile);
	V.save(outVFile);
	AllPatchCenter.clear(); // clear here to save memory
	
	
	string outParamSettingFile = outTreePath + "/setting.txt";
	ofstream outParamSetting(outParamSettingFile.data());
	outParamSetting << numTree << endl; // more settings can be added here if needed
	outParamSetting << PatchSize << endl;
	outParamSetting << dTensorVectorLength << endl;
	outParamSetting << reducedLength << endl;
	outParamSetting << numHaarFeature << endl;
	outParamSetting << PatchNum << endl; // just for recording, the PatchNum won't be used in the testing stage
	outParamSetting.close();
	
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	
	const unsigned long AllPatchNum = PatchNum*count;
	cout << colblue << "AllPatchNum is " << AllPatchNum << coldef << endl;
	
	
	for (unsigned int i=0;i<numTree;i++)
	{   
		cout << colcyn << "Working on No." << i+1 << " Tree." << endl;
// 		process_mem_usage();
		boost::timer::auto_cpu_timer t; 
		unsigned int featureSeedNum = seedNum + i;
		BRIC::IDEA::FISH::Random random;
		random.Seed(featureSeedNum);
		
		vector< featureClass<VectorPixelType> > FeatureCollection(AllPatchNum);
		
		
		VectorImageType::SizeType regionSize;
		regionSize.Fill(PatchSize);
		cout << colred << "Computing Features" << endl;
		
		
		#pragma omp parallel for ordered
		
		for (unsigned int pp=0;pp< count;pp++)
		{
			for (unsigned int jj = 0;jj < PatchNum; jj++)
			{
				unsigned long int currentPatchNum = jj+pp*PatchNum;
				
				// Initialising patch
				location<VectorPixelType> tempLocation = PatchCenter[currentPatchNum];
				
				VectorImageType::RegionType smallRegion;
				smallRegion.SetSize(regionSize);
				VectorImageType::IndexType regionIndex;
				regionIndex[0] = tempLocation.x-radius;
				regionIndex[1] = tempLocation.y-radius;
				regionIndex[2] = tempLocation.z-radius;
				smallRegion.SetIndex(regionIndex);
				
				ConstIteratorType patFTPtr(fTensorImage[pp], smallRegion);
				patFTPtr.GoToBegin();
				
				vector<Cube<ImgPixelType> > fTensorContent(fTensorVectorLength);
				for (unsigned int kk=0;kk < fTensorVectorLength;kk++)
					fTensorContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
				
				
				for (unsigned int kk = 0; kk < PatchSize3; kk++)
				{				
					VectorImageType::IndexType tempIndex = patFTPtr.GetIndex();
					VectorPixelType tempFTensorValue = patFTPtr.Get();
					for (unsigned int jj = 0; jj < fTensorVectorLength;jj++)
						fTensorContent[jj](tempIndex[0]-regionIndex[0],tempIndex[1]-regionIndex[1],tempIndex[2]-regionIndex[2]) = tempFTensorValue[jj];
					++patFTPtr;
				}
				FeatureCollection[currentPatchNum] = featureClass<VectorPixelType>(currentPatchNum,pp,tempLocation);
				FeatureCollection[currentPatchNum].funAssignHaar(fTensorContent,ranHaarParam[i], numHaarFeature);
			}
			
		}
		
		
		
		// warp the current data available to fit Yaozong's code
		unsigned int numPatchFeatures = FeatureCollection[0].featureWarpper().size();
		cout << coldef << "\nFeature Length is: " << numPatchFeatures << endl;
		// 	process_mem_usage();
		
		BRIC::IDEA::FISH::TreeTrainingParameters treeParams;
		treeParams.maxTreeDepth = MAXDEPTH;
		treeParams.minElementsOfLeafNode = MINLEAFNUM;
		treeParams.numOfCandidateThresholdsPerWeakLearner = NUMTHRES;
		treeParams.numOfRandomWeakLearners = numPatchFeatures;
		treeParams.minInformationGain = MININFOGAIN;
		
		
		BRIC::IDEA::FISH::MemoryDataCollection trainingData;
		trainingData.SetFeatureNumber(numPatchFeatures);
		trainingData.SetTargetDim(numTarget);
		
		vector<float>& data = trainingData.GetDataVector();
		vector<float>& targets = trainingData.GetTargetVector();
		
		data.reserve(numPatchFeatures*AllPatchNum);
		targets.reserve(numTarget*AllPatchNum);
		
		
		for (unsigned long int j=0;j<AllPatchNum;j++)
		{
			for (unsigned int ii=0;ii<dTensorVectorLength;ii++)
				targets.push_back(FeatureCollection[j].center.label[ii]);
			FeatureCollection[j].featureWarpper(data);
		}
		
		trainingData.SetSampleNumber(AllPatchNum);
		
		
		// using the Yaozong's TreeTraining code
		BRIC::IDEA::FISH::SimpleRegressionRandTrainingContext context(numPatchFeatures, numTarget);
		cout << colpur << "Start formal training process" << coldef << endl;
		std::unique_ptr< TreeType > tree = BRIC::IDEA::FISH::TreeTrainer<W,S>::TrainTree(context, treeParams, &trainingData, random);
		
		// Write tree
		string outTreeFile = outTreePath + "/Tree_" + convertInt(i+1) + ".bin";
		ofstream outTree(outTreeFile.data(),std::ios::binary);
		if(!outTree) cerr << "Fail to write the tree." << endl;
		tree->Serialize(outTree);
		outTree.close();

	}
	
	cout << colyel << "Finished" << endl;
	return EXIT_SUCCESS;
}

