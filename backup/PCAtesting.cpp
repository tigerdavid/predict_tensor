// NOTE: For the testing stage, modified from training.cpp


// #define ARMA_DONT_USE_CXX11
#include "common/classes.h"
#include "common/BoostHeaders.h"

// Needed by Yaozong's code (Forest's testing code)
#include "common/ArrayFunctor.h"
#include "forest/tester/ForestClassifier.h"

#define SPACING 1


int main(int argc, char *argv[])
{
	//initialisation
	if(( argc != 5 )&&(argc!=6))
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " WorkingDIR InputFMRI detectorFolder outTensorFile (maskImage)" << endl;
		cerr << "For example: $HOME/ware $HOME/fMRIAllCollection/rfMRI_60.nii.gz detector outTensor_60 ($HOME/maskImage.nii.gz)" << endl;
		return EXIT_FAILURE;
	}
	
	string outputPath = argv[1];
	string inFTensorFile = argv[2];
	string detectorFolder = argv[3];
	string outDTensorFile = argv[4];
	string outputIterPath = outputPath + "/currentIter";
	string inTreePath = outputIterPath + "/" + detectorFolder;
	string inParamSettingFile = inTreePath + "/setting.txt";
	
	boost::timer::auto_cpu_timer t; 
	
	vector<unsigned int> inParamVector;
	funLoadVector<unsigned int>(inParamVector,inParamSettingFile);
	const unsigned int numTree = inParamVector[0];
	const unsigned int PatchSize = inParamVector[1];
	const unsigned int reducedLength = inParamVector[2];
	const unsigned int dTensorVectorLength = inParamVector[3];
	const int numHaarFeature = inParamVector[4];
	const unsigned int numTarget = dTensorVectorLength;
	
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	
	
	VectorImageType::Pointer fTensorImage = ImageReader<VectorImageType>(inFTensorFile.data());
	const unsigned int fTensorVectorLength = fTensorImage->GetNumberOfComponentsPerPixel();
	
	// initialise maskImage if existed; create maskImage with whole region marked if not
	VectorImageType::Pointer maskImage = VectorImageType::New();
	if (argc == 6)
		maskImage = ImageReader<VectorImageType>(argv[5]);
	else
		maskImage = ImageInitializer<VectorImageType>(fTensorImage,1,1);
	
	VectorImageType::RegionType OutRegion = fTensorImage->GetLargestPossibleRegion();
	
	vector <location<VectorPixelType> > PatchCenter;   
	// 	patchClass<VectorPixelType> tempPatch;
	
	funTestingSampling(fTensorImage,maskImage,SPACING,radius,PatchCenter);
	
	const int PatchNum = PatchCenter.size();
	cout << colblue << "Selecting " << PatchNum << " patches from the image" << endl;
	
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	
	VectorImageType::SizeType regionSize;
	regionSize.Fill(PatchSize);
	
	cout << colgreen << "Reading information from trained classifiers" << coldef << endl;
	
	// Tree Testing Process
	vector<vector< featureClass<VectorPixelType> > > FeatureCollection(numTree, vector<featureClass<VectorPixelType> >(PatchNum));
	vector<vector< vector<int> > > ranHaarParam;ranHaarParam.reserve(numTree);
	unique_ptr< ForestType> forest(new ForestType);
	
	for (unsigned int i=0;i<numTree;i++)
	{
		
		vector< vector<int> > tempHaarParam;
		string inHaarFeatureFile = inTreePath + "/Tree_" + convertInt(i+1) + ".mat";
		funLoadMatrix<int>(tempHaarParam,inHaarFeatureFile);
		ranHaarParam.push_back(tempHaarParam);
		
		string inTreeFile = inTreePath + "/Tree_" + convertInt(i+1) + ".bin";
		ifstream inTree(inTreeFile.data(),ios::binary);
		if(!inTree) {
			cerr << "could not read tree from " << inTreeFile << endl; exit(-1);
		}
		unique_ptr<typename ForestType::TreeType> tree = ForestType::TreeType::Deserialize(inTree);
		inTree.close();
		tree->CheckValid();
		forest->AddTree(move(tree));
		tree.release();
	}
	
	cout << colred << "Computing Features" << endl;
	// 	vector<int> progress(PatchNum,0);
	
	#pragma omp parallel for ordered 
	for (int j=0;j<PatchNum;j++)
	{
		
		unsigned long int currentPatchNum = j;
		
		// Initialising patch
		location<VectorPixelType> tempLocation = PatchCenter[currentPatchNum];
		
		VectorImageType::RegionType smallRegion;
		smallRegion.SetSize(regionSize);
		VectorImageType::IndexType regionIndex;
		regionIndex[0] = tempLocation.x-radius;
		regionIndex[1] = tempLocation.y-radius;
		regionIndex[2] = tempLocation.z-radius;
		smallRegion.SetIndex(regionIndex);
		
		ConstIteratorType patFTPtr(fTensorImage, smallRegion);
		patFTPtr.GoToBegin();
		
		vector<Cube<ImgPixelType> > fTensorContent(fTensorVectorLength);
		for (unsigned int kk=0;kk < fTensorVectorLength;kk++)
			fTensorContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
		
		for (unsigned int kk = 0; kk < PatchSize3; kk++)
		{				
			VectorImageType::IndexType tempIndex = patFTPtr.GetIndex();
			VectorPixelType tempFTensorValue = patFTPtr.Get();
			for (unsigned int jj = 0; jj < fTensorVectorLength;jj++)
				fTensorContent[jj](tempIndex[0]-regionIndex[0],tempIndex[1]-regionIndex[1],tempIndex[2]-regionIndex[2]) = tempFTensorValue[jj];
			++patFTPtr;
		}
		for (unsigned int i=0;i<numTree;i++)
		{
			FeatureCollection[i][currentPatchNum] = featureClass<VectorPixelType>(currentPatchNum,1,tempLocation);
			FeatureCollection[i][currentPatchNum].funAssignHaar(fTensorContent,ranHaarParam[i], numHaarFeature);
		}
	}           
	
	
	unsigned int numPatchFeatures = FeatureCollection[0][0].featureWarpper().size();
	cout << "\nFeature Length is: " << numPatchFeatures << endl;
	
	cout << "Start classifying the obtained patches." << endl;
	vector<vector<float> > predictions(PatchNum,vector<float>(reducedLength,0.0));
	for (int ii = 0; ii < PatchNum; ii++)
	{
		for (unsigned int i = 0; i < numTree; ++i) 
		{
			BRIC::IDEA::FISH::ArrayFunctor<float> functor;
			vector<float> features = FeatureCollection[i][ii].featureWarpper();
			functor.SetArray(&features[0]);
			const S& stat = forest->GetTree(i)->FastApply(functor);
			vector<float> tempProbs(reducedLength);
			stat.GetMean(&tempProbs[0]);
			for (unsigned int jj = 0; jj < fTensorVectorLength;jj++)
				predictions[ii][jj] += tempProbs[jj]/numTree;  
		}
	}
	
	
	// produce result
	VectorImageType::Pointer resTensorImage = ImageInitializer<VectorImageType>(fTensorImage,numTarget,0);
	
	
	for (int i=0;i<PatchNum;i++)
	{
		VectorPixelType tensorElement;
		VectorImageType::IndexType tensorIndex;
		tensorIndex[0] = PatchCenter[i].x;
		tensorIndex[1] = PatchCenter[i].y;
		tensorIndex[2] = PatchCenter[i].z;
		tensorElement.SetSize(numTarget);
		for (unsigned int j=0;j<numTarget;j++)
			tensorElement[j] = predictions[i][j];
		resTensorImage->SetPixel(tensorIndex, tensorElement);
	}
	
	
	string outTensorPath = outputIterPath + "/" + outDTensorFile + ".nii.gz";
	ImageWriter<VectorImageType>(resTensorImage,outTensorPath);
	
	cout << colyel << "Finished" << coldef << endl;
	
	return EXIT_SUCCESS;
}
