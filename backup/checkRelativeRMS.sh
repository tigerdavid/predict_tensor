#!/bin/bash - 
#===============================================================================
#
#          FILE: checkRelativeRMS.sh
# 
#         USAGE: ./checkRelativeRMS.sh 
# 
#   DESCRIPTION: 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 07/07/2016 11:13
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
# set -x
set -u

j=0
num2=0.5
for i in {30..630}
do
    num1=$(sed -n "$i"p $1 )
    if [ $(echo $num1 '>' $num2 | bc -l) -eq 1 ]
    then
        let j=$j+1
    fi
done

if [ $(echo $j '>' 0 | bc -l) -eq 1 ]
then
    echo $1 $j
fi

