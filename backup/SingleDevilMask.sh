#!/bin/bash - 
#===============================================================================
#
#          FILE: TripleTrain.sh
# 
#         USAGE: ./TripleTrain.sh 
# 
#   DESCRIPTION: Modified from SingleDevilHCP, using only one fold of cross validation 
# 
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
#  ORGANIZATION: 
#       CREATED: 05/29/2016 02:20
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error
# set -e

waiting(){
    while [ ! -z "$(bjobs -w | grep ware )"  ]
    do
        sleep 10
    done

}

START=$(date +%s.%N)
echo Start time: $(date)

fileCollection=$(cat ~/group1 ~/group2 ~/group3 )

# # rm $HOME/ware/*.txt
# for j in $fileCollection; do ls $HOME/mountdata2/FunImgF/$j/tensor.nii.gz >> $HOME/ware/fTensor.txt; done
# for j in $fileCollection; do ls $HOME/mountdata2/smaller-tensors/$j/out_tensor.nii.gz >> $HOME/ware/dTensor.txt; done
# for j in $fileCollection; do ls $HOME/mountdata2/HCP_Seg/$j/label_gm.nii.gz >> $HOME/ware/gmMask.txt; done
# for j in $fileCollection; do ls $HOME/mountdata2/HCP_Seg/$j/label_wm.nii.gz >> $HOME/ware/wmMask.txt; done
# for ((i=0;i<=2;i++))
# do
#     for j in $(cat ~/fileCollection ); do echo $HOME/ware/iter$i/outTensor_$j.nii.gz >> $HOME/ware/iter$i.txt; done
# done
# 
# export OMP_NUM_THREADS=10
# for k in {1..20}
# do
#     bsub -o $HOME/mountdata2/outlog/training.out -k $HOME/mountdata2/outlog -n 8 -M 55 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Training_Mask $HOME/ware $HOME/ware/dTensor.txt $HOME/ware/fTensor.txt $HOME/ware/gmMask.txt $k 1 30000 1 11 detector_gm
#     bsub -o $HOME/mountdata2/outlog/training.out -k $HOME/mountdata2/outlog -n 8 -M 55 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Training_Mask $HOME/ware $HOME/ware/dTensor.txt $HOME/ware/fTensor.txt $HOME/ware/wmMask.txt $k 1 30000 1 11 detector_wm
# done
# waiting ware
for j in $(cat ~/all); do bsub -o $HOME/mountdata2/outlog/$j.out -k $HOME/mountdata2/outlog -n 8 -M 10 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Testing $HOME/ware $HOME/mountdata2/FunImgF/$j/tensor.nii.gz detector_gm outTensor_gm_$j 20;done
for j in $(cat ~/all); do bsub -o $HOME/mountdata2/outlog/$j.out -k $HOME/mountdata2/outlog -n 8 -M 10 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Testing $HOME/ware $HOME/mountdata2/FunImgF/$j/tensor.nii.gz detector_wm outTensor_wm_$j 20;done
waiting ware

# for ((i=0;i<2;i++))
# do
#     mv $HOME/ware/currentIter $HOME/ware/iter$i
# 
#     for k in {1..20}
#     do
#         bsub -o $HOME/mountdata2/outlog/training_$i.out -k $HOME/mountdata2/outlog -n 8 -M 105 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Training $HOME/ware $HOME/ware/dTensor.txt $HOME/ware/fTensor.txt $HOME/ware/iter$i.txt $k 1 30000 1 11 detector
#     done
#     waiting ware
#     for j in $(cat ~/all ); do bsub -o $HOME/mountdata2/outlog/$i\_$j.out -k $HOME/mountdata2/outlog -n 8 -M 15 -R "span[hosts=1]" $HOME/projects/predict_dev/Map_Upper_Testing $HOME/ware $HOME/mountdata2/test/fTensor/$j/tensor.nii.gz  $HOME/ware/iter$i/outTensor_$j.nii.gz detector outTensor_$j 20;done
#     waiting ware
# done

END=$(date +%s.%N)
echo End time: $(date)
DIFF=$(echo "$END - $START" | bc)
echo Total execution time: $DIFF
