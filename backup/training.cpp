// #define ARMA_DONT_USE_CXX11
#include "common/classes.h"
#include "common/BoostHeaders.h"

// Param Settings for the Trees
#define MAXDEPTH 26
#define MININFOGAIN 0
#define MINLEAFNUM 8
#define NUMTHRES 20

const int numCorrFeature = 600;
// const unsigned int confType = 1; // The very baseline of the configurations for this project, using only the very conventional way of RF



int main(int argc, char *argv[])
{
	//initialisation
	if(( argc != 9 )&&(argc!=10))
	{
		cerr << "Usage: " << endl;
		cerr << argv[0] << " WorkingDIR TensorFileList fMRIFileList RandomSeed PatchNum numTree PatchSize detectorFolder (maskImage)" << endl;
		cerr << "For example: $HOME/ware $HOME/ware/tensor.txt $HOME/ware/fMRI.txt 1 1000 20 11 detector ($HOME/mask.nii.gz)" << endl;
		
		return EXIT_FAILURE;
	}
	
	vector<string> fmriNames;
	vector<string> tensorNames;
	
	const string tensorFileList = argv[2];
	const string fMRIFileList = argv[3];
	
	funLoadVector<string>(fmriNames,fMRIFileList);
	funLoadVector<string>(tensorNames,tensorFileList);
	
	const unsigned int count = fmriNames.size();
	if (tensorNames.size()!=count)
	{
		cerr << "Num of subjects in TensorFileList is different with that in fMRIFileList, please double check!" << endl;
		return EXIT_FAILURE;
	}
	
	const int seedNum = atoi(argv[4]);
	const unsigned int PatchNum = atoi (argv[5]);
	const unsigned int numTree = atoi(argv[6]); // same number as the numFeature;
	const unsigned int PatchSize = atoi( argv[7]);
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	const unsigned int extradius = radius + 1;
	
	srand(seedNum);
	
	string outputPath = argv[1];
	string detectorFolder = argv[8];
	string outputIterPath = outputPath +"/currentIter";

	
	//check if currentIter folder exists
	mkdir(outputIterPath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	
	
	// create Tree_*.mat files in the detectorFolder
	string outTreePath = outputIterPath + "/" + detectorFolder;
	mkdir(outTreePath.data(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
	vector<vector< vector<int> > > ranCorrParam(numTree);
	for (unsigned int i=0;i<numTree;i++)
	{
		funInitParamCorr(seedNum+i, PatchSize3,numCorrFeature, ranCorrParam[i]);
		string outCorrFeatureFile = outTreePath + "/Tree_" + convertInt(i+1) + ".mat";
		funSaveMatrix<int>(ranCorrParam[i],outCorrFeatureFile);
	}
	
	// intialize setting.txt file
	// following only to extract tensorVectorLength value. Will consider a better way in the future
	VectorImageType::Pointer tempTensorImage = ImageReader<VectorImageType>(tensorNames[0].data());
	const unsigned int tensorVectorLength = tempTensorImage->GetNumberOfComponentsPerPixel();
	const unsigned int numTarget = tensorVectorLength;
	// 	cout << colred << numTarget << coldef << endl;
	
	
	// initialise maskImage if existed; create maskImage with whole region marked if not
	VectorImageType::Pointer maskImage = VectorImageType::New();
	if (argc == 10)
		maskImage = ImageReader<VectorImageType>(argv[9]);
	else
	{
		maskImage->CopyInformation(tempTensorImage);
		maskImage->SetRegions(tempTensorImage->GetLargestPossibleRegion());
		maskImage->SetNumberOfComponentsPerPixel(1);
		maskImage->Allocate();
		
		VectorPixelType valOne;
		valOne.SetSize(1);
		valOne.Fill(1.0);
		maskImage->FillBuffer(valOne);
	}
	tempTensorImage = NULL;
	
	string outParamSettingFile = outTreePath + "/setting.txt";
	ofstream outParamSetting(outParamSettingFile.data());
	outParamSetting << numTree << endl; // more settings can be added here if needed
// 	outParamSetting << PatchNum << endl;
	outParamSetting << PatchSize << endl;
	outParamSetting << tensorVectorLength << endl;
	outParamSetting << numCorrFeature << endl;
	outParamSetting.close();
	
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	
	
	const unsigned long AllPatchNum = PatchNum*count;
	cout << colblue << "AllPatchNum is " << AllPatchNum << coldef << endl;
	// Initialising Features from the Patches
	vector<vector< featureClass<VectorPixelType> > > FeatureCollection(numTree, vector<featureClass<VectorPixelType> >(AllPatchNum));
	
	for (unsigned int pp = 0;pp<count;pp++)
	{
		cout << colgreen << "Working on No." << pp + 1 << " Image." << coldef << endl;
	
		VectorImageType::Pointer fmriImage = ImageReader<VectorImageType>(fmriNames[pp].data());
		VectorImageType::Pointer tensorImage = ImageReader<VectorImageType>(tensorNames[pp].data());
		const unsigned int fmriVectorLength = fmriImage->GetNumberOfComponentsPerPixel();
// 		const unsigned int tensorVectorLength = tensorImage->GetNumberOfComponentsPerPixel();

		
		// select patches' locations
		
		vector <location<VectorPixelType> > PatchCenter;
		funTrainingSampling(fmriImage,tensorImage,maskImage,PatchNum,extradius,PatchCenter);
		
		string outMapFile = outputIterPath + "/map_" + convertInt(pp+1) + ".txt";
		ofstream outMap(outMapFile.data());
		for (auto i : PatchCenter)
			i.Serialize(outMap);
		outMap.close();


		VectorImageType::SizeType regionSize;
		regionSize.Fill(PatchSize);
		VectorImageType::SizeType tinyRegionSize; // only 3x3x3 for patch correlation computation (instead of voxel correlation)
		tinyRegionSize.Fill(3);
		VectorImageType::SizeType unitSize;
		unitSize.Fill(1);
		
		cout << "Computing Features" << endl;

		
		#pragma omp parallel for ordered
		for (unsigned int jj = 0;jj < PatchNum; jj++)
		{
			unsigned long int currentPatchNum = jj+pp*PatchNum;
			
			// Initialising patch
			location<VectorPixelType> tempLocation = PatchCenter[jj];
			
			VectorImageType::RegionType smallRegion;
			smallRegion.SetSize(regionSize);
			VectorImageType::IndexType regionIndex;
			regionIndex[0] = tempLocation.x-radius;
			regionIndex[1] = tempLocation.y-radius;
			regionIndex[2] = tempLocation.z-radius;
			smallRegion.SetIndex(regionIndex);
			
			ConstIteratorType patFMRIPtr(fmriImage, smallRegion);
			patFMRIPtr.GoToBegin();
			
			vector<Col<float> > PatchContent(PatchSize3);
			
			
			// initialize stuffs for patch correlation computation
			VectorImageType::RegionType tinyRegion;
			tinyRegion.SetSize(tinyRegionSize);
			VectorImageType::IndexType tinyRegionIndex;
			
			for (unsigned int kk = 0; kk < PatchSize3; kk++)
			{				
				// for patch correlation computation
				tinyRegionIndex = patFMRIPtr.GetIndex()-unitSize;
				tinyRegion.SetIndex(tinyRegionIndex);
				ConstIteratorType tinyPtr(fmriImage, tinyRegion);
				vector<float> tempPatchContent;
				tempPatchContent.reserve(fmriVectorLength*27); // assume that the tiny region size is 3x3x3
				for (tinyPtr.GoToBegin();(!tinyPtr.IsAtEnd());++tinyPtr)
				{
					VectorPixelType tempTinyPatchContent = tinyPtr.Get();
					tempPatchContent.insert(std::end(tempPatchContent),tempTinyPatchContent.GetDataPointer(),tempTinyPatchContent.GetDataPointer()+fmriVectorLength);
				}
				PatchContent[kk] = Col<float>(tempPatchContent);
				
				// old voxel correlation computation
// 				VectorPixelType tempPatchContent = patFMRIPtr.Get();
// 				PatchContent[kk] = Col<float>(tempPatchContent.GetDataPointer(),fmriVectorLength);
				++patFMRIPtr;
			}
			for (unsigned int i=0;i<numTree;i++)
			{
				FeatureCollection[i][currentPatchNum] = featureClass<VectorPixelType>(currentPatchNum,pp,tempLocation);
				FeatureCollection[i][currentPatchNum].funAssignCorr(PatchContent,ranCorrParam[i], numCorrFeature);
			}
			
		}
	
	}
                

        // warp the current data available to fit Yaozong's code
        unsigned int numPatchFeatures = FeatureCollection[0][0].featureWarpper().size();
        cout << "\nFeature Length is: " << numPatchFeatures << endl;
        
        BRIC::IDEA::FISH::TreeTrainingParameters treeParams;
        treeParams.maxTreeDepth = MAXDEPTH;
        treeParams.minElementsOfLeafNode = MINLEAFNUM;
        treeParams.numOfCandidateThresholdsPerWeakLearner = NUMTHRES;
        treeParams.numOfRandomWeakLearners = numPatchFeatures;
        treeParams.minInformationGain = MININFOGAIN;
        

	for (unsigned int i=0;i<numTree;i++)
	{   
		cout << "Working on No." << i+1 << " Tree." << endl;
		boost::timer::auto_cpu_timer t; 
		unsigned int featureSeedNum = seedNum + i;
		BRIC::IDEA::FISH::Random random;
		random.Seed(featureSeedNum);
		
		BRIC::IDEA::FISH::MemoryDataCollection trainingData;
		trainingData.SetFeatureNumber(numPatchFeatures);
		trainingData.SetTargetDim(numTarget);
		
		vector<double>& data = trainingData.GetDataVector();
		vector<double>& targets = trainingData.GetTargetVector();
		
        data.reserve(numPatchFeatures*AllPatchNum);
        targets.reserve(numTarget*AllPatchNum);
		
		for (unsigned long int j=0;j<AllPatchNum;j++)
		{
			for (unsigned int ii=0;ii<tensorVectorLength;ii++)
				targets.push_back(FeatureCollection[i][j].center.label[ii]);
			FeatureCollection[i][j].featureWarpper(data);
		}
		
		string outDataDebugFile = outputIterPath + "/data_debug_" + convertInt(i+1) + ".txt";
		string outLabelsDebugFile = outputIterPath + "/labels_debug_" + convertInt(i+1) + ".txt";
		funSaveVector(data,outDataDebugFile);
		funSaveVector(targets,outLabelsDebugFile);
		
		trainingData.SetSampleNumber(AllPatchNum);
		
		
		// using the Yaozong's TreeTraining code
		BRIC::IDEA::FISH::SimpleRegressionRandTrainingContext context(numPatchFeatures, numTarget);
		cout << "Start formal training process" << endl;
		std::unique_ptr< TreeType > tree = BRIC::IDEA::FISH::TreeTrainer<W,S>::TrainTree(context, treeParams, &trainingData, random);
		
		// Write tree
		string outTreeFile = outTreePath + "/Tree_" + convertInt(i+1) + ".bin";
		ofstream outTree(outTreeFile.data(),std::ios::binary);
		if(!outTree) cerr << "Fail to write the tree." << endl;
		tree->Serialize(outTree);
		outTree.close();
	}
	
// 		cout << "Finished." << endl;
//                 double cpu_duration = (std::clock() - startCPUTime) / (double)CLOCKS_PER_SEC;
//                 std::cout << "Finished in " << cpu_duration << " seconds [CPU Clock] " << std::endl;
//                 std::chrono::duration<double> wctduration = (std::chrono::system_clock::now() - wcts);
//                 std::cout << "Finished in " << wctduration.count() << " seconds [Wall Clock]" << std::endl;
		
		return EXIT_SUCCESS;
}

