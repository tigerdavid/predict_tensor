/*
 * =====================================================================================
 *
 *       Filename:  exec_fetch.cpp
 *
 *    Description:  test the program to execuate the linux command and fetch its output
 *
 *        Version:  1.0
 *        Created:  11/13/2016 04:16:23 PM
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Lichi Zhang (TigerDavid), tigerdavidxeon@gmail.com
 *   Organization:  Shanghai Jiao Tong University
 *
 * =====================================================================================
 */

#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <sstream>

using namespace std;
std::string exec(const char* cmd) {
    char buffer[128];
    std::string result = "";
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer, 128, pipe.get()) != NULL)
            result += buffer;
    }
    return result;
}

int main ()
{
    string str_result=exec("cd ~/mountdata/FunImgF/645551; echo 0 0 0 | img2imgcoord -src warp_tensor.nii.gz -dest tensor.nii -xfm fMRI2T1.mat | sed -n 2p");
    stringstream ss(str_result);
    float x,y,z;
    ss >> x >> y >> z;
    cout << x << endl;
    cout << y << endl;
    cout << z << endl;

    return 0;
    }
