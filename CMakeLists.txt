CMAKE_MINIMUM_REQUIRED(VERSION 2.6)
IF(COMMAND cmake_policy)
  CMAKE_POLICY(VERSION 2.6)
ENDIF(COMMAND cmake_policy)

PROJECT(PREDICT)

SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -std=gnu++0x -fpermissive -Wunused-variable")
 
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -msse -msse2 -msse3")
#  SET(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} -DDEBUG")
#  SET(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG")
#  SET(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)
#  SET(LIBRARY_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/lib)
SET(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}")

SET(INCL
    "${PREDICT_SOURCE_DIR}/"
    "${PREDICT_SOURCE_DIR}/common/"
)
INCLUDE_DIRECTORIES(${INCL})




SET(LIBS 
	"")
LINK_DIRECTORIES(${LIBS})

##
# Required Packages
#
 
# ## VXL
# # FIND_PACKAGE(VXL REQUIRED)
# # IF(VXL_FOUND)
# # 	MESSAGE(STATUS "VXL found")
# # 	INCLUDE(${VXL_CMAKE_DIR}/UseVXL.cmake)
# # ENDIF()
# 
# # ITK
#  FIND_PACKAGE(ITK REQUIRED)
#  IF(ITK_FOUND)
# 	MESSAGE(STATUS "ITK found")
# 	INCLUDE(${ITK_USE_FILE})
# 	MESSAGE(STATUS "ITK_REVIEW must be turned ON!")
# 	SET(ITK_INCL ${ITK_SOURCE_DIR}/Code/Review/)
# 	INCLUDE_DIRECTORIES(${ITK_INCL})
#  ENDIF()
# 
# 
# add_executable(MINE main.cpp)
# TARGET_LINK_LIBRARIES( MINE ${ITK_LIBRARIES})
# 
# 
# ##
# # Optional Packages
# #
# 
# 
# 
# ##
# # Add Subdirectories
# #
# ## ADD_SUBDIRECTORY("${MINE_SOURCE_DIR}/hammer")
# ## ADD_SUBDIRECTORY("${MINE_SOURCE_DIR}/demons")
# 

find_package( BLAS REQUIRED )
message( STATUS BLAS found: ${BLAS_LIBRARIES} )
find_package( LAPACK REQUIRED )
message( STATUS LAPACK found: ${LAPACK_LIBRARIES} )

find_package( Armadillo REQUIRED )
include_directories(${ARMADILLO_INCLUDE_DIRS})
message ( STATUS ARMADILLO found: ${Armadillo_LIBRARIES} )

# #ITK4.4
# 
# find_package(ITK REQUIRED)
# include(${ITK_USE_FILE})
# if (ITKVtkGlue_LOADED)
#  find_package(VTK REQUIRED)
#  include(${VTK_USE_FILE})
# else()
#  find_package(ItkVtkGlue REQUIRED)
#  include(${ItkVtkGlue_USE_FILE})
#  set(Glue ItkVtkGlue)
# endif()

# ITK3
FIND_PACKAGE(ITK REQUIRED)
 IF(ITK_FOUND)
	MESSAGE(STATUS "ITK found")
	INCLUDE(${ITK_USE_FILE})
#	MESSAGE(STATUS "ITK_REVIEW must be turned ON!")
#	SET(ITK_INCL ${ITK_SOURCE_DIR}/Code/Review/)
	INCLUDE_DIRECTORIES(${ITK_INCL})
 ENDIF()
 
 
 #  OpenMP
  find_package(OpenMP)
 if (OPENMP_FOUND)
     set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
     set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
 endif()
 
 
 
# # VXL
# FIND_PACKAGE(VXL REQUIRED)
# IF(VXL_FOUND)
# 	MESSAGE(STATUS "VXL found")
# 	INCLUDE(${VXL_CMAKE_DIR}/UseVXL.cmake)
# ENDIF()

# # GSL
# FIND_PACKAGE(GSL REQUIRED)
# include_directories(${GSL_INCLUDE_DIRS} ${GSLCBLAS_INCLUDE_DIRS})
# set(LIBS ${LIBS} ${GSL_LIBRARIES} ${GSLCBLAS_LIBRARIES})



# # Find VTK.
# FIND_PACKAGE(VTK)
# IF(VTK_FOUND)
#     INCLUDE(${USE_VTK_FILE})
# ELSE(VTK_FOUND)
#     MESSAGE(FATAL_ERROR "VTK not found. Please set VTK_DIR.")
# ENDIF(VTK_FOUND)

set(LIBS ${LIBS} ${ITK_LIBRARIES})
set(ARMALIBS ${LIBS} ${ARMADILLO_LIBRARIES} ${BLAS_LIBRARIES} ${LAPACK_LIBRARIES})


# Find Boost
set(Boost_USE_STATIC_LIBS ON)
set(Boost_USE_MULTITHREADED ON) 
set(Boost_USE_STATIC_RUNTIME OFF)

# add_definitions( -DBOOST_ALL_NO_LIB -DBOOST_ALL_DYN_LINK )
add_definitions( -DBOOST_ALL_NO_LIB )

find_package(Boost REQUIRED COMPONENTS system filesystem timer program_options chrono regex)
include_directories(${Boost_INCLUDE_DIRS})

# set output directory
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
# set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR})


# recursively add each projects
add_subdirectory(apps)

