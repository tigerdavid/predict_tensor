#ifndef FORMAT_H
#define FORMAT_H

#define underline "\33[4m"
#define nounderline "\33[0m"
#define bold "\e[1m"
#define nobold "\e[0m"

namespace Color {
	enum Code {
		FG_RED      = 31,
		FG_GREEN    = 32,
		FG_YELLOW   = 33,
		FG_BLUE     = 34,
		FG_PURPLE   = 35,
		FG_CYAN     = 36,
		FG_DEFAULT  = 39,
		BG_RED      = 41,
		BG_GREEN    = 42,
		BG_YELLOW   = 43,
		BG_BLUE     = 44,
		BG_PURPLE   = 45,
		BG_CYAN     = 46,
		BG_DEFAULT  = 49
	};
	class Modifier {
		Code code;
	public:
		Modifier(Code pCode) : code(pCode) {}
		friend std::ostream&
		operator<<(std::ostream& os, const Modifier& mod) {
			return os << "\033[" << mod.code << "m";
		}
	};
}

Color::Modifier colred(Color::FG_RED);
Color::Modifier colgreen(Color::FG_GREEN);
Color::Modifier colyel(Color::FG_YELLOW);
Color::Modifier colblue(Color::FG_BLUE);
Color::Modifier colpur(Color::FG_PURPLE);
Color::Modifier colcyn(Color::FG_CYAN);

Color::Modifier coldef(Color::FG_DEFAULT);

#define REDLN(x) cout << colred << x << coldef << endl
#define GRNLN(x) cout << colgreen << x << coldef << endl
#define YELLN(x) cout << colyel << x << coldef << endl
#define BLULN(x) cout << colblue << x << coldef << endl
#define PURLN(x) cout << colpur << x << coldef << endl
#define CYNLN(x) cout << colcyn << x << coldef << endl
#define UNDLN(x) cout << underline << x << nounderline << endl
#define BOLDLN(x) cout << bold << x << nobold << endl

#endif