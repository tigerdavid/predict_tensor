/* C-implementation of the affinity propagation clustering algorithm. See */
/* BJ Frey and D Dueck, Science 315, 972-976, Feb 16, 2007, for a         */
/* description of the algorithm.                                          */
/*                                                                        */
/* Copyright 2007, BJ Frey and Delbert Dueck. This software may be freely */
/* used and distributed for non-commercial purposes.                      */

#ifndef APCLUSTER_H
#define APCLUSTER_H

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <omp.h>
/*#include <value.h>*/

#ifndef MINDOUBLE
#define MINDOUBLE 2.2250e-308
#endif
#ifndef MAXDOUBLE
#define MAXDOUBLE 1.7976e308
#endif

using namespace std;

typedef struct {
    int cbSize; /* size of options structure, in bytes */
    float lambda; /* damping factor (between 0.5 and 1) */
    int minimum_iterations, /* minimum number of iterations (=1) */
        converge_iterations, /* number of static iterations until considered converged */
        maximum_iterations; /* maximum number of iterations */
    int nonoise; /* add noise (modifies input similarities!) to break symmetry (=0) or don't add noise (=1) */
} APOPTIONS;

int apclusterf32(float* s,unsigned long* i, unsigned long* k, unsigned long n, unsigned long* idx, double* netsim, APOPTIONS* inOption)
{

    float lam = inOption->lambda;
    unsigned int maxits = inOption->maximum_iterations;
    unsigned int convits = inOption->converge_iterations;
    int conv = 0;
    unsigned long m = n*(n-1);


    if(maxits<1) {
        cerr << "\n\n*** Error: maximum number of iterations must be at least 1\n\n";
        return EXIT_FAILURE;
    }
    if(convits<1) {
        cerr << "\n\n*** Error: number of iterations to test convergence must be at least 1\n\n";
	return EXIT_FAILURE;
    }
    if((lam<0.5)||(lam>=1)) {
        cerr << "\n\n*** Error: damping factor must be between 0.5 and 1\n\n";
	return EXIT_FAILURE;
    }
    printf("\nmaxits=%d, convits=%d, dampfact=%lf\n",maxits,convits,lam);


    /* Allocate memory for similarities, preferences, messages, etc */
    float *a=(float *)calloc(m+n,sizeof(float));
    for(unsigned int j=0; j<m; j++) a[j]=0.0;
    float *r=(float *)calloc(m+n,sizeof(float));
    float *mx1=(float *)calloc(n,sizeof(float));
    float *mx2=(float *)calloc(n,sizeof(float));
    float *srp=(float *)calloc(n,sizeof(float));
    unsigned long **dec=(unsigned long **)calloc(convits,sizeof(unsigned long *));
    for(unsigned int j=0; j<convits; j++)
        dec[j]=(unsigned long *)calloc(n,sizeof(unsigned long));
    for(unsigned int j=0; j<convits; j++) for(unsigned long i1=0; i1<n; i1++) dec[j][i1]=0;
    unsigned long *decsum=(unsigned long *)calloc(n,sizeof(unsigned long));
    for(unsigned int j=0; j<n; j++) decsum[j]=0;

    m=m+n; // Important line needs to be noticed for three times! This is the main reason I was struggled for so long......
    /* Include a tiny amount of noise in similarities to avoid degeneracies */
    if (!inOption->nonoise)
        for(unsigned int j=0; j<m; j++) s[j]=s[j]+(1e-16*s[j]+MINDOUBLE*(10^100))*(rand()/((float)RAND_MAX+1));

    /* Initialize availabilities to 0 and run affinity propagation */
    unsigned int dn=0;
    unsigned int it=0;
    unsigned int decit=convits;
    unsigned long K;
    while(dn==0) 
    {
        it++; /* Increase iteration index */

        /* Compute responsibilities */
        for(unsigned int j=0; j<n; j++) {
            mx1[j]=-MAXDOUBLE;
            mx2[j]=-MAXDOUBLE;
        }
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<m; j++)
        {
            float temp=a[j]+s[j];
            if(temp>mx1[i[j]])
            {
                mx2[i[j]]=mx1[i[j]];
                mx1[i[j]]=temp;
            }
            else if(temp>mx2[i[j]])
                mx2[i[j]]=temp;
        }
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<m; j++)
        {
            float temp=a[j]+s[j];
            if(temp==mx1[i[j]]) r[j]=lam*r[j]+(1-lam)*(s[j]-mx2[i[j]]);
            else r[j]=lam*r[j]+(1-lam)*(s[j]-mx1[i[j]]);
        }

        /* Compute availabilities */
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<n; j++)
            srp[j]=0.0;
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<m-n; j++)
            if(r[j]>0.0)
                srp[k[j]]=srp[k[j]]+r[j];
        #pragma omp parallel for ordered
        for(unsigned int j=m-n; j<m; j++)
            srp[k[j]]=srp[k[j]]+r[j];
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<m-n; j++)
        {
            float temp;
            if(r[j]>0.0)
                temp=srp[k[j]]-r[j];
            else
                temp=srp[k[j]];
            if(temp<0.0)
                a[j]=lam*a[j]+(1-lam)*temp;
            else
                a[j]=lam*a[j];
        }
        #pragma omp parallel for ordered
        for(unsigned int j=m-n; j<m; j++)
            a[j]=lam*a[j]+(1-lam)*(srp[k[j]]-r[j]);

        /* Identify exemplars and check to see if finished */
        decit++;
        if(decit>=convits) decit=0;
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<n; j++)
            decsum[j]=decsum[j]-dec[decit][j];
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<n; j++)
            if(a[m-n+j]+r[m-n+j]>0.0)
                dec[decit][j]=1;
            else
                dec[decit][j]=0;
        K=0;
        for(unsigned int j=0; j<n; j++) K=K+dec[decit][j];
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<n; j++)
            decsum[j]=decsum[j]+dec[decit][j];
        if((it>=convits)||(it>=maxits)) {
            /* Check convergence */
            conv=1;
            for(unsigned int j=0; j<n; j++) if((decsum[j]!=0)&&(decsum[j]!=convits)) conv=0;
            /* Check to see if done */
            if(((conv==1)&&(K>0))||(it==maxits)) dn=1;
        }
    }
    /* If clusters were identified, find the assignments and output them */
    if(K>0) {
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<m; j++)
            if(dec[decit][k[j]]==1)
                a[j]=0.0;
            else
                a[j]=-MAXDOUBLE;
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<n; j++)
            mx1[j]=-MAXDOUBLE;
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<m; j++)
        {
            float temp=a[j]+s[j];
            if(temp>mx1[i[j]])
            {
                mx1[i[j]]=temp;
                idx[i[j]]=k[j];
            }
        }
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<n; j++)
            if(dec[decit][j])
                idx[j]=j;
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<n; j++)
            srp[j]=0.0;
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<m; j++)
            if(idx[i[j]]==idx[k[j]])
                srp[k[j]]=srp[k[j]]+s[j];
        for(unsigned int j=0; j<n; j++) mx1[j]=-MAXDOUBLE;
        for(unsigned int j=0; j<n; j++) if(srp[j]>mx1[idx[j]]) mx1[idx[j]]=srp[j];
        for(unsigned int j=0; j<n; j++)
            if(srp[j]==mx1[idx[j]]) dec[decit][j]=1;
            else dec[decit][j]=0;
        for(unsigned int j=0; j<m; j++)
            if(dec[decit][k[j]]==1) a[j]=0.0;
            else a[j]=-MAXDOUBLE;
        for(unsigned int j=0; j<n; j++) mx1[j]=-MAXDOUBLE;
	#pragma omp parallel for ordered
        for(unsigned int j=0; j<m; j++) 
	{
            float temp=a[j]+s[j];
            if(temp>mx1[i[j]]) 
	    {
                mx1[i[j]]=temp;
                idx[i[j]]=k[j];
            }
        }
        #pragma omp parallel for ordered
        for(unsigned int j=0; j<n; j++)
            if(dec[decit][j]) idx[j]=j;
	    
//	ofstream outIdx("outIdx.txt");
//        for(unsigned int j=0; j<n; j++) outIdx << idx[j]+1 << endl;
//        outIdx.close();
	
        float dpsim=0.0;
        float expref=0.0;
        for(unsigned int j=0; j<m; j++) {
            if(idx[i[j]]==k[j]) {
                if(i[j]==k[j]) expref=expref+s[j];
                else dpsim=dpsim+s[j];
            }
        }
        *netsim=dpsim+expref;
        printf("\nNumber of identified clusters: %lu\n",K);
        printf("Fitness (net similarity): %f\n",*netsim);
        printf("  Similarities of data points to exemplars: %f\n",dpsim);
        printf("  Preferences of selected exemplars: %f\n",expref);
        printf("Number of iterations: %d\n\n",it);
    } else printf("\nDid not identify any clusters\n");
    if(conv==0) {
        printf("\n*** Warning: Algorithm did not converge. Consider increasing\n");
        printf("    maxits to enable more iterations. It may also be necessary\n");
        printf("    to increase damping (increase dampfact).\n\n");
    }
    return EXIT_SUCCESS;
}

#endif
