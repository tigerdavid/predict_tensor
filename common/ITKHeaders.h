#ifndef ITK_H
#define ITK_H


#include "headers.h"

#include "itkImage.h"
// #include "itkSimpleFilterWatcher.h"
// #include "itkImageRegionIterator.h"
#include "itkImageFileWriter.h"
#include "itkImageFileReader.h"
#include "itkNumericSeriesFileNames.h"
#include "itkMinimumMaximumImageCalculator.h"
// #include "itkFFTNormalizedCorrelationImageFilter.h"
#include "itkThresholdImageFilter.h"
#include "itkCropImageFilter.h"
// #include "itkGaussianBlurImageFunction.h"
#include "itkImageRegionIterator.h"
#include "itkImageDuplicator.h"
// #include "itkGrayscaleDilateImageFilter.h"
// #include "itkBinaryBallStructuringElement.h"
#include "itkCannyEdgeDetectionImageFilter.h"
#include "itkDiscreteGaussianImageFilter.h"
#include <itkResampleImageFilter.h>
#include <itkAbsImageFilter.h>
#include <itkSubtractImageFilter.h>
// #include "itkUnaryFunctorImageFilter.h"

#include "itkBinaryDilateImageFilter.h"
#include "itkBinaryBallStructuringElement.h"
#include "itkBinaryErodeImageFilter.h"




// #include "itkAddImageFilter.h"

// #if ITK_VERSION_MAJOR >= 4
// #include "itkMultiplyImageFilter.h"
// #else
// #include "itkMultiplyByConstantImageFilter.h"
// #endif

const int Dimension = 3;

typedef itk::Image< float, Dimension > ImageType;
typedef itk::ImageFileReader<ImageType> ReaderType;
typedef itk::ImageFileWriter<ImageType> WriterType;

typedef itk::Image< unsigned char, Dimension > charImageType;
typedef itk::ImageFileReader<charImageType> charReaderType;
typedef itk::ImageFileWriter<charImageType> charWriterType;

typedef itk::Image< int, Dimension > intImageType;
typedef itk::ImageFileReader<intImageType> intReaderType;
typedef itk::ImageFileWriter<intImageType> intWriterType;

typedef itk::VectorImage<float, Dimension> VectorImageType;
typedef itk::ImageFileReader<VectorImageType> VectorReaderType;
typedef itk::ImageFileWriter<VectorImageType> VectorWriterType;


typedef ImageType::PixelType ImgPixelType;
typedef VectorImageType::PixelType VectorPixelType;

typedef itk::MinimumMaximumImageCalculator <ImageType> ImageCalculatorFilterType;
typedef itk::DiscreteGaussianImageFilter<ImageType, ImageType> filterType;
typedef itk::NumericSeriesFileNames NameGeneratorType;

// WriterType::Pointer writer = WriterType::New();
// VectorWriterType::Pointer vectorWriter = VectorWriterType::New();

// General Reader and Writer function
// TODO: replace all the related reader and writer part with this ones
template< typename TImageType >
typename TImageType::Pointer ImageReader(const string inFile)
{
	DMSG("Reading image " << inFile);
//	cout << "Reading image " << inFile << endl;
	typedef itk::ImageFileReader<TImageType > GeneralFileReader;
	typename GeneralFileReader::Pointer inReader = GeneralFileReader::New();
	inReader->SetFileName(inFile.data());
	inReader->Update();
	typename TImageType::Pointer inImage = inReader->GetOutput();
	return inImage;
}

template< typename TImageType >
void ImageWriter(typename TImageType::Pointer outImage, string outFile)
{
	DMSG("Writing image " << outFile);
//	cout << "Writing image " << outFile << endl;
	typedef itk::ImageFileWriter<TImageType > GeneralFileWriter;
	typename GeneralFileWriter::Pointer outWriter = GeneralFileWriter::New();
	outWriter->SetFileName(outFile.data());
	outWriter->SetInput(outImage);
	outWriter->Update();
}

template < typename TImageType >
typename TImageType::Pointer ImageInitializer(typename TImageType::Pointer refImage, const int lengthVector = 0,const float valFill = 0)
{
	typename TImageType::Pointer resImage = TImageType::New();
	resImage->CopyInformation(refImage);
	resImage->SetRegions(refImage->GetLargestPossibleRegion());
	if (lengthVector != 0)
		resImage->SetNumberOfComponentsPerPixel(lengthVector);
	resImage->Allocate();
	
	typename TImageType::PixelType vecFill;
	vecFill.SetSize(lengthVector);
	vecFill.Fill(valFill);
	resImage->FillBuffer(vecFill);
	
	return resImage;
}

template < typename TImageType >
typename TImageType::Pointer ZeroPad(typename TImageType::Pointer refImage, const int padSize)
{
	typename TImageType::PixelType vecFill;
	vecFill.SetSize(refImage->GetNumberOfComponentsPerPixel());
	vecFill.Fill(0);
	
	typename TImageType::Pointer resImage = TImageType::New();
	resImage->CopyInformation(refImage);
	typename TImageType::RegionType inRegion = refImage->GetLargestPossibleRegion();
	typename TImageType::SizeType inSize = inRegion.GetSize();
	typename TImageType::SizeType outSize;
	outSize[0] = inSize[0] + 2*padSize;
	outSize[1] = inSize[1] + 2*padSize;
	outSize[2] = inSize[2] + 2*padSize;
	typename TImageType::RegionType outRegion;
	outRegion.SetIndex(inRegion.GetIndex());
	outRegion.SetSize(outSize);
	resImage->SetRegions(outRegion);
	resImage->SetNumberOfComponentsPerPixel(refImage->GetNumberOfComponentsPerPixel());
	resImage->Allocate();
	resImage->FillBuffer(vecFill);
	
	typedef itk::ImageRegionIterator< TImageType > IteratorType;
	IteratorType inPtr(refImage,inRegion);
	
	for (inPtr.GoToBegin();!inPtr.IsAtEnd();++inPtr)
	{
		typename TImageType::IndexType inPixelIndex = inPtr.GetIndex();
		typename TImageType::PixelType PixelValue = inPtr.Get();
		typename TImageType::IndexType outPixelIndex;
		outPixelIndex[0] = inPixelIndex[0] + padSize;
		outPixelIndex[1] = inPixelIndex[1] + padSize;
		outPixelIndex[2] = inPixelIndex[2] + padSize;
		resImage->SetPixel(outPixelIndex,PixelValue);
	}
	
	return resImage;
}

template < typename TImageType > 
typename TImageType::Pointer rmZeroPad(typename TImageType::Pointer refImage, const int padSize)
{
	typename TImageType::PixelType vecFill;
	vecFill.SetSize(refImage->GetNumberOfComponentsPerPixel());
	vecFill.Fill(0);
	
	typename TImageType::Pointer resImage = TImageType::New();
	resImage->CopyInformation(refImage);
	typename TImageType::RegionType inRegion = refImage->GetLargestPossibleRegion();
	typename TImageType::SizeType inSize = inRegion.GetSize();
	typename TImageType::SizeType outSize;
	outSize[0] = inSize[0] - 2*padSize;
	outSize[1] = inSize[1] - 2*padSize;
	outSize[2] = inSize[2] - 2*padSize;
	typename TImageType::RegionType outRegion;
	outRegion.SetIndex(inRegion.GetIndex());
	outRegion.SetSize(outSize);
	resImage->SetRegions(outRegion);
	resImage->SetNumberOfComponentsPerPixel(refImage->GetNumberOfComponentsPerPixel());
	resImage->Allocate();
	resImage->FillBuffer(vecFill);
	
	typedef itk::ImageRegionIterator< TImageType > IteratorType;
	IteratorType ouPtr(resImage,outRegion);
	
	for (ouPtr.GoToBegin();!ouPtr.IsAtEnd();++ouPtr)
	{
		typename TImageType::IndexType outPixelIndex = ouPtr.GetIndex();
		typename TImageType::IndexType inPixelIndex;
		inPixelIndex[0] = outPixelIndex[0] + padSize;
		inPixelIndex[1] = outPixelIndex[1] + padSize;
		inPixelIndex[2] = outPixelIndex[2] + padSize;
		typename TImageType::PixelType PixelValue = refImage->GetPixel(inPixelIndex);
		resImage->SetPixel(outPixelIndex,PixelValue);
	}
	
	return resImage;
}

template < typename TImageType >
typename TImageType::Pointer ImageReaderPad(const string inFile, const int padSize)
{
	typename TImageType::Pointer tmpImage = ImageReader<TImageType>(inFile.data());
	typename TImageType::Pointer padImage = TImageType::New();
	padImage = ZeroPad<TImageType>(tmpImage, padSize);
	return padImage;
}

template < typename TImageType > 
void ImageWriterPad(typename TImageType::Pointer outImage, string outFile, const int padSize)
{
	typename TImageType::Pointer saveImage = TImageType::New();
	saveImage = rmZeroPad<TImageType>(outImage, padSize);
	ImageWriter<TImageType>(saveImage, outFile);
}


namespace itk {
	template<typename TValue>
	TValue* begin(const VariableLengthVector<TValue> & v_) 
	{ 
		return const_cast<TValue*>(v_.GetDataPointer()); 
	}
	
	template<typename TValue>
	TValue* end(const VariableLengthVector<TValue> & v_)   
	{ 
		return begin(v_)+v_.GetSize(); 
	}
}

#endif
