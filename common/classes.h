// FIXME, TODO, HACK, NOTE, BUG
// NOTE: all the header content for the training and testing works

#ifndef CLASSES_H
#define CLASSES_H

#include "common/common.h"
#include "functors.h"


// Needed by Yaozong's code (Forest's training code)
#include "forest/Tree.h"
#include "forest/ForestTrainer.h"
#include "common/Random.h"
#include "forest/DataCollection.h"
#include "forest/stat/SimpleRegressionStatisticsAggregator.h"
#include "forest/tester/SimpleRegressor.h"
#include "forest/context/SimpleRegressionRandTrainingContext.h"
// #include "forest/context/ClassificationExhaustSearchTrainingContext.h"


// using namespace arma;
typedef BRIC::IDEA::FISH::MemoryAxisAlignedWeakLearner W;
typedef BRIC::IDEA::FISH::SimpleRegressionStatisticsAggregator S;
// typedef BRIC::IDEA::FISH::HistogramStatisticsAggregator S;
typedef BRIC::IDEA::FISH::Tree<W,S> TreeType;
typedef BRIC::IDEA::FISH::Forest<W,S> ForestType;


enum NormalizationType {
        NONE,
        UNIT_NORM,
        MAX_ONE,
        MIN_MAX,
        ZERO_MEAN_ONE_STD
};

const int chosenNormalization = UNIT_NORM;

//TODO: generate a class that wrap up the content


// template <typename T>
// class contentClass
// {
// public:
// 	
// }

//TODO: generate a class that wrap up the parameter settings (including the feature parameters(using friend))

template <typename T>
class featureClass  // class for the feature of single patch
{
public:
	long index;
	int subjIndex;
	location<T> center;
	vector<float> featureCorr; // for correlation features from fMRI
	vector<float> featureHaar; // for Haar-like features for mapping usage
	vector<float> featureContext; // for the Haar-like features of auto-context model
	vector<float> featureMean; // for Haar-like features of mean values from fMRI
	vector<float> featureEPIHaar; // for Haar-like features of mean-EPI from fMRI
	vector<float> featureProbHaar; // for Haar-like features of GM/WM/CSF seg prob map from T1, containing three prob maps altogether
	
	featureClass() {};
	featureClass(long int argIndex, int argSubjIndex, location< T > argCenter);
	~featureClass();
	
	inline void funAssignCorr(const vector<Col<float> > &argContent, const vector<vector<int> > &inputParamCorr, int numCorrFeature);
	inline void funAssignHaar(vector<Cube<float> > &argHaarContent, const vector<vector<int> > &inputParamHaar,int numHaarFeature);
	inline void funAssignContext(vector< Cube< float > >& ContextContent, const vector< vector< int > >& inputParamContext, int numContextFeature);
	inline void funAssignMean(Cube< float> &argMeanContent, const vector< vector< int > >& inputParamMean, int numMeanFeature);
	inline void funAssignEPIHaar(Cube< float> &argEPIContent, const vector< vector< int> >&inputParamEPIHaar, int numEPIHaarFeature);
	inline void funAssignProbHaar(Cube<float> &argProbContent1, Cube<float> &argProbContent2, Cube<float> &argProbContent3, const vector< vector< int> >&inputParamProbHaar, int numHaarFeature);
	
        inline void funCorrCompute(const vector< Col< float > >& patchContent, const vector< vector< int > >& inputParamCorr); // inline for the class function to increase speed
	inline void funNormalization(int type, Cube< float >& content);
	inline void funHaarCompute(const Cube< float >& intpatch, const vector< vector< int > >& inputParamHaar, vector< float >& resFeature);
	
// 	void infoSubjReader(umat tumorData);
        inline vector< float > featureWarpper();
	inline void featureWarpper(vector<float> &outFeatures);
        
        
        featureClass<T>& operator=(const featureClass<T>& rhs)
        {
                if (this == &rhs)
                        return *this;
                index = rhs.index;
                subjIndex = rhs.subjIndex;
                center = rhs.center;
		featureCorr = rhs.featureCorr;
                featureHaar = rhs.featureHaar;
		featureContext = rhs.featureContext;
		featureMean = rhs.featureMean;
		featureEPIHaar = rhs.featureEPIHaar;
		featureProbHaar = rhs.featureProbHaar;
                
                return *this;
        }
        
        featureClass<T>& operator==(const featureClass<T>& rhs) const
        {
                if (subjIndex == rhs.subjIndex && center == rhs.center)
                        return true;
                else
                        return false;
        }
                

};

template <typename T>
void featureClass<T>::funNormalization(int type,Cube<float> &content)
{
	switch (type)
	{
		case NONE:
			break;
		case UNIT_NORM:
		{
			float valNorm = sqrt(accu(pow(content,2)));
			content = content/valNorm;
			break;
		}
		case MAX_ONE:
		{
			content = content/content.max();
			break;
		}
		case MIN_MAX:
		{
			content = (content-content.min())/(content.max()-content.min());
			break;
		}
		case ZERO_MEAN_ONE_STD:
		{
			const int patchSize = content.n_cols;
			float* tempPointer = content.memptr();
			const Mat<float> tempMat = reshape(Mat<float>(tempPointer,content.n_elem,1,false),patchSize*patchSize,patchSize);
			float valStdDev = stddev(vectorise(tempMat));
			content = (content-accu(content)/pow(patchSize,3))/valStdDev;
			break;
		}
		default:
			cout << "Wrong normalization type, program is processed without normalizing patches" << endl;
	}
	
}


template <typename T>
void featureClass<T>::funHaarCompute(const Cube< float >& intpatch, const vector< vector< int > >& inputParamHaar, vector<float> &resFeature)
{

	for (auto paramIterator = inputParamHaar.begin();paramIterator!=inputParamHaar.end();++paramIterator)
	{
		int blockType = (*paramIterator)[0];
		int blockRad1 = (*paramIterator)[1];
		int x1 = (*paramIterator)[3];
		int y1 = (*paramIterator)[4];
		int z1 = (*paramIterator)[5];
		float haarBlock1 = accu(intpatch.subcube(x1-blockRad1,y1-blockRad1,z1-blockRad1,x1+blockRad1,y1+blockRad1,z1+blockRad1));
		if (blockType == 0) // one block
			resFeature.push_back(haarBlock1);
		else
		{
			int blockRad2 = (*paramIterator)[2];
			int x2 = (*paramIterator)[6];
			int y2 = (*paramIterator)[7];
			int z2 = (*paramIterator)[8];
			float haarBlock2 = accu(intpatch.subcube(x2-blockRad2,y2-blockRad2,z2-blockRad2,x2+blockRad2,y2+blockRad2,z2+blockRad2));
			resFeature.push_back(haarBlock1-haarBlock2);
		}
			
	}
}

template <typename T>
void featureClass<T>::funCorrCompute(const vector< Col< float > >& patchContent, const vector< vector< int > >& inputParamCorr)
{  
	//      #pragma omp parallel for ordered
	for (unsigned int i=0;i<featureCorr.size();i++)
		featureCorr[i] = as_scalar(cor(patchContent[inputParamCorr[i][0]],patchContent[inputParamCorr[i][1]]));
}


template <typename T>
featureClass<T>::featureClass(long int argIndex, int argSubjIndex, location< T > argCenter)
{
	index = argIndex;
	subjIndex = argSubjIndex;
	center = argCenter;
	
}

// template <typename T>
// featureClass<T>::featureClass(patchClass< T > inputPatch)
// {
// 	index = inputPatch.index; 
// 	subjIndex = inputPatch.subjIndex;
// 	center = inputPatch.center;
// }

template <typename T>
featureClass<T>::~featureClass()
{
	index = 0;
	subjIndex = 0;
	center.Clear();
	featureCorr.clear();
	featureHaar.clear();
	featureContext.clear();
	featureMean.clear();
	featureEPIHaar.clear();
	featureProbHaar.clear();
}

template <typename T>
void featureClass<T>::funAssignCorr(const vector<Col<float> > &argContent, const vector<vector<int> > &inputParamCorr, int numCorrFeature)
{
	featureCorr.resize(numCorrFeature);
	funCorrCompute(argContent,inputParamCorr);
}

template <typename T>
void featureClass<T>::funAssignHaar(vector<Cube<float> > &argHaarContent,const vector<vector<int> > &inputParamHaar,int numHaarFeature)
{
	const int targetVectorLength = argHaarContent.size();
	featureHaar.reserve(targetVectorLength*numHaarFeature);
	
	for (int i=0;i<targetVectorLength;i++)
	{
		funNormalization(chosenNormalization,argHaarContent[i]);
		funHaarCompute(argHaarContent[i],inputParamHaar,featureHaar);
	}
}

template <typename T>
void featureClass<T>::funAssignEPIHaar(Cube<float> &argEPIHaarContent, const vector<vector<int> > &inputParamEPIHaar, int numEPIHaarFeature)
{
	featureEPIHaar.reserve(numEPIHaarFeature);
	funNormalization(chosenNormalization,argEPIHaarContent);
	funHaarCompute(argEPIHaarContent,inputParamEPIHaar,featureEPIHaar);
	
}

template <typename T>
void featureClass<T>::funAssignProbHaar(Cube<float> &argProbContent1, Cube<float> &argProbContent2, Cube<float> &argProbContent3, const vector< vector< int> >&inputParamProbHaar, int numProbHaarFeature)
{
	vector<float> featureProb1;
	vector<float> featureProb2;
	vector<float> featureProb3;
	featureProb1.reserve(numProbHaarFeature);
	featureProb2.reserve(numProbHaarFeature);
	featureProb3.reserve(numProbHaarFeature);
	funNormalization(chosenNormalization,argProbContent1);
	funNormalization(chosenNormalization,argProbContent2);
	funNormalization(chosenNormalization,argProbContent3);
	funHaarCompute(argProbContent1,inputParamProbHaar,featureProb1);
	funHaarCompute(argProbContent2,inputParamProbHaar,featureProb2);
	funHaarCompute(argProbContent3,inputParamProbHaar,featureProb3);
	featureProbHaar.insert(std::end(featureProbHaar),std::begin(featureProb1),std::end(featureProb1));
	featureProbHaar.insert(std::end(featureProbHaar),std::begin(featureProb2),std::end(featureProb2));
	featureProbHaar.insert(std::end(featureProbHaar),std::begin(featureProb3),std::end(featureProb3));

}

template <typename T>
void featureClass<T>::funAssignContext(vector<Cube<float> > &ContextContent, const vector<vector<int> > &inputParamContext,int numContextFeature)
{
	// context content
	const int tensorVectorLength = ContextContent.size();
	featureContext.reserve(tensorVectorLength*numContextFeature);
	
	for ( int i=0;i<tensorVectorLength;i++)
	{
		funNormalization(chosenNormalization,ContextContent[i]);
		funHaarCompute(ContextContent[i],inputParamContext,featureContext);
	}
}

template <typename T>
void featureClass<T>::funAssignMean(Cube< float >& argMeanContent, const vector< vector< int > >& inputParamMean, int numMeanFeature)
{
	featureMean.reserve(numMeanFeature);
	funNormalization(chosenNormalization,argMeanContent);
	funHaarCompute(argMeanContent,inputParamMean,featureMean);
}

template <typename T>
vector<float> featureClass<T>::featureWarpper() //////////////////// very important
{
        vector<float> outFeatures;
// 	for (unsigned int i=0;i<featurePatch.n_elem;i++)
// 		outFeatures.push_back((float)featurePatch(i));
//	outFeatures.push_back(center.x);
//	outFeatures.push_back(center.y);
//	outFeatures.push_back(center.z);
	outFeatures.insert(std::end(outFeatures),std::begin(featureEPIHaar),std::end(featureEPIHaar));
	outFeatures.insert(std::end(outFeatures),std::begin(featureCorr),std::end(featureCorr));
	outFeatures.insert(std::end(outFeatures),std::begin(featureHaar),std::end(featureHaar));
	outFeatures.insert(std::end(outFeatures),std::begin(featureMean),std::end(featureMean));
	outFeatures.insert(std::end(outFeatures),std::begin(featureProbHaar),std::end(featureProbHaar));
	outFeatures.insert(std::end(outFeatures),std::begin(featureContext),std::end(featureContext));
	
        return outFeatures;
}

template <typename T>
void featureClass<T>::featureWarpper(vector<float> &outFeatures) //////////////////// very important
{
//	outFeatures.push_back(center.x);
//	outFeatures.push_back(center.y);
//	outFeatures.push_back(center.z);
	outFeatures.insert(std::end(outFeatures),std::begin(featureEPIHaar),std::end(featureEPIHaar));
	outFeatures.insert(std::end(outFeatures),std::begin(featureCorr),std::end(featureCorr));
	outFeatures.insert(std::end(outFeatures),std::begin(featureHaar),std::end(featureHaar));
	outFeatures.insert(std::end(outFeatures),std::begin(featureMean),std::end(featureMean));
	outFeatures.insert(std::end(outFeatures),std::begin(featureProbHaar),std::end(featureProbHaar));
	outFeatures.insert(std::end(outFeatures),std::begin(featureContext),std::end(featureContext));
}

void featureExtraction(unsigned long int currentPatchNum, unsigned int currentSubj, location<VectorPixelType> tempLocation, VectorImageType::Pointer fTensorImage, \
vector<float> &outFeatures, vector<float> &outTargets, unsigned int PatchSize,vector<vector<int> >& currentParamHaar)
{
	
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	
	featureClass<VectorPixelType> FeatureIndv;
	
	VectorImageType::SizeType regionSize;
	regionSize.Fill(PatchSize);
	
	VectorImageType::SizeType radiusSize;
	radiusSize.Fill(radius);
	
	VectorImageType::RegionType smallRegion;
	smallRegion.SetSize(regionSize);
	VectorImageType::IndexType regionIndex = tempLocation.GetIndex() - radiusSize;
	smallRegion.SetIndex(regionIndex);
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType patFTPtr(fTensorImage, smallRegion);
	patFTPtr.GoToBegin();
	const unsigned int fTensorVectorLength = fTensorImage->GetNumberOfComponentsPerPixel();
	const unsigned int dTensorVectorLength = tempLocation.label.Size();
	
	vector<Cube<ImgPixelType> > fTensorContent(fTensorVectorLength);
	for (unsigned int kk=0;kk < fTensorVectorLength;kk++)
		fTensorContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	
	
	for (unsigned int kk = 0; kk < PatchSize3; kk++)
	{				
		VectorImageType::IndexType tempIndex = patFTPtr.GetIndex();
		VectorPixelType tempFTensorValue = patFTPtr.Get();
		for (unsigned int jj = 0; jj < fTensorVectorLength;jj++)
			fTensorContent[jj](tempIndex[0]-regionIndex[0],tempIndex[1]-regionIndex[1],tempIndex[2]-regionIndex[2]) = tempFTensorValue[jj];
		++patFTPtr;
	}
	FeatureIndv = featureClass<VectorPixelType>(currentPatchNum,currentSubj,tempLocation);
	FeatureIndv.funAssignHaar(fTensorContent,currentParamHaar, currentParamHaar.size());

	for (unsigned int ii=0;ii<dTensorVectorLength;ii++)
		outTargets.push_back(FeatureIndv.center.label[ii]);
	FeatureIndv.featureWarpper(outFeatures);
}

void featureExtractionProb(unsigned long int currentPatchNum, unsigned int currentSubj, location<VectorPixelType> tempLocation, VectorImageType::Pointer fTensorImage, \
VectorImageType::Pointer probImage1, VectorImageType::Pointer probImage2, VectorImageType::Pointer probImage3, vector<float> &outFeatures, vector<float> &outTargets, \
unsigned int PatchSize,vector<vector<int> >& currentParamHaar,vector<vector<int> >& currentParamProbHaar)
{
	
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	
	featureClass<VectorPixelType> FeatureIndv;
	
	VectorImageType::SizeType regionSize;
	regionSize.Fill(PatchSize);
	
	VectorImageType::SizeType radiusSize;
	radiusSize.Fill(radius);
	
	VectorImageType::RegionType smallRegion;
	smallRegion.SetSize(regionSize);
	VectorImageType::IndexType regionIndex = tempLocation.GetIndex() - radiusSize;
	smallRegion.SetIndex(regionIndex);
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType patFTPtr(fTensorImage, smallRegion);
	patFTPtr.GoToBegin();
	ConstIteratorType patProbPtr1(probImage1,smallRegion);
	ConstIteratorType patProbPtr2(probImage2,smallRegion);
	ConstIteratorType patProbPtr3(probImage3,smallRegion);
	patProbPtr1.GoToBegin();patProbPtr2.GoToBegin();patProbPtr3.GoToBegin();
	const unsigned int fTensorVectorLength = fTensorImage->GetNumberOfComponentsPerPixel();
	const unsigned int dTensorVectorLength = tempLocation.label.Size();
	
	vector<Cube<ImgPixelType> > fTensorContent(fTensorVectorLength);
	for (unsigned int kk=0;kk < fTensorVectorLength;kk++)
		fTensorContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	Cube<ImgPixelType> probContent1 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	Cube<ImgPixelType> probContent2 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	Cube<ImgPixelType> probContent3 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	
	
	for (unsigned int kk = 0; kk < PatchSize3; kk++)
	{	
		VectorImageType::IndexType tempIndex = patFTPtr.GetIndex();
		VectorPixelType tempFTensorValue = patFTPtr.Get();
		for (unsigned int jj = 0; jj < fTensorVectorLength;jj++)
			fTensorContent[jj](tempIndex[0]-regionIndex[0],tempIndex[1]-regionIndex[1],tempIndex[2]-regionIndex[2]) = tempFTensorValue[jj];
		++patFTPtr;
		
		VectorImageType::IndexType probIndex1 = patProbPtr1.GetIndex();
		VectorPixelType tempProbValue1 = patProbPtr1.Get();
		probContent1(probIndex1[0]-regionIndex[0],probIndex1[1]-regionIndex[1],probIndex1[2]-regionIndex[2]) = tempProbValue1[0];
		++patProbPtr1;
		
		VectorImageType::IndexType probIndex2 = patProbPtr2.GetIndex();
		VectorPixelType tempProbValue2 = patProbPtr2.Get();
		probContent2(probIndex2[0]-regionIndex[0],probIndex2[1]-regionIndex[1],probIndex2[2]-regionIndex[2]) = tempProbValue2[0];
		++patProbPtr2;
		
		VectorImageType::IndexType probIndex3 = patProbPtr3.GetIndex();
		VectorPixelType tempProbValue3 = patProbPtr3.Get();
		probContent3(probIndex3[0]-regionIndex[0],probIndex3[1]-regionIndex[1],probIndex3[2]-regionIndex[2]) = tempProbValue3[0];
		++patProbPtr3;
	}
	FeatureIndv = featureClass<VectorPixelType>(currentPatchNum,currentSubj,tempLocation);
	FeatureIndv.funAssignHaar(fTensorContent,currentParamHaar, currentParamHaar.size());
	FeatureIndv.funAssignProbHaar(probContent1,probContent2,probContent3,currentParamProbHaar,currentParamProbHaar.size());
	
	for (unsigned int ii=0;ii<dTensorVectorLength;ii++)
		outTargets.push_back(FeatureIndv.center.label[ii]);
	FeatureIndv.featureWarpper(outFeatures);
}

void featureExtractionFMRI(unsigned long int currentPatchNum, unsigned int currentSubj, location<VectorPixelType> tempLocation, VectorImageType::Pointer fmriImage, \
VectorImageType::Pointer probImage1, VectorImageType::Pointer probImage2, VectorImageType::Pointer probImage3, vector<float> &outFeatures, vector<float> &outTargets, \
unsigned int PatchSize,vector<vector<int> >& currentParamCorr,vector<vector<int> >& currentParamProbHaar)
{
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	
	featureClass<VectorPixelType> FeatureIndv;
	const unsigned int fmriVectorLength = fmriImage->GetNumberOfComponentsPerPixel();
	const unsigned int dTensorVectorLength = tempLocation.label.Size();
	
	VectorImageType::SizeType regionSize;
	regionSize.Fill(PatchSize);
	
	VectorImageType::SizeType radiusSize;
	radiusSize.Fill(radius);
	
	VectorImageType::RegionType smallRegion;
	smallRegion.SetSize(regionSize);
	VectorImageType::IndexType regionIndex = tempLocation.GetIndex() - radiusSize;
	smallRegion.SetIndex(regionIndex);
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType patFMRIPtr(fmriImage, smallRegion);
	patFMRIPtr.GoToBegin();
	
	vector<Col<float> > PatchContent;
	PatchContent.reserve(PatchSize3);			
	
	// initialize probImage information
	ConstIteratorType patProbPtr1(probImage1,smallRegion);
	ConstIteratorType patProbPtr2(probImage2,smallRegion);
	ConstIteratorType patProbPtr3(probImage3,smallRegion);
	patProbPtr1.GoToBegin();patProbPtr2.GoToBegin();patProbPtr3.GoToBegin();
	Cube<ImgPixelType> probContent1 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	Cube<ImgPixelType> probContent2 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	Cube<ImgPixelType> probContent3 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	
	for (unsigned int kk = 0; kk < PatchSize3; kk++)
	{					
		VectorImageType::IndexType probIndex1 = patProbPtr1.GetIndex();
		VectorPixelType tempProbValue1 = patProbPtr1.Get();
		probContent1(probIndex1[0]-regionIndex[0],probIndex1[1]-regionIndex[1],probIndex1[2]-regionIndex[2]) = tempProbValue1[0];
		++patProbPtr1;
		
		VectorImageType::IndexType probIndex2 = patProbPtr2.GetIndex();
		VectorPixelType tempProbValue2 = patProbPtr2.Get();
		probContent2(probIndex2[0]-regionIndex[0],probIndex2[1]-regionIndex[1],probIndex2[2]-regionIndex[2]) = tempProbValue2[0];
		++patProbPtr2;
		
		VectorImageType::IndexType probIndex3 = patProbPtr3.GetIndex();
		VectorPixelType tempProbValue3 = patProbPtr3.Get();
		probContent3(probIndex3[0]-regionIndex[0],probIndex3[1]-regionIndex[1],probIndex3[2]-regionIndex[2]) = tempProbValue3[0];
		++patProbPtr3;
		
		vector<float> targetValue;
		targetValue.reserve(fmriVectorLength);
		VectorPixelType tempPatchContent = patFMRIPtr.Get();
		targetValue.insert(std::end(targetValue),tempPatchContent.GetDataPointer(),tempPatchContent.GetDataPointer()+fmriVectorLength);
		PatchContent.push_back(Col<float>(targetValue));
		++patFMRIPtr;
	}
	
	FeatureIndv = featureClass<VectorPixelType>(currentPatchNum,currentSubj,tempLocation);
	FeatureIndv.funAssignCorr(PatchContent,currentParamCorr,currentParamCorr.size());
	FeatureIndv.funAssignProbHaar(probContent1,probContent2,probContent3,currentParamProbHaar,currentParamProbHaar.size());
	
	for (unsigned int ii=0;ii<dTensorVectorLength;ii++)
		outTargets.push_back(FeatureIndv.center.label[ii]);
	FeatureIndv.featureWarpper(outFeatures);
	
}

void featureExtractionFMRIContext(unsigned long int currentPatchNum, unsigned int currentSubj, location<VectorPixelType> tempLocation, VectorImageType::Pointer fmriImage, \
VectorImageType::Pointer probImage1, VectorImageType::Pointer probImage2, VectorImageType::Pointer probImage3, VectorImageType::Pointer contextImage, \
vector<float> &outFeatures, vector<float> &outTargets, unsigned int PatchSize,vector<vector<int> >& currentParamCorr,vector<vector<int> >& currentParamProbHaar,vector<vector<int> >& currentParamContext)
{
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	
	featureClass<VectorPixelType> FeatureIndv;
	const unsigned int fmriVectorLength = fmriImage->GetNumberOfComponentsPerPixel();
	const unsigned int dTensorVectorLength = tempLocation.label.Size();
	
	VectorImageType::SizeType regionSize;
	regionSize.Fill(PatchSize);
	
	VectorImageType::SizeType radiusSize;
	radiusSize.Fill(radius);
	
	VectorImageType::RegionType smallRegion;
	smallRegion.SetSize(regionSize);
	VectorImageType::IndexType regionIndex = tempLocation.GetIndex() - radiusSize;
	smallRegion.SetIndex(regionIndex);
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType patFMRIPtr(fmriImage, smallRegion);
	patFMRIPtr.GoToBegin();
	ConstIteratorType patConPtr(contextImage, smallRegion);
	patConPtr.GoToBegin();
	
	vector<Col<float> > PatchContent;
	PatchContent.reserve(PatchSize3);			
	
	// initialize probImage information
	ConstIteratorType patProbPtr1(probImage1,smallRegion);
	ConstIteratorType patProbPtr2(probImage2,smallRegion);
	ConstIteratorType patProbPtr3(probImage3,smallRegion);
	patProbPtr1.GoToBegin();patProbPtr2.GoToBegin();patProbPtr3.GoToBegin();
	Cube<ImgPixelType> probContent1 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	Cube<ImgPixelType> probContent2 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	Cube<ImgPixelType> probContent3 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	
	// initialize contextImage information
	vector<Cube<ImgPixelType> > ContextContent(dTensorVectorLength);
	for (unsigned int kk=0;kk < dTensorVectorLength;kk++)
		ContextContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	
	for (unsigned int kk = 0; kk < PatchSize3; kk++)
	{					
		VectorImageType::IndexType probIndex1 = patProbPtr1.GetIndex();
		VectorPixelType tempProbValue1 = patProbPtr1.Get();
		probContent1(probIndex1[0]-regionIndex[0],probIndex1[1]-regionIndex[1],probIndex1[2]-regionIndex[2]) = tempProbValue1[0];
		++patProbPtr1;
		
		VectorImageType::IndexType probIndex2 = patProbPtr2.GetIndex();
		VectorPixelType tempProbValue2 = patProbPtr2.Get();
		probContent2(probIndex2[0]-regionIndex[0],probIndex2[1]-regionIndex[1],probIndex2[2]-regionIndex[2]) = tempProbValue2[0];
		++patProbPtr2;
		
		VectorImageType::IndexType probIndex3 = patProbPtr3.GetIndex();
		VectorPixelType tempProbValue3 = patProbPtr3.Get();
		probContent3(probIndex3[0]-regionIndex[0],probIndex3[1]-regionIndex[1],probIndex3[2]-regionIndex[2]) = tempProbValue3[0];
		++patProbPtr3;
		
		vector<float> targetValue;
		targetValue.reserve(fmriVectorLength);
		VectorPixelType tempPatchContent = patFMRIPtr.Get();
		targetValue.insert(std::end(targetValue),tempPatchContent.GetDataPointer(),tempPatchContent.GetDataPointer()+fmriVectorLength);
		PatchContent.push_back(Col<float>(targetValue));
		++patFMRIPtr;
		
		VectorImageType::IndexType ConIndex = patConPtr.GetIndex();
		VectorPixelType tempContextValue = patConPtr.Get();
		for (unsigned int jj = 0; jj < dTensorVectorLength;jj++)
			ContextContent[jj](ConIndex[0]-regionIndex[0],ConIndex[1]-regionIndex[1],ConIndex[2]-regionIndex[2]) = tempContextValue[jj];
		++patConPtr;
	}
	
	FeatureIndv = featureClass<VectorPixelType>(currentPatchNum,currentSubj,tempLocation);
	FeatureIndv.funAssignCorr(PatchContent,currentParamCorr,currentParamCorr.size());
	FeatureIndv.funAssignProbHaar(probContent1,probContent2,probContent3,currentParamProbHaar,currentParamProbHaar.size());
	FeatureIndv.funAssignContext(ContextContent,currentParamContext,currentParamContext.size());
	
	for (unsigned int ii=0;ii<dTensorVectorLength;ii++)
		outTargets.push_back(FeatureIndv.center.label[ii]);
	FeatureIndv.featureWarpper(outFeatures);
	
}

void featureExtractionEPI(unsigned long int currentPatchNum, unsigned int currentSubj, location<VectorPixelType> tempLocation, VectorImageType::Pointer fTensorImage, \
VectorImageType::Pointer EPIImage, vector<float> &outFeatures, vector<float> &outTargets, unsigned int PatchSize,vector<vector<int> >& currentParamHaar, vector<vector<int> >& currentParamEPIHaar)
{
	
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	
	featureClass<VectorPixelType> FeatureIndv;
	
	VectorImageType::SizeType regionSize;
	regionSize.Fill(PatchSize);
	
	VectorImageType::SizeType radiusSize;
	radiusSize.Fill(radius);
	
	VectorImageType::RegionType smallRegion;
	smallRegion.SetSize(regionSize);
	VectorImageType::IndexType regionIndex = tempLocation.GetIndex() - radiusSize;
	smallRegion.SetIndex(regionIndex);
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType patFTPtr(fTensorImage, smallRegion);
	patFTPtr.GoToBegin();
	ConstIteratorType patEPIPtr(EPIImage,smallRegion);
	patEPIPtr.GoToBegin();
	
	const unsigned int fTensorVectorLength = fTensorImage->GetNumberOfComponentsPerPixel();
	const unsigned int dTensorVectorLength = tempLocation.label.Size();
	
	vector<Cube<ImgPixelType> > fTensorContent(fTensorVectorLength);
	for (unsigned int kk=0;kk < fTensorVectorLength;kk++)
		fTensorContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	Cube<ImgPixelType> EPIContent = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	
	
	for (unsigned int kk = 0; kk < PatchSize3; kk++)
	{				
		VectorImageType::IndexType FTIndex = patFTPtr.GetIndex();
		VectorPixelType tempFTensorValue = patFTPtr.Get();
		for (unsigned int jj = 0; jj < fTensorVectorLength;jj++)
			fTensorContent[jj](FTIndex[0]-regionIndex[0],FTIndex[1]-regionIndex[1],FTIndex[2]-regionIndex[2]) = tempFTensorValue[jj];
		++patFTPtr;
		
		VectorImageType::IndexType EPIIndex = patEPIPtr.GetIndex();
		VectorPixelType tempEPIValue = patEPIPtr.Get();
		EPIContent(EPIIndex[0]-regionIndex[0],EPIIndex[1]-regionIndex[1],EPIIndex[2]-regionIndex[2]) = tempEPIValue[0];
		++patEPIPtr;
	}
	FeatureIndv = featureClass<VectorPixelType>(currentPatchNum,currentSubj,tempLocation);
	FeatureIndv.funAssignHaar(fTensorContent,currentParamHaar, currentParamHaar.size());
	FeatureIndv.funAssignEPIHaar(EPIContent, currentParamEPIHaar,currentParamEPIHaar.size());
	
	for (unsigned int ii=0;ii<dTensorVectorLength;ii++)
		outTargets.push_back(FeatureIndv.center.label[ii]);
	FeatureIndv.featureWarpper(outFeatures);
}

void featureExtractionContext(unsigned long int currentPatchNum, unsigned int currentSubj, location<VectorPixelType> tempLocation, VectorImageType::Pointer fTensorImage, \
VectorImageType::Pointer contextImage, vector<float> &outFeatures, vector<float> &outTargets, unsigned int PatchSize,vector<vector<int> >& currentParamHaar,vector<vector<int> >& currentParamContext)
{
	
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	
	featureClass<VectorPixelType> FeatureIndv;
	
	VectorImageType::SizeType regionSize;
	regionSize.Fill(PatchSize);
	
	VectorImageType::SizeType radiusSize;
	radiusSize.Fill(radius);
	
	VectorImageType::RegionType smallRegion;
	smallRegion.SetSize(regionSize);
	VectorImageType::IndexType regionIndex = tempLocation.GetIndex() - radiusSize;
	smallRegion.SetIndex(regionIndex);
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType patFTPtr(fTensorImage, smallRegion);
	patFTPtr.GoToBegin();
	ConstIteratorType patConPtr(contextImage, smallRegion);
	patConPtr.GoToBegin();

	const unsigned int fTensorVectorLength = fTensorImage->GetNumberOfComponentsPerPixel();
	const unsigned int dTensorVectorLength = tempLocation.label.Size();
	
	vector<Cube<ImgPixelType> > fTensorContent(fTensorVectorLength);
	for (unsigned int kk=0;kk < fTensorVectorLength;kk++)
		fTensorContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	vector<Cube<ImgPixelType> > ContextContent(dTensorVectorLength);
	for (unsigned int kk=0;kk < dTensorVectorLength;kk++)
		ContextContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	
	
	for (unsigned int kk = 0; kk < PatchSize3; kk++)
	{
		VectorImageType::IndexType FTIndex = patFTPtr.GetIndex();
		VectorPixelType tempFTensorValue = patFTPtr.Get();
		for (unsigned int jj = 0; jj < fTensorVectorLength;jj++)
			fTensorContent[jj](FTIndex[0]-regionIndex[0],FTIndex[1]-regionIndex[1],FTIndex[2]-regionIndex[2]) = tempFTensorValue[jj];
		
		VectorImageType::IndexType ConIndex = patConPtr.GetIndex();
		VectorPixelType tempContextValue = patConPtr.Get();
		for (unsigned int jj = 0; jj < dTensorVectorLength;jj++)
			ContextContent[jj](ConIndex[0]-regionIndex[0],ConIndex[1]-regionIndex[1],ConIndex[2]-regionIndex[2]) = tempContextValue[jj];
		++patFTPtr;++patConPtr;
	}
	FeatureIndv = featureClass<VectorPixelType>(currentPatchNum,currentSubj,tempLocation);
	FeatureIndv.funAssignHaar(fTensorContent,currentParamHaar, currentParamHaar.size());
	FeatureIndv.funAssignContext(ContextContent,currentParamContext,currentParamContext.size());

	for (unsigned int ii=0;ii<dTensorVectorLength;ii++)
		outTargets.push_back(FeatureIndv.center.label[ii]);
	FeatureIndv.featureWarpper(outFeatures);
}

void featureExtractionProbContext(unsigned long int currentPatchNum, unsigned int currentSubj, location<VectorPixelType> tempLocation, VectorImageType::Pointer fTensorImage, \
VectorImageType::Pointer probImage1, VectorImageType::Pointer probImage2, VectorImageType::Pointer probImage3, VectorImageType::Pointer contextImage, \
vector<float> &outFeatures, vector<float> &outTargets, unsigned int PatchSize,vector<vector<int> >& currentParamHaar,vector<vector<int> >& currentParamProbHaar,vector<vector<int> >& currentParamContext)
{
	
	const unsigned int PatchSize3 = pow(PatchSize,3);
	const unsigned int radius = (PatchSize - 1)/2;
	
	featureClass<VectorPixelType> FeatureIndv;
	
	VectorImageType::SizeType regionSize;
	regionSize.Fill(PatchSize);
	
	VectorImageType::SizeType radiusSize;
	radiusSize.Fill(radius);
	
	VectorImageType::RegionType smallRegion;
	smallRegion.SetSize(regionSize);
	VectorImageType::IndexType regionIndex = tempLocation.GetIndex() - radiusSize;
	smallRegion.SetIndex(regionIndex);
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType patFTPtr(fTensorImage, smallRegion);
	patFTPtr.GoToBegin();
	ConstIteratorType patConPtr(contextImage, smallRegion);
	patConPtr.GoToBegin();
	ConstIteratorType patProbPtr1(probImage1,smallRegion);
	ConstIteratorType patProbPtr2(probImage2,smallRegion);
	ConstIteratorType patProbPtr3(probImage3,smallRegion);
	patProbPtr1.GoToBegin();patProbPtr2.GoToBegin();patProbPtr3.GoToBegin();
	const unsigned int fTensorVectorLength = fTensorImage->GetNumberOfComponentsPerPixel();
	const unsigned int dTensorVectorLength = tempLocation.label.Size();
	
	vector<Cube<ImgPixelType> > fTensorContent(fTensorVectorLength);
	for (unsigned int kk=0;kk < fTensorVectorLength;kk++)
		fTensorContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	vector<Cube<ImgPixelType> > ContextContent(dTensorVectorLength);
	for (unsigned int kk=0;kk < dTensorVectorLength;kk++)
		ContextContent[kk] = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	Cube<ImgPixelType> probContent1 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	Cube<ImgPixelType> probContent2 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	Cube<ImgPixelType> probContent3 = Cube<ImgPixelType>(PatchSize,PatchSize,PatchSize);
	
	
	for (unsigned int kk = 0; kk < PatchSize3; kk++)
	{	
		VectorImageType::IndexType tempIndex = patFTPtr.GetIndex();
		VectorPixelType tempFTensorValue = patFTPtr.Get();
		for (unsigned int jj = 0; jj < fTensorVectorLength;jj++)
			fTensorContent[jj](tempIndex[0]-regionIndex[0],tempIndex[1]-regionIndex[1],tempIndex[2]-regionIndex[2]) = tempFTensorValue[jj];
		++patFTPtr;
		
		VectorImageType::IndexType ConIndex = patConPtr.GetIndex();
		VectorPixelType tempContextValue = patConPtr.Get();
		for (unsigned int jj = 0; jj < dTensorVectorLength;jj++)
			ContextContent[jj](ConIndex[0]-regionIndex[0],ConIndex[1]-regionIndex[1],ConIndex[2]-regionIndex[2]) = tempContextValue[jj];
		++patConPtr;
		
		VectorImageType::IndexType probIndex1 = patProbPtr1.GetIndex();
		VectorPixelType tempProbValue1 = patProbPtr1.Get();
		probContent1(probIndex1[0]-regionIndex[0],probIndex1[1]-regionIndex[1],probIndex1[2]-regionIndex[2]) = tempProbValue1[0];
		++patProbPtr1;
		
		VectorImageType::IndexType probIndex2 = patProbPtr2.GetIndex();
		VectorPixelType tempProbValue2 = patProbPtr2.Get();
		probContent2(probIndex2[0]-regionIndex[0],probIndex2[1]-regionIndex[1],probIndex2[2]-regionIndex[2]) = tempProbValue2[0];
		++patProbPtr2;
		
		VectorImageType::IndexType probIndex3 = patProbPtr3.GetIndex();
		VectorPixelType tempProbValue3 = patProbPtr3.Get();
		probContent3(probIndex3[0]-regionIndex[0],probIndex3[1]-regionIndex[1],probIndex3[2]-regionIndex[2]) = tempProbValue3[0];
		++patProbPtr3;
	}
	FeatureIndv = featureClass<VectorPixelType>(currentPatchNum,currentSubj,tempLocation);
	FeatureIndv.funAssignHaar(fTensorContent,currentParamHaar, currentParamHaar.size());
	FeatureIndv.funAssignContext(ContextContent,currentParamContext,currentParamContext.size());
	FeatureIndv.funAssignProbHaar(probContent1,probContent2,probContent3,currentParamProbHaar,currentParamProbHaar.size());
	
	for (unsigned int ii=0;ii<dTensorVectorLength;ii++)
		outTargets.push_back(FeatureIndv.center.label[ii]);
	FeatureIndv.featureWarpper(outFeatures);
}
// template <typename T>
// std::ostream& operator<<(std::ostream& os, const featureClass<T>& feature)
// {
//         
// }


//backup code

/*
 t em*plate <typename T>
 Cube< float > featureClass<T>::funIntegralPatch(const Cube<float> &content) // converted from IntegralImage in Haar3DFeatures.h of yaozong's code
 {
 Cube<float> res = zeros<Cube<float> >(size(content));
 const int patchSize = content.n_cols;
 
 
 for (int i = 1;i < patchSize; i++)
 {
 res(i,0,0) = content(i,0,0) +content(i-1,0,0);
 res(0,i,0) = content(0,i,0) +content(0,i-1,0);
 res(0,0,i) = content(0,0,i) + content(0,0,i-1);
 }
 
 for ( int z = 1; z < patchSize; ++z) 
 {
 for ( int y = 1; y < patchSize; ++y)
 {
 res(0,y,z) = content(0,y,z) + content(0,y-1,z) + content(0,y,z-1) - content(0,y-1,z-1);
 //assert(res(0,y,z)+1e-4 >= res(0,y-1,z));
 //assert(res(0,y,z)+1e-4 >= res(0,y,z-1));
 }
 }
 // y = 0 plane
 for ( int z = 1; z < patchSize; ++z) 
 {
 for ( int x = 1; x < patchSize; ++x) 
 {
 res(x,0,z) = content(x,0,z) + content(x-1,0,z) + content(x,0,z-1) - content(x-1,0,z-1);
 //assert(res(x,0,z)+1e-4 >= res(x,0,z-1));
 //assert(res(x,0,z)+1e-4 >= res(x-1,0,z));
 }
 }
 // z = 0 plane
 for ( int y = 1; y < patchSize; ++y) 
 {
 for ( int x = 1; x < patchSize; ++x) 
 {
 res(x,y,0) = content(x,y,0) + content(x-1,y,0) + content(x,y-1,0) - content(x-1,y-1,0);
 //assert(res(x,y,0)+1e-4 >= res(x,y-1,0));
 //assert(res(x,y,0)+1e-4 >= res(x-1,y,0));
 }
 }
 
 // rest volume
 for ( int z = 1; z < patchSize; ++z) 
 {
 for ( int y = 1; y < patchSize; ++y) 
 {
 for ( int x = 1; x < patchSize; ++x) 
 {
 res(x,y,z) = content(x,y,z) + content(x-1,y,z) + content(x,y-1,z) + content(x,y,z-1)
 - content(x-1,y-1,z) - content(x-1,y,z-1) - content(x,y-1,z-1) + content(x-1,y-1,z-1);
 //assert(res(x,y,z)+1e-4 >= res(x-1,y,z));
 //assert(res(x,y,z)+1e-4 >= res(x,y-1,z));
 //assert(res(x,y,z)+1e-4 >= img(x,y,z-1));
 }
 }
 }
 
 return res;
 }
 
 template <typename T>
 float featureClass<T>::funRectSum(const Cube< float> &intpatch, Col<int> sp, Col<int> ep) /////
 {
 double v111 = intpatch(ep(0),ep(1),ep(2)); 
 double v011 = (sp(0) == 0) ? 0:intpatch(sp(0)-1,ep(1),ep(2)); 
 double v101 = (sp(1) == 0) ? 0:intpatch(ep(0),sp(1)-1,ep(2)); 
 double v110 = (sp(2) == 0) ? 0:intpatch(ep(0),ep(1),sp(2)-1); 
 double v001 = (sp(0) == 0 || sp(1) == 0) ? 0 : intpatch(sp(0)-1, sp(1)-1, ep(2)); 
 double v010 = (sp(0) == 0 || sp(2) == 0) ? 0 : intpatch(sp(0)-1, ep(1),   sp(2)-1); 
 double v100 = (sp(1) == 0 || sp(2) == 0) ? 0 : intpatch(ep(0),   sp(1)-1, sp(2)-1); 
 double v000 = (sp(0) == 0 || sp(1) == 0 || sp(2) == 0) ? 0: intpatch(sp(0)-1, sp(1)-1, sp(2)-1); 
 
 float rect_sum = v111-v011-v101-v110+v001+v010+v100-v000;
 return rect_sum;
 }*/

/*
 tem*plate <typename T>
 Cube<float> featureClass<T>::funNormalization(int type,Cube<float> &content)
 {
 Cube<float> res = zeros<Cube<float> >(size(content));
 switch (type)
 {
	 case NONE:
		 res = content;
		 break;
	 case UNIT_NORM:
	 {
	 float valNorm = sqrt(accu(pow(content,2)));
	 res = content/valNorm;
	 break;
	 }
	 case MAX_ONE:
	 {
	 res = content/content.max();
	 break;
	 }
	 case MIN_MAX:
	 {
	 res = (content-content.min())/(content.max()-content.min());
	 break;
	 }
	 case ZERO_MEAN_ONE_STD:
	 {
	 const int patchSize = content.n_cols;
	 float* tempPointer = content.memptr();
	 const Mat<float> tempMat = reshape(Mat<float>(tempPointer,content.n_elem,1,false),patchSize*patchSize,patchSize);
	 float valStdDev = stddev(vectorise(tempMat));
	 res = (content-accu(content)/pow(patchSize,3))/valStdDev;
	 break;
	 }
	 default:
		 res = content;
		 cout << "Wrong normalization type, program is processed without normalizing patches" << endl;
		 }
		 return res;
		 
}*/


/*
template <typename T>
class patchClass
{
public:
	long index;
	location<T> center;
	int subjIndex;
	vector<Col<float> > content;
	// 	vector<location<T> > context;
	// 	Col<T> featurePatch; //used to be 
	// 	Col<T> featureContext;
	
	patchClass() {};
	patchClass(long int argIndex, location< T >& argCenter, int argSubjIndex, vector< Col< float > >& argContent);
	// 	patchClass(long int argIndex, location< T > argCenter, int argSubjIndex, vector<Col<float> > argContent, vector<location<T> > argContext);
	// 	patchClass(long argIndex, location<T> argCenter, int argSubjIndex, vector<T> argContent, vector<location<float> > argContext);
	
	
	// 	~patchClass();
	
	//         void funNormalization(int type);
	// 	Cube<T> funIntegralPatch();
	// 	void funGatherContext(mat prevResult);
	// 	void funContextCompute(vector<location<float> > argContext);
	
	
	patchClass<T>& operator=(const patchClass<T>& rhs)
	{
		if (this == &rhs)
			return *this;
		
		index = rhs.index;
		center = rhs.center;
		subjIndex = rhs.subjIndex;
		content = rhs.content;
		// 		context = rhs.context;
		// 		featurePatch = rhs.featurePatch;
		// 		featureContext = rhs.featureContext;
		
		return *this;
	}
	
	bool operator==(const patchClass<T>& rhs) const // const on the left means const patchClass *this, meaning the decleared member won't be able to change its values during the process of this function
	{
		// 		if (center == rhs.center || subjIndex == rhs.subjIndex || content == rhs.content || context = rhs.context)
		if (center == rhs.center || subjIndex == rhs.subjIndex || content == rhs.content)
			return true;
		else
			return false;
	}
	
	// 	bool isContentEmpty() const
	// 	{
	// 		return content.is_empty();
	// 	}
};

template <typename T>
patchClass<T>::patchClass(long int argIndex, location< T > &argCenter, int argSubjIndex, vector<Col<float> > &argContent)
{
	index = argIndex;
	center  = argCenter;
	subjIndex = argSubjIndex;
	content = argContent;
	
}

// template <typename T>
// patchClass<T>::patchClass(long int argIndex, location< T > argCenter, int argSubjIndex, vector<Col<float> > argContent, vector<location<T> > argContext)
// {
// 	index = argIndex;
// 	center  = argCenter;
// 	subjIndex = argSubjIndex;
// 	content = argContent;
// 	context =argContext;
// }


// template <typename T>
// std::ostream& operator<<(std::ostream& os, const patchClass<T>& patch)
// {
//         
// }*/

#endif
