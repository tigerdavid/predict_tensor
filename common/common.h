// All the stuffs that might be needed for other cpp files

#ifndef COMMON_H
#define COMMON_H

#include "headers.h"
#include "ITKHeaders.h"
#include <armadillo>
#include "IO.h"
#include "apcluster_source.h"




// #include "ImageViewer.h"

using namespace arma;


template<class Matrix>
void print_matrix(Matrix matrix) {
	matrix.print(std::cout);
}

//provide explicit instantiations of the template function for 
//every matrix type you use somewhere in your program.
//eg: call print_matrix<arma::Mat<double> >(matrix) 
template void print_matrix<arma::mat>(arma::mat matrix);
template void print_matrix<arma::cx_mat>(arma::cx_mat matrix);
template void print_matrix<arma::vec >(arma::vec matrix);


string convertInt(int number)
{
	stringstream ss;//create a stringstream
	ss << number;//add number to the stream
	return ss.str();//return a string with the contents of the stream
}


// to check the current memory usuage of the process
void process_mem_usage()
{
	using std::ios_base;
	using std::ifstream;
	using std::string;
	
	double vm_usage     = 0.0;
	double resident_set = 0.0;
	
	// 'file' stat seems to give the most reliable results
	//
	ifstream stat_stream("/proc/self/stat",ios_base::in);
	
	// dummy vars for leading entries in stat that we don't care about
	//
	string pid, comm, state, ppid, pgrp, session, tty_nr;
	string tpgid, flags, minflt, cminflt, majflt, cmajflt;
	string utime, stime, cutime, cstime, priority, nice;
	string O, itrealvalue, starttime;
	
	// the two fields we want
	//
	unsigned long vsize;
	long rss;
	
	stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
	>> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
	>> utime >> stime >> cutime >> cstime >> priority >> nice
	>> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest
	
	stat_stream.close();
	
	long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
	vm_usage     = vsize / 1024.0;
	resident_set = rss * page_size_kb;
	
	cout << "VM: " << vm_usage << "; RSS: " << resident_set << endl;
}

template <typename T>
class location
{
public:
	int x;
	int y;
	int z;
	T label;
// 	int MapIndex;
	
	location() {};
	location(int arg_x, int arg_y, int arg_z, T arg_label)
	{
		x = arg_x;
		y = arg_y;
		z = arg_z;
		label = arg_label;
	}
	location(itk::Index<Dimension> imageIndex, T arg_label)
	{
		SetIndex(imageIndex);
		label = arg_label;
	}
	location(int arg_x, int arg_y, int arg_z)
	{
		x = arg_x;
		y = arg_y;
		z = arg_z;
	}
	location(itk::Index<Dimension> imageIndex)
	{
		SetIndex(imageIndex);
	}
	~location() {Clear();};
	
	bool operator==(const location& temploc) const {
		if(x == temploc.x && y == temploc.y && z == temploc.z && label == temploc.label)
			return true;
		else
			return false;
	}
	location& operator=(const location& rhs) 
	{
		if(this == &rhs)
			return *this;
		x=rhs.x;
		y=rhs.y;
		z=rhs.z;
		label=rhs.label;
		
		return *this;
	}	
	
	void Serialize(std::ostream& o) const {
                o << x << " " << y << " " << z << " " << label << endl;
        }
        
        void Deserialize(std::istream& in) {
                in >> x >> y >> z >> label;
        }
        
        itk::Index<Dimension> GetIndex()
	{
		itk::Index<Dimension> resIndex;
		resIndex[0] = x;
		resIndex[1] = y;
		resIndex[2] = z;
		
		return resIndex;
	}
	
	void SetIndex(itk::Index<Dimension> inIndex)
	{
		x = inIndex[0];
		y = inIndex[1];
		z = inIndex[2];
	}
	
	
	
	T GetValue()
	{
		return label;
	}
	
	void Clear()
	{
		x = y = z = 0;
	}
        
	
};

template <typename T1, typename T2>
bool CloseLocation(location<T1> Loc1, location<T2> Loc2, int distance)
{
	int radius = (distance-1)/2;
	int disX = abs(Loc1.x - Loc2.x);
	int disY = abs(Loc1.y - Loc2.y);
	int disZ = abs(Loc1.z - Loc2.z);
	if ((disX<=radius) &&(disY<=radius)&&(disZ<=radius))
		return true;
	else
		return false;
}


template <typename T>
bool funLocInCollection(location<T> inLocation, vector<location<T> > LocationCollection)
{
	bool res = false;
	for (auto i = LocationCollection.begin();i != LocationCollection.end();++i)
		if (*i == inLocation)
			res = true;
	return res;
}


struct tmpStruct
{
	ImageType::PixelType weight;
	ImageType::PixelType ProbPixelValue;
	tmpStruct(ImageType::PixelType w, ImageType::PixelType p) : weight(w), ProbPixelValue(p) {}
};


bool NotZero (tmpStruct i) { return (i.weight != 0);}

struct sortless //decrease order
{
	inline bool operator() (const tmpStruct& struct1, const tmpStruct& struct2)
	{
		return (struct1.weight > struct2.weight);
	}
};

struct sortmore //increase order
{
	inline bool operator() (const tmpStruct& struct1, const tmpStruct& struct2)
	{
		if (struct1.weight != struct2.weight)
			return (struct1.weight < struct2.weight);
		else
			return (struct1.ProbPixelValue < struct2.ProbPixelValue);
	}
};


VectorPixelType funAbs(const VectorPixelType &inputVector)
{
	VectorPixelType res = inputVector;
	for (unsigned int i=0;i<inputVector.Size();i++)
		res[i] = abs(inputVector[i]);
	return res;
}

ImgPixelType funMean(const VectorPixelType &inputVector)
{
// 	ImgPixelType result = 0;
// 	for (unsigned int i=0;i<inputVector.Size();i++)
// 		result+=inputVector[i];
// 	return result/(inputVector.Size());
	auto iBegin(itk::begin<float>(inputVector));
	auto iEnd(itk::end<float>(inputVector));
	return accumulate(iBegin,iEnd,(float)0.0)/(inputVector.Size());
}

ImgPixelType funSTD(const VectorPixelType &inputVector)
{
	ImgPixelType result = 0;
	auto resMean = funMean(inputVector);
	for (unsigned int i=0;i<inputVector.Size();i++)
		result+=pow((inputVector[i]-resMean),2);
	return sqrt(result/(inputVector.Size()));
}

bool funNotZero(const VectorPixelType &inputVector)
{
	ImgPixelType result = 0;
	auto resMean = funMean(inputVector);
	for (unsigned int i=0;i<inputVector.Size();i++)
		result+=pow((inputVector[i]-resMean),2);
	
	return ((result > 0)|(resMean > 0));
}

// bubble sort on float array, in an increase order
void bubbleSort(float* arr, int* index, int n)
{
	bool swapped = true;
	int j = 0;
	float tmp;
	int index_tmp;
	while (swapped)
	{
		swapped = false;
		j++;
		for (int i = 0; i < n - j; i++)
		{
			if (arr[i] > arr[i + 1])
			{
				tmp = arr[i];
				arr[i] = arr[i + 1];
				arr[i + 1] = tmp;
				
				index_tmp = index[i];
				index[i] = index[i+1];
				index[i+1] = index_tmp;
				
				swapped = true;
			}
		}
	}
}


ImageType::Pointer errcompare(ImageType::Pointer image1, ImageType::Pointer image2)
{
	
	//Further initialisation
	ImageType::RegionType InRegion = image1->GetLargestPossibleRegion();
	ImageType::SizeType InSize = InRegion.GetSize();
	ImageType::PixelType InPixelValue1;
	ImageType::PixelType InPixelValue2;
// 	ImageType::PixelType MaskPixelValue;
	ImageType::IndexType InPixelIndex;
// 	ImageType::SpacingType InSpacing = image1->GetSpacing();
	ImageType::PointType InOrigin;
	
	ImageType::RegionType OutRegion = InRegion;
	ImageType::SizeType OutSize = InSize;
	
	ImageType::Pointer errResult = ImageType::New();
	errResult->CopyInformation(image1);
	errResult->SetRegions(OutRegion);
	errResult->Allocate();
	errResult->FillBuffer(0);
	
	for (int k = 0; k < (int) OutSize[2]; k++)
	{
		for (int j = 0; j < (int) OutSize[1]; j++)
		{
			for (int i = 0; i < (int) OutSize[0]; i++)
			{
				InPixelIndex[0] = i;
				InPixelIndex[1] = j;
				InPixelIndex[2] = k;
				InPixelValue1 = image1->GetPixel(InPixelIndex);
				InPixelValue2 = image2->GetPixel(InPixelIndex);
				
				if (InPixelValue1 != InPixelValue2)
					errResult->SetPixel(InPixelIndex,1);
			}
		}
	}
	
	return errResult;
}

float intcompare(ImageType::Pointer image1, ImageType::Pointer image2)
{
	
	ImageType::PixelType sumdiff = 0.0;
	int count = 0;
	
	typedef itk::ImageRegionConstIterator<ImageType> ConstIteratorType;
	ConstIteratorType retImgPtr1( image1, image1->GetRequestedRegion());
	ConstIteratorType retImgPtr2( image2, image2->GetRequestedRegion());
	
	for (retImgPtr1.GoToBegin(),retImgPtr2.GoToBegin();!retImgPtr1.IsAtEnd();++retImgPtr1,++retImgPtr2)
	{
		ImageType::PixelType InPixelValue1 = retImgPtr1.Get();
		ImageType::PixelType InPixelValue2 = retImgPtr2.Get();
		
		if ((InPixelValue1 > 0) || (InPixelValue2 > 0))
		{
			count++;
			sumdiff += pow((InPixelValue1-InPixelValue2),2);
		}
	}
	
	float meandiff = sqrt(sumdiff/count);
	
	return meandiff;
}

VectorImageType::PixelType intcompare(VectorImageType::Pointer image1, VectorImageType::Pointer image2)
{
	const unsigned int vectorLength = image1->GetNumberOfComponentsPerPixel();
	
	VectorImageType::PixelType sumdiff;
	sumdiff.Fill(0.0);
	int count = 0;
	
	typedef itk::ImageRegionConstIterator<VectorImageType> ConstIteratorType;
	ConstIteratorType retImgPtr1( image1, image1->GetRequestedRegion());
	ConstIteratorType retImgPtr2( image2, image2->GetRequestedRegion());
	
// 	typedef itk::VariableLengthVector<float> VariableVectorType;
	
	for (retImgPtr1.GoToBegin(),retImgPtr2.GoToBegin();!retImgPtr1.IsAtEnd();++retImgPtr1,++retImgPtr2)
	{
		VectorImageType::PixelType InPixelValue1 = retImgPtr1.Get();
		VectorImageType::PixelType InPixelValue2 = retImgPtr2.Get();
		
		if (funNotZero(InPixelValue1) || funNotZero(InPixelValue2))
		{
			count++;
			VectorImageType::PixelType diffValue = InPixelValue1 - InPixelValue2;
			for (unsigned int i=0;i<vectorLength;i++)
				sumdiff[i]+=pow(diffValue[i],2);
		}
	}
	
	VectorImageType::PixelType meandiff;
	for (unsigned int i=0;i<vectorLength;i++)
		meandiff[i] = sqrt(sumdiff[i]/count);
	
	return meandiff;
}


float compare(ImageType::Pointer image1, ImageType::Pointer image2, string selectLabelPath, bool verbose)
{

	// read the selectlabelFile
	vector<double> selectLabel;
	ifstream selectlabelFile(selectLabelPath.data());
	string labelline;
	while (getline(selectlabelFile,labelline))
	{
		double temp;
		istringstream iaa(labelline);
		if (!(iaa >> temp)) {
			break;
		}
		selectLabel.push_back(temp);
	}
	const int labelCount = selectLabel.size();
	selectlabelFile.close();
	
	
	//Further initialisation
	ImageType::RegionType InRegion = image1->GetLargestPossibleRegion();
	ImageType::SizeType InSize = InRegion.GetSize();
	ImageType::PixelType InPixelValue1;
	ImageType::PixelType InPixelValue2;
	ImageType::IndexType InPixelIndex;
	ImageType::PointType InOrigin;
	
	ImageType::RegionType OutRegion = InRegion;
	ImageType::SizeType OutSize = InSize;
	
	vector<long> countImage1(labelCount,0);
	vector<long> countImage2(labelCount,0);
	vector<long> correctCount(labelCount,0);
	vector<float> diceall(labelCount,0);
	int indImage;
	
	for (int k = 0; k < (int) OutSize[2]; k++)
	{
		for (int j = 0; j < (int) OutSize[1]; j++)
		{
			for (int i = 0; i < (int) OutSize[0]; i++)
			{
				InPixelIndex[0] = i;
				InPixelIndex[1] = j;
				InPixelIndex[2] = k;
				InPixelValue1 = image1->GetPixel(InPixelIndex);
				InPixelValue2 = image2->GetPixel(InPixelIndex);
				
				if (InPixelValue1 > 0) {indImage = distance(selectLabel.begin(),find(selectLabel.begin(),selectLabel.end(),InPixelValue1));countImage1[indImage]++;}
				if (InPixelValue2 > 0) {indImage = distance(selectLabel.begin(),find(selectLabel.begin(),selectLabel.end(),InPixelValue2));countImage2[indImage]++;}
				
				if ((InPixelValue1 > 0) && (InPixelValue2 > 0 ) && (InPixelValue1 == InPixelValue2)) correctCount[indImage]++;
			}
		}
	}
	
	
	float similarity = 0.0;
	for (int i=0;i<labelCount;i++)
	{
		diceall[i] = (2.0*correctCount[i])/(countImage1[i]+countImage2[i]);
		if (isnan(diceall[i])) diceall[i] = 1;
		if (verbose) cout << selectLabel[i] << "\t" << correctCount[i] << "\t" << countImage1[i] << "\t" << countImage2[i] << "\t" << diceall[i] << endl;
		similarity+=diceall[i]/labelCount;
	}
	
	return similarity;
}


// do Affinity Propagation Clustering
void APClustering(float** distanceMatrix, int groupSize, int &APclustersize,
		  int* &APclustersubrep, int* &APclustersubsize, int** &APcluster, float pref)
{
	// for APcluster
	
// 	float* APsij = new float[groupSize*groupSize];
	float* APsijd = new float[groupSize*groupSize];
// 	unsigned int* APi = new unsigned int[groupSize*groupSize];
// 	unsigned int* APj = new unsigned int[groupSize*groupSize];
	unsigned long* APid = new unsigned long[groupSize*groupSize];
	unsigned long* APjd = new unsigned long[groupSize*groupSize];
	
	const int groupSize2 = groupSize*(groupSize-1);
	
	int APindexij = 0;
	for (int i = 0; i < groupSize; i++)
	{
		for (int j = 0; j < groupSize; j++)
		{
// 			APi[i*groupSize+j] = i;
// 			APj[i*groupSize+j] = j;
			if (i!=j)
			{
				APid[APindexij] = i;
				APjd[APindexij] = j;
				APindexij++;
			}
			
		}
	}
	
	///////////////////////////////////////
	// run affinity propagation on the current pairwise distance matrix
	
	//  // use APCluster to do clustering and save results
// 	ofstream outSimilarity("outSimilarity.txt");
	int APsindexij = 0;
	for(int i = 0; i < groupSize; i++)
	{
		for (int j = 0; j < groupSize; j++)
		{
// 			APsij[i*groupSize+j] = 0-distanceMatrix[i][j];
			if (i!=j)
			{
				APsijd[APsindexij] = 0-distanceMatrix[i][j];
				APsindexij++;
// 				outSimilarity << i+1 << "  " << j+1 << "  " << -distanceMatrix[i][j] << endl;
			}
		}
	}
// 	outSimilarity.close();
	
	float* APsijd_copy = new float[groupSize*groupSize-groupSize];
	
// 	// find median of APsijd to set all APsij[i][i]
// 	int* APSortindexij = new int[groupSize2];
	for (int i = 0; i < groupSize2; i++)
		APsijd_copy[i] = APsijd[i];
// 		APSortindexij[i]=i;
	// 	cout << "Start sorting" << endl;
	
	sort(APsijd_copy, APsijd_copy+groupSize2);
	
	cout << "sorting finished, Start clustering" << endl;
// 	ofstream outPreference("outPreference.txt");
	float APmedian = (APsijd_copy[(groupSize2)/2]+APsijd_copy[(groupSize2)/2-1])/2*pref;
	for (int i = 0; i < groupSize; i++)
	{
		APsijd[groupSize2+i]=APmedian;
		APid[groupSize2+i] = i;
		APjd[groupSize2+i] = i;
// 		outPreference << APmedian << endl;
	}
// 	outPreference.close();
// 	delete[] APSortindexij;
// 	delete[] APsij;
	
	
	APOPTIONS AP_options= {0};
	int APiter;
	unsigned long *APidx = 0;
	double APnetsim = 0.0;
	AP_options.cbSize = sizeof(APOPTIONS);
	AP_options.lambda = 0.9;
	AP_options.minimum_iterations = 1;
	AP_options.converge_iterations = 200;
	AP_options.nonoise = 0;
	AP_options.maximum_iterations = 2000;
	time_t start_comp,end_comp;
	time (&start_comp);
	APiter = apclusterf32(APsijd, APid, APjd, groupSize, APidx=(unsigned long*)calloc(groupSize,sizeof(*APidx)), &APnetsim, &AP_options);
	time (&end_comp);
	double elapsed_secs = difftime(end_comp,start_comp);
	cout << "Elapsed time is " << elapsed_secs << endl;	
	
	if (APiter > 0)
	{
		printf("Error code: %d\n", APiter);
		return;
	}

		
	cout << "APclustering is finished." << endl;
	
	
	vector<tmpStruct> APoutSort;
	for (int i = 0; i< groupSize; i++)
		APoutSort.push_back(tmpStruct(APidx[i],i));
	sort(APoutSort.begin(),APoutSort.end(),sortmore());
	int* APindex = new int[groupSize];
	float* APoutput = new float[groupSize];
	for (int i = 0; i < groupSize; i++)
	{
		APindex[i]=floor(APoutSort[i].ProbPixelValue);
		APoutput[i]= APoutSort[i].weight;
	}
	APoutSort.clear();
	
//	ofstream outIndex("outIndex.txt");
//	ofstream outWeight("outWeight.txt");
//	for (int i = 0;i< groupSize; i++)
//	{
//		outIndex << APindex[i] << endl;
//		outWeight << APoutput[i] << endl;
//	}
//	outIndex.close();
//	outWeight.close();
//	ofstream outTemp("outTemp.txt");
//	outTemp.close();
	
	int APclassnum = 1;
	int APclassindexcur = APoutput[0];
	for (int i = 1; i<groupSize; i++)
	{
		if (APclassindexcur != APoutput[i])
		{
			APclassnum++;
			APclassindexcur = APoutput[i];
		}
	}
	
	//  // get exemplars for each class, count class size and get the first (smallest) index in each class
	int* APclassrep = new int[APclassnum];
	for (int i = 0; i < APclassnum; i++) APclassrep[i] = 0;
	int* APclasssize = new int[APclassnum];
	for (int i = 0; i < APclassnum; i++) APclasssize[i] = 0;
	int* APclassfirst = new int[APclassnum];
	for (int i = 0; i < APclassnum; i++) APclassfirst[i] = 0;
	int* APclassstart = new int[APclassnum];
	for (int i = 0; i < APclassnum; i++) APclassstart[i] = 0;
	
	int APclassindex = 0;
	APclassstart[APclassindex] = 0;
	APclasssize[APclassindex] = 1;
	APclassrep[APclassindex] = (int)APoutput[0];
	APclassfirst[APclassindex] = APindex[0];
	for (int i = 1; i < groupSize; i++)
	{
		if (APclassrep[APclassindex] != (int)APoutput[i])
		{
			APclassindex++;
			APclassstart[APclassindex] = i;
			APclasssize[APclassindex]++;
			APclassrep[APclassindex] = (int)APoutput[i];
			if (APclasssize[APclassindex] == 1)
				APclassfirst[APclassindex] = APindex[i];
			else if (APindex[i]<APclassfirst[APclassindex])
				APclassfirst[APclassindex] = APindex[i];
		}
		else
		{
			APclasssize[APclassindex]++;
			if (APindex[i]<APclassfirst[APclassindex])
				APclassfirst[APclassindex] = APindex[i];
		}
	}
	
	//  // test output
	//for (int i = 0; i < APclassnum; i++) printf("%d ", APclassrep[i]); printf("\n");
	//for (int i = 0; i < APclassnum; i++) printf("%d ", APclassfirst[i]); printf("\n");
	//for (int i = 0; i < APclassnum; i++) printf("%d ", APclasssize[i]); printf("\n");
	//for (int i = 0; i < APclassnum; i++) printf("%d ", APclassstart[i]); printf("\n");
	
	//  // sort all classes based on APclassfirst;
	
	vector<tmpStruct> APclassSort;
	for (int i = 0; i < APclassnum; i++)
		APclassSort.push_back(tmpStruct(APclassfirst[i],i));
	sort(APclassSort.begin(),APclassSort.end(),sortmore());
	int* APclassfirstindex = new int[APclassnum];
	float* APclassfirstfloat = new float[APclassnum];
	for (int i = 0; i < APclassnum; i++) 
	{
		APclassfirstindex[i] = floor(APclassSort[i].ProbPixelValue);
		APclassfirstfloat[i] = APclassSort[i].weight;
	}
	APclassSort.clear();
	
	//  // write into the data structure of final AP results
	APclustersize = APclassnum;
	APcluster = new int*[APclustersize];
	APclustersubsize = new int[APclustersize];
	APclustersubrep = new int[APclustersize];
	
	for (int i = 0; i < APclustersize; i++)
	{
		APcluster[i] = new int[APclasssize[APclassfirstindex[i]]];
		APclustersubsize[i] = APclasssize[APclassfirstindex[i]];
		APclustersubrep[i] = APclassrep[APclassfirstindex[i]];
	}
	for (int i = 0; i < APclustersize; i++)
		for (int j = 0; j < APclustersubsize[i]; j++)
			APcluster[i][j] = (int)APindex[APclassstart[APclassfirstindex[i]]+j];
		
		delete[] APclassfirstindex;
	delete[] APclassfirstfloat;
	
	delete[] APclassstart;
	delete[] APclasssize;
	delete[] APclassfirst;
	delete[] APclassrep;
	
	delete[] APindex;
	delete[] APoutput;
	
	//  // delete APidx
	if(APidx) free(APidx);
	
	// run affinity propagation on the current pairwise distance matrix
	///////////////////////////////////////
	// delete newed variables
// 	delete[] APsij;
	delete[] APsijd;
// 	delete[] APi;
// 	delete[] APj;
	delete[] APid;
	delete[] APjd;
}

#endif
