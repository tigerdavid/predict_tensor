//
//  aux_func.h
//  FISH
//
//  Created by Yaozong Gao on 05/20/14.
//  Copyright (c) 2014 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __aux_func_h__
#define __aux_func_h__

namespace BRIC { namespace IDEA { namespace FISH {

inline void err_message(const char* str) {
	assert(false);
	std::cerr << str << std::endl; 
	exit(-1);
}

inline void assert_message(bool flag, const char* str) {
	assert(flag);
	if (!flag) {
		std::cerr << str << std::endl;
		exit(-1);
	}
}

template <typename T>
inline bool in_range(T val, T bound1, T bound2)
{
	if (bound1 < bound2)
	{
		if (val >= bound1 && val <= bound2)
			return true;
		else
			return false;
	}
	else{

		if (val >= bound2 && val <= bound1)
			return true;
		else
			return false;
	}
}

template <typename T>
inline T truncate(T val, T bound1, T bound2)
{
	if (bound1 < bound2)
	{
		return (val < bound1) ? bound1 : ( (val > bound2) ? bound2 : val );
	}
	else
	{
		return (val < bound2) ? bound2 : ( (val > bound1) ? bound1 : val );
	}
}


} } }

#endif