//
//  ArrayFunctor.h
//  FISH
//
//  Created by Yaozong Gao on 6/25/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __ArrayFunctor_h__
#define __ArrayFunctor_h__

namespace BRIC { namespace IDEA { namespace FISH {


/** @brief Functor for continuous array
 *
 * a simple-to-use wrapper for continuous array
 */ 
template <typename T>
class ArrayFunctor
{
public:

	ArrayFunctor() {
		m_pointer = NULL;
	}

	ArrayFunctor(const T* array) {
		m_pointer = array;
	}

	void SetArray(const T* array) {
		m_pointer = array;
	}

	T operator() (unsigned int featureIndex) const {
		return m_pointer[featureIndex];
	}

private:

	const T* m_pointer;
};


} } }

#endif