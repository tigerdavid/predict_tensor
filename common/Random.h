//
//  Random.h
//  FISH
//
//  Created by Yaozong Gao on 4/4/13.
//  Copyright (c) 2013 yzgao@cs.unc.edu. All rights reserved.
//

#ifndef __Random_h__
#define __Random_h__

// #include "stdafx.h"
#include "common/BoostHeaders.h"
#include <time.h>

//#include <boost/random/mersenne_twister.hpp>
//#include <boost/random/uniform_real_distribution.hpp>
//#include <boost/random/uniform_int_distribution.hpp>

namespace BRIC { namespace IDEA { namespace FISH {
    
    
/** @brief Random number generator wrapper
 *
 * use boost random number generator inside
 */
class Random {
    
public:
    
	/** @brief constructor */
    Random();
    
	/** @brief constructor */
    Random(unsigned int seed);
    
    void Seed(const unsigned int seed);
    
	/** @brief random a float in [0,1] */
    float NextDouble();
    
	/** @brief random a float within a range */
    float NextDouble(float minValue, float maxValue);
    
	/** @brief random a integer [minValue,maxValue] */
	template <typename T>
    T Next(T minValue, T maxValue);

	/** @brief random a float from a Gaussian distribution */
	float GaussianNextDouble(float mean, float sigma);
    
private:
    
    boost::random::mt19937 m_gen;
    
};

/** @brief constructor */
Random::Random(): m_gen( (unsigned int)(time(NULL)) ) {}

/** @brief constructor */
Random::Random(unsigned int seed): m_gen(seed) {}

void Random::Seed(const unsigned int seed) {
	m_gen.seed(seed);
}

/** @brief random a float in [0,1] */
float Random::NextDouble() 
{
	boost::random::uniform_real_distribution<float> dist(0,1);
	return dist(m_gen);
}

/** @brief random a float within a range */
float Random::NextDouble(float minValue, float maxValue) 
{
	boost::random::uniform_real_distribution<float> dist(minValue, maxValue);
	return dist(m_gen);
}

/** @brief random a integer [minValue,maxValue] */
template <typename T>
T Random::Next(T minValue, T maxValue) 
{
	boost::random::uniform_int_distribution<T> dist(minValue, maxValue);
	return dist(m_gen);
}

/** @brief generate a float number from a Gaussian distribution */
float Random::GaussianNextDouble(float mean, float sigma)
{
	boost::random::normal_distribution<float> dist(mean, sigma);
	return dist(m_gen);
}
    
} } }

#endif
