#ifndef HEADERS_H
#define HEADERS_H

#include <math.h>
#include <omp.h>

#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>
#define DBL_MAX         1.7976931348623158e+308 /* max value */
#include <sys/stat.h>
#include <sys/param.h>
#include <fcntl.h>
#include <errno.h>
#include <assert.h>
#include <time.h>
// #include <ctime>

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <numeric>
#include <random>

#include <cstdio>
#include <ctime>
#include <chrono>

// #include <gsl/gsl_matrix.h>
// #include <gsl/gsl_vector.h>
// #include <gsl/gsl_math.h>
// #include <gsl/gsl_blas.h>
// #include <gsl/gsl_linalg.h>
// #include <gsl/gsl_matrix_double.h>

#include "string_format.h"

#ifdef NDEBUG 
#define DMSG(x) 
#else 
#define DMSG(x) cout << bold << "[Debug] " << nobold << x << endl
#endif

// #ifdef DEBUG // NDEBUG for "not in debug state"
// #define LOG_MSG(msg) cout << msg << endl;
// #else
// #define LOG_MSG(msg)
// #endif

using namespace std;

#endif
