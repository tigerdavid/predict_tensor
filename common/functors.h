// #include "armadillo"

#ifndef FUNCTOR_H
#define FUNCTOR_H

#include <memory>
#include <stdexcept>

std::string exec(const char* cmd) {
	char buffer[128];
	std::string result = "";
	std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
	if (!pipe) throw std::runtime_error("popen() failed!");
	while (!feof(pipe.get())) {
		if (fgets(buffer, 128, pipe.get()) != NULL)
			result += buffer;
	}
	return result;
}

void funInitParamCorr(int seedNum,unsigned int PatchSize3, int numCorrFeature, vector<vector<int> >& currentParamCorr)
{
	srand(seedNum);
	const int numParamPerCorr = 2;
	currentParamCorr = vector<vector<int> >(numCorrFeature,vector<int>(numParamPerCorr));
	vector<int> tempPossibleValues(PatchSize3);
	for (unsigned int ii=0;ii<PatchSize3;ii++)
		tempPossibleValues[ii] = ii;
	random_shuffle(begin(tempPossibleValues),end(tempPossibleValues));
	for (unsigned int ii=0;ii<currentParamCorr.size();ii++)
	{
		currentParamCorr[ii][0] = (PatchSize3-1)/2;
		currentParamCorr[ii][1] = tempPossibleValues[ii];
	}
}

void funInitParamHaar(int seedNum,unsigned int PatchSize, int numHaarFeature, vector<vector<int> >& currentParamHaar)
{
	srand(seedNum);
	const int numParamPerHaar = 9;
	currentParamHaar = vector<vector<int> >(numHaarFeature,vector<int>(numParamPerHaar));
	for (int j = 0;j<numHaarFeature;j++)
	{
		currentParamHaar[j][0] = rand()% 2; // To decide if it is one or two blocks
		currentParamHaar[j][1] = rand()% 3;
		currentParamHaar[j][2] = rand()% 3; // To decide the size of two blocks
		for (int k = 3;k < 6;k++)
			currentParamHaar[j][k] = rand()%(PatchSize-2*currentParamHaar[j][1]) + currentParamHaar[j][1];
		for (int k = 6;k < 9;k++)
			currentParamHaar[j][k] = rand()%(PatchSize-2*currentParamHaar[j][2]) + currentParamHaar[j][2];
	}
	
}

void funTestingSampling(const VectorImageType::Pointer inputImage, const VectorImageType::Pointer maskImage,unsigned int SPACING, unsigned int radius, vector<location<VectorPixelType> > &PatchCenter)
{
	VectorImageType::RegionType OutRegion = inputImage->GetLargestPossibleRegion();
	VectorImageType::SizeType OutSize = OutRegion.GetSize();
	
	for (unsigned int k = radius; k < (int) OutSize[2]-radius; k+=SPACING)
	{
		for (unsigned int j = radius; j < (int) OutSize[1]-radius; j+=SPACING)
		{
			for (unsigned int i = radius; i < (int) OutSize[0]-radius; i+=SPACING)
			{
				VectorImageType::IndexType tempPixelIndex;
				
				tempPixelIndex[0] = i;
				tempPixelIndex[1] = j;
				tempPixelIndex[2] = k;
				VectorPixelType tempPixelValue = inputImage->GetPixel(tempPixelIndex);
				VectorPixelType maskPixelValue = maskImage->GetPixel(tempPixelIndex);
				if ((funMean(tempPixelValue) !=0)&&(maskPixelValue[0]>0))
				{
					location<VectorPixelType> tempLoc;
					tempLoc.x = i;
					tempLoc.y = j;
					tempLoc.z = k;
					PatchCenter.push_back(tempLoc);
				}
			}
		}
	}
}


void funTrainingSampling(int seedSampling, const VectorImageType::Pointer inputImage, const VectorImageType::Pointer targetImage, const VectorImageType::Pointer maskImage, unsigned int PatchNum, unsigned int radius, vector<location<VectorPixelType> > &PatchCenter)
{
	
	srand(seedSampling);
	// select patches' locations
	VectorImageType::RegionType OutRegion = targetImage->GetLargestPossibleRegion();
	
	// generate shrinkedRegion which crop out the region which the patch's boundary will go out of image's boundary
	VectorImageType::IndexType shrinkedIndex;shrinkedIndex.Fill(radius);
	VectorImageType::SizeType radiusSize;radiusSize.Fill(2*radius);
	VectorImageType::SizeType shrinkedRegionSize = OutRegion.GetSize() - radiusSize;
	VectorImageType::RegionType shrinkedRegion;
	shrinkedRegion.SetIndex(shrinkedIndex);
	shrinkedRegion.SetSize(shrinkedRegionSize);
	
	
	itk::ImageRegionConstIterator<VectorImageType> inputPtr(inputImage,shrinkedRegion);
	for (inputPtr.GoToBegin();!inputPtr.IsAtEnd();++inputPtr)
	{
		VectorPixelType inPixelValue = inputPtr.Get();
		VectorImageType::IndexType TempPixelIndex = inputPtr.GetIndex();
		VectorPixelType tarPixelValue = targetImage->GetPixel(TempPixelIndex);
		VectorPixelType maskPixelValue = maskImage->GetPixel(TempPixelIndex);
		if (maskPixelValue[0]>0)
		{
			if (funNotZero(inPixelValue)&&(funNotZero(tarPixelValue)))
			{
				location<VectorPixelType> tempLoc(TempPixelIndex,tarPixelValue);
				PatchCenter.push_back(tempLoc);
			}
		}
	}
	
	random_shuffle(begin(PatchCenter),end(PatchCenter));
	PatchCenter.resize(PatchNum);
	
}

VectorImageType::Pointer funGenMaskFromProb(VectorImageType::Pointer probImage1, VectorImageType::Pointer probImage2, VectorImageType::Pointer probImage3,VectorImageType::Pointer inMaskImage)
{
	VectorImageType::Pointer outMaskImage = VectorImageType::New();
	outMaskImage = ImageInitializer<VectorImageType>(probImage1,1,0);
	typedef itk::ImageRegionIterator<VectorImageType> IteratorType;
	VectorImageType::RegionType ImageRegion = probImage1->GetLargestPossibleRegion();
	IteratorType outMaskPtr(outMaskImage,ImageRegion);outMaskPtr.GoToBegin();
	IteratorType probPtr1(probImage1, ImageRegion);probPtr1.GoToBegin();
	IteratorType probPtr2(probImage2, ImageRegion);probPtr2.GoToBegin();
	IteratorType probPtr3(probImage3, ImageRegion);probPtr3.GoToBegin();
	IteratorType inMaskPtr(inMaskImage,ImageRegion);inMaskPtr.GoToBegin();
	
	for (;!outMaskPtr.IsAtEnd();++outMaskPtr,++probPtr1,++probPtr2,++probPtr3,++inMaskPtr)
	{
		VectorPixelType maskPixelValue = inMaskPtr.Get();
		if (maskPixelValue[0]>0)
		{
			VectorPixelType sumValue = probPtr1.Get()+probPtr2.Get()+probPtr3.Get();
			outMaskPtr.Set(sumValue);
		}
	}
	return outMaskImage;
}

// void funFMRITrainingSampling(int seedSampling, const VectorImageType::Pointer fmriImage, const VectorImageType::Pointer tensorImage, const VectorImageType::Pointer maskImage, unsigned int PatchNum, unsigned int extradius, vector<location<VectorPixelType> > &PatchCenter)
// {
// 	
// 	srand(seedSampling);
// 	
// 	VectorImageType::RegionType OutRegion = tensorImage->GetLargestPossibleRegion();
// 	VectorImageType::SizeType partOutSize = OutRegion.GetSize();
// 	partOutSize[0]-=(2*extradius);
// 	partOutSize[1]-=(2*extradius);
// 	partOutSize[2]-=(2*extradius);
// 	VectorImageType::RegionType partOutRegion;
// 	partOutRegion.SetSize(partOutSize);
// 	VectorImageType::IndexType regionIndex;
// 	regionIndex.Fill(extradius);
// 	partOutRegion.SetIndex(regionIndex);
// 	
// 	itk::ImageRegionConstIterator<VectorImageType> fMRIPtr(fmriImage,partOutRegion);
// 	for (fMRIPtr.GoToBegin();!fMRIPtr.IsAtEnd();++fMRIPtr)
// 	{
// 		VectorPixelType fmriPixelValue = fMRIPtr.Get();
// 		VectorImageType::IndexType TempPixelIndex = fMRIPtr.GetIndex();
// 		VectorPixelType tensorPixelValue = tensorImage->GetPixel(TempPixelIndex);
// 		VectorPixelType maskPixelValue = maskImage->GetPixel(TempPixelIndex);
// 		if (maskPixelValue[0]>0)
// 		{
// 			if (funNotZero(fmriPixelValue)&&(funNotZero(tensorPixelValue)))
// 			{
// 				location<VectorPixelType> tempLoc(TempPixelIndex,tensorPixelValue);
// 				PatchCenter.push_back(tempLoc);
// 			}
// 		}
// 	}
// 	
// 	random_shuffle(begin(PatchCenter),end(PatchCenter));
// 	PatchCenter.resize(PatchNum);
// 
// 	
// }

void funPCATrainingSampling(const VectorImageType::Pointer inputImage, const VectorImageType::Pointer targetImage, const VectorImageType::Pointer maskImage, unsigned int PatchNum, unsigned int radius, vector<location<Row<float> > > &AllPatchCenter)
{
	const int PCAPatchSize = 3;
	const int PCARadius = (PCAPatchSize-1)/2;
	const unsigned int targetVectorLength = targetImage->GetNumberOfComponentsPerPixel();
	
	VectorImageType::SizeType PCARadiusVect3;
	PCARadiusVect3.Fill(PCARadius);
	
	vector<location<Row<float> > > PatchCenter;
	
	// select patches' locations
	VectorImageType::RegionType OutRegion = targetImage->GetLargestPossibleRegion();
	
	// generate shrinkedRegion which crop out the region which the patch's boundary will go out of image's boundary
	VectorImageType::IndexType shrinkedIndex;shrinkedIndex.Fill(radius);
	VectorImageType::SizeType radiusSize;radiusSize.Fill(2*radius);
	VectorImageType::SizeType shrinkedRegionSize = OutRegion.GetSize() - radiusSize;
	VectorImageType::RegionType shrinkedRegion;
	shrinkedRegion.SetIndex(shrinkedIndex);
	shrinkedRegion.SetSize(shrinkedRegionSize);
	
	VectorImageType::RegionType PatchRegion;
	VectorImageType::SizeType PCAPatchRegionSize;
	PCAPatchRegionSize.Fill(PCAPatchSize);
	PatchRegion.SetSize(PCAPatchRegionSize);
	
	
	itk::ImageRegionConstIterator<VectorImageType> inputPtr(inputImage,shrinkedRegion);
	for (inputPtr.GoToBegin();!inputPtr.IsAtEnd();++inputPtr)
	{
		VectorPixelType inPixelValue = inputPtr.Get();
		VectorImageType::IndexType TempPixelIndex = inputPtr.GetIndex();
		VectorPixelType tarPixelValue = targetImage->GetPixel(TempPixelIndex);
		VectorPixelType maskPixelValue = maskImage->GetPixel(TempPixelIndex);
		if (maskPixelValue[0]>0)
		{
			if (funNotZero(inPixelValue)&&(funNotZero(tarPixelValue)))
			{
				location<Row<float> > tempLoc(TempPixelIndex);
				PatchCenter.push_back(tempLoc);

			}
		}
	}
	
	random_shuffle(begin(PatchCenter),end(PatchCenter));
	PatchCenter.resize(PatchNum);
	
	for (unsigned int i = 0;i<PatchNum;i++)
	{
		VectorImageType::IndexType TempPixelIndex = PatchCenter[i].GetIndex();
		
		VectorImageType::IndexType regionIndex = TempPixelIndex - PCARadiusVect3;
		PatchRegion.SetIndex(regionIndex);
		itk::ImageRegionConstIterator<VectorImageType> patPatchPtr(targetImage,PatchRegion);
		
		vector<float> patchOfDTensor;
		
		for (patPatchPtr.GoToBegin();(!patPatchPtr.IsAtEnd());++patPatchPtr)
		{
			VectorPixelType tempPatchDTensor = patPatchPtr.Get();
			patchOfDTensor.insert(patchOfDTensor.end(),tempPatchDTensor.GetDataPointer(),tempPatchDTensor.GetDataPointer()+targetVectorLength);
		}
		PatchCenter[i].label = Row<float>(patchOfDTensor);
	}
	AllPatchCenter.insert(AllPatchCenter.end(),PatchCenter.begin(),PatchCenter.end());
	
}

vector<location<VectorPixelType> > funDimensionReduction(vector<location<Row<float> > > &AllPatchCenter, Col<float> &s, Mat<float> &V, const unsigned int reducedLength)
{
	vector<location<VectorPixelType> > PatchCenter(AllPatchCenter.size());
	Mat<float> U;
	Mat<float> A;
	int index = 0;
	for (auto i : AllPatchCenter)
		A.insert_rows(index,i.GetValue());
	svd_econ(U,s,V,A);
	for (unsigned int i = 0;i<AllPatchCenter.size();i++)
	{
		PatchCenter[i].SetIndex(AllPatchCenter[i].GetIndex());
		for (unsigned int j = 0;j<reducedLength;j++)
			PatchCenter[i].label[j] = U(i,j);
	}
	return PatchCenter;
}

//TODO: funDimensionRecovery
// vector<location<VectorPixelType> > funDimensionRecovery(vector<location<Row<float> > > &AllPatchCenter, Col<float> &s, Mat<float> &V, const unsigned int reducedLength)
// {
// 	vector<location<VectorPixelType> > PatchCenter(AllPatchCenter.size());
// 	Mat<float> U;
// 	Mat<float> A;
// 	int index = 0;
// 	for (auto i : AllPatchCenter)
// 		A.insert_rows(index,i.GetValue());
// 	svd_econ(U,s,V,A);
// 	for (unsigned int i = 0;i<AllPatchCenter.size();i++)
// 	{
// 		PatchCenter[i].SetIndex(AllPatchCenter[i].GetIndex());
// 		for (unsigned int j = 0;j<reducedLength;j++)
// 			PatchCenter[i].label[j] = U(i,j);
// 	}
// 	return PatchCenter;
// }

#endif