#ifndef IO_H
#define IO_H

#include "headers.h"

void exitWithProblem()
{
	exit(1);
}

template <typename T>
void funSaveVector(vector<T> &inputVec, string outFileName)
{
	ofstream outFile(outFileName.data());
	if (outFile)
	{
		DMSG("Saving " << outFileName);
		for (auto i: inputVec)
			outFile << i << endl;
		outFile.close();
	} else
	{
		cerr << "Warning: Exception in saving file: " << outFileName << endl;
		cerr << "Exception ID: " << strerror(errno) << endl;
		exitWithProblem();
	}
}

template <typename T>
void funLoadVector(vector<T> &outputVec, string inFileName)
{
	ifstream inFile(inFileName.data());
	if (inFile)
	{
		DMSG("Reading " << inFileName);
		string inLine;
		while (getline(inFile,inLine)) // cannot use try-catch as it will get error with getline function
		{
			T temp;
			istringstream itt(inLine);
			if (!(itt >> temp)){
				break;
			}
			outputVec.push_back(temp);
		}
	} else
	{
		cerr << "Warning: Exception in opening file: " << inFileName << endl;
		cerr << "Exception ID: " << strerror(errno) << endl;
		exitWithProblem();
	}
	inFile.close();
}

template <typename T>
void funSaveMatrix(vector< vector<T> > &inputMat, string outFileName)
{
	ofstream outFile(outFileName.data());
	if (outFile)
	{
		const unsigned long int x = inputMat[0].size();
		const unsigned long int y = inputMat.size();
		DMSG("Saving " << outFileName);
		outFile << x << "\t" << y << endl; // print out the size of matrix
		for (unsigned long int j = 0;j<y;j++)
		{
			for (unsigned long int i = 0;i<x;i++)
				outFile << inputMat[j][i] << " ";
			outFile << endl;
		}
	} else
	{
		cerr << "Warning: Exception in saving file: " << outFileName << endl;
		cerr << "Exception ID: " << strerror(errno) << endl;
		exitWithProblem();
	}
	outFile.close();
}

template <typename T>
void funLoadMatrix(vector< vector<T> > &outputMat, string inFileName)
{
	ifstream inFile(inFileName.data());
	if (inFile)
	{
		DMSG("Reading " << inFileName);
		unsigned long int x,y;
		inFile >> x >> y;
		outputMat.resize(y);
		for (unsigned long int j=0;j<y;j++)
		{
			outputMat[j].resize(x);
			for (unsigned long int i=0;i<x;i++)
				inFile >> outputMat[j][i];
		}
	} else
	{
		cerr << "Warning: Exception in opening file: " << inFileName << endl;
		cerr << "Exception ID: " << strerror(errno) << endl;
		exitWithProblem();
	}
	inFile.close();
}

template <typename T>
void funSaveCuboid(vector< vector< vector<T> > > &inputCub, string outFileName)
{
	ofstream outFile(outFileName.data());
	if (outFile)
	{
		const unsigned long int x = inputCub[0][0].size();
		const unsigned long int y = inputCub[0].size();
		const unsigned long int z = inputCub.size();
		DMSG("Saving " << outFileName);
		outFile << x << "\t" << y << "\t" << z << endl; // print out the size of matrix
		for (unsigned long int k = 0;k<z;k++)
		{
			for (unsigned long int j = 0;j<y;j++)
				for (unsigned long int i = 0;i<x;i++)
					outFile << inputCub[k][j][i] << " ";
				outFile << endl;
		}
	} else
	{
		cerr << "Warning: Exception in saving file: " << outFileName << endl;
		cerr << "Exception ID: " << strerror(errno) << endl;
		exitWithProblem();
	}
	outFile.close();
}

template <typename T>
void funLoadCuboid(vector< vector< vector<T> > > &outputCub, string inFileName)
{
	ifstream inFile(inFileName.data());
	if (inFile)
	{
		inFile.open(inFileName.data());
		DMSG("Reading " << inFileName);
		unsigned long int x,y,z;
		inFile >> x >> y >> z;
		outputCub.resize(z);
		for (unsigned long int k=0;k<z;k++)
		{
			outputCub[k].resize(y);
			for (unsigned long int j=0;j<y;j++)
			{
				outputCub[j].resize(x);
				for (unsigned long int i=0;i<x;i++)
					inFile >> outputCub[k][j][i];
			}
		}
	} else
	{
		cerr << "Warning: Exception in opening file: " << inFileName << endl;
		cerr << "Exception ID: " << strerror(errno) << endl;
		exitWithProblem();
	}
	inFile.close();
	
}

#endif